<?php $query = mysqli_query($koneksi, "SELECT max(id_lelang) as kodeTerbesar FROM lelang");
        $data = mysqli_fetch_array($query);
        $kodeid = $data['kodeTerbesar'];
        $urutan = (int) substr($kodeid,3,5);

        // bilangan yang diambil ini ditambah 1 untuk menentukan nomor urut berikutnya
        $urutan++;


        $huruf = "LEL";
        $kodeid = $huruf . sprintf("%05s", $urutan);

        if (!empty($_SESSION["notif"])) {
            echo $_SESSION["notif"];
            unset($_SESSION["notif"]);
        }

        ?>


<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                <form action="function/insert_lelang.php" method="POST">
                    <div class="row g-3">



                        <div class="col-lg-6">
                            <label class="form-label">No. Lelang</label>
                            <input type="text" class="form-control" value="<?= $kodeid ?>" name="id_lelang" readonly>

                        </div>
                        <div class="col-lg-6">
                            <label class="form-label">Start Date Lelang</label>
                             <input type="date" class="form-control flatpickr-input active" data-provider="flatpickr" data-date-format="Y-m-d" enable-time value="<?= $tgl ?>" name="tgl_lelang" required>
                        </div>
						 <div class="col-lg-6">
                            <label class="form-label">End Date Lelang</label>
                            <input type="date" class="form-control flatpickr-input active" data-provider="flatpickr" data-date-format="Y-m-d" data-enable-time  name="end_tgl_lelang" required >
                        </div>
                        <div class="col-lg-6">
                            <label class="form-label">Nama Barang</label>
                            <input type="text" class="form-control" name="nama_barang" required>
                        </div>
                        <div class="col-lg-6">
                            <label class="form-label">Kategori</label>
                            <input type="text" class="form-control" name="kategori_barang" required>
                        </div>
                        <div class="col-lg-6">
                            <label class="form-label">Type</label>
                            <input type="text" class="form-control" name="type_barang" required>
                        </div>
                        <div class="col-lg-6">
                            <label class="form-label">Jenis </label>
                            <input type="text" class="form-control" name="jenis_barang" required>
                        </div>
                        <div class="col-lg-6">
                            <label class="form-label">Merk</label>
                            <input type="text" class="form-control" name="merk_barang" required>
                        </div>
                        <div class="col-lg-6">
                            <label class="form-label">Tahun</label>
                            <input type="text" class="form-control" name="tahun_barang" required>
                        </div>
                        <div class="col-lg-6">
                            <label class="form-label">Harga Lelang</label>
                            <input type="text" class="form-control" name="harga_lelang" id="cleave-numeral" required>
                        </div>
						<div class="col-lg-8">
                            <label class="form-label">Link Inspection</label>
                            <input type="text" class="form-control" name="link_inspeksi" >
                        </div>
						<div class="col-lg-2">

                            <label class="form-label">Grade</label>
                            <input type="text" class="form-control" name="grade" >
                        </div>
                        <div class="col-lg-2">

                            <label class="form-label">Status Lelang</label>
                            <select class="form-control" data-plugin="choices" name="status_lelang" required>
                                <option value="Aktif">Aktif</option>
                                <option value="Non Aktif">Non Aktif</option>
                            </select>
                        </div>
                        <div class="col-lg-12">
                            <label class="form-label">Description Barang</label>
                            <textarea id="ckeditor-classic" name="diskripsi" required>
                        </textarea>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="mt-4 hstack gap-2">
                            <a type="button" class="btn btn-light" href="index.php?page=Lelang" >Cancle</a>
                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add Lelang</button>

                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="card">
			<div class="card-header d-flex align-items-center">
                                    <h6 class="card-title mb-0 flex-grow-1">Add Foto</h6>
                                    
                                </div>
            <div class="card-body">
				
				<div class="table-responsive table-card">
                                        <table class="table table-borderless align-middle">
                                            <tbody>
												<?php $sql1 = mysqli_query($koneksi, "SELECT * FROM foto_lelang where id_lelang = '$kodeid' "); 
																$nomor = 1;
															while ($row1 = mysqli_fetch_assoc($sql1)) { ?>
                                                <tr>
                                                    <td>
                                                        <div class="d-flex align-items-center">
                                                            <img src="file/lelang/<?=$row1['foto']?>" alt="" class="avatar-sm object-cover">
                                                            <div class="ms-2">
                                                              <h6 class="fs-15 mb-1">Foto Ke <?=$nomor++ ?></h6>
																<a href="function/delete_foto_lelang.php?aksi=delete&id=<?=$row1['id_foto_lelang']?>" class="btn btn-danger btn-sm" >Delete</a>
															
                                                             </div>
                                                        </div>
                                                    </td>
                                                        </tr>
												<?php } ?>
                                                
                                              </tbody>
                                        </table>
                                    </div>
				<form action="function/insert_lelang.php"  method="POST" enctype="multipart/form-data">
              	<div>
					<br>
                        <label for="client_nameName-field" class="form-label">Add Foto</label>
						<input type="text" class="form-control" value="<?= $kodeid ?>" name="id_lelang" hidden>
                        <input class="form-control" type="file" name="foto" >
                </div>
					<div class="modal-footer">
                <div class="mt-4 hstack gap-2">
                    <button type="submit" class="btn btn-success" name="upload_foto" >Upload Foto</button>
                </div>
					</div>
                </form>
            </div>
		</div>
		
		
	</div>
</div>





<script src="assets/libs/@ckeditor/ckeditor5-build-classic/build/ckeditor.js"></script>
<script src="assets/js/pages/project-create.init.js"></script>
<script src="assets/libs/cleave.js/cleave.min.js"></script>
<!-- form masks init -->
<script src="assets/js/pages/form-masks.init.js"></script>