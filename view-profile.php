
<?php


$sql = mysqli_query($koneksi, "SELECT  *, login.username  FROM user INNER JOIN login ON user.idnik = login.idnik WHERE user.idnik ='" . $_GET['id'] . "' ");

$row = mysqli_fetch_assoc($sql)

?>

<div class="profile-foreground position-relative mx-n4 mt-n4">
                        <div class="profile-wid-bg">
                            <img src="assets/images/profile-bg.jpg" alt="" class="profile-wid-img" />
                        </div>
                    </div>
                    <div class="pt-4 mb-4 mb-lg-3 pb-lg-4">
                        <div class="row g-4">
                            <div class="col-auto">
                                <div class="avatar-lg">
                                    <img src="file/profile/<?=$row['file_foto']?>" alt="user-img" class="avatar-lg img-thumbnail rounded-circle mx-auto profile-img" />
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col">
                                <div class="p-2">
                                    <h3 class="text-white mb-1"><?=$row['nama']?></h3>
                                    <p class="text-white-75"><?=$row['divisi']?> / <?=$row['position']?></p>
                                    
                                </div>
                            </div>
                            <!--end col-->
                            
                            <!--end col-->

                        </div>
                        <!--end row-->
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div>
                                <div class="d-flex">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-pills animation-nav profile-nav gap-2 gap-lg-3 flex-grow-1" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link fs-14 active" data-bs-toggle="tab" href="#overview-tab" role="tab">
                                                <i class="ri-airplay-fill d-inline-block d-md-none"></i> <span class="d-none d-md-inline-block">Overview</span>
                                            </a>
                                        </li>
                                        
                                        <li class="nav-item">
                                            <a class="nav-link fs-14" data-bs-toggle="tab" href="#projects" role="tab">
                                                <i class="ri-price-tag-line d-inline-block d-md-none"></i> <span class="d-none d-md-inline-block">Team</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link fs-14" data-bs-toggle="tab" href="#documents" role="tab">
                                                <i class="ri-folder-4-line d-inline-block d-md-none"></i> <span class="d-none d-md-inline-block">Documents</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="flex-shrink-0">
                                        <a href="index.php?page=UpdateUser&id=<?= $row['idnik'] ?>" class="btn btn-success"><i class="ri-edit-box-line align-bottom"></i> Edit Employeer</a>
                                    </div>
                                </div>
                                <!-- Tab panes -->
                                <div class="tab-content pt-4 text-muted">
                                    <div class="tab-pane active" id="overview-tab" role="tabpanel">
                                        <div class="row">
                                            <div class="col-xxl-3">
                                                

                                                <div class="card">
                                                    <div class="card-body">
                                                        <h5 class="card-title mb-3">Info</h5>
                                                        <div class="table-responsive">
                                                            <table class="table table-borderless mb-0">
                                                                <tbody>
																	<tr>
                                                                        <th class="ps-0" scope="row">ID-NIK :</th>
                                                                        <td class="text-muted"><?=$row['idnik']?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="ps-0" scope="row">Full Name :</th>
                                                                        <td class="text-muted"><?=$row['nama']?></td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <th class="ps-0" scope="row">E-mail :</th>
                                                                        <td class="text-muted"><?=$row['username']?></td>
                                                                    </tr>
																	
                                                                    <tr>
                                                                        <th class="ps-0" scope="row">Location :</th>
                                                                        <td class="text-muted"><?=$row['lokasi']?>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="ps-0" scope="row">Joining Date : </th>
                                                                        <td class="text-muted"><?=$row['doh']?></td>
                                                                    </tr>
																	<tr>
                                                                        <th class="ps-0" scope="row">Status : </th>
                                                                        <td class="text-muted"> <?=$row['status']?></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div><!-- end card body -->
                                                </div><!-- end card -->

                                                <div class="card">
                                                    <!-- end card body -->
                                                </div><!-- end card -->

                                                

                                                
                                                <!--end card-->

                                                
                                                <!--end card-->
                                            </div>
                                            <!--end col-->
                                            <div class="col-xxl-9">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h5 class="card-title mb-3">Company</h5>
                                                        
                                                        
                                                        <div class="row">
                                                            <div class="col-6 col-md-4">
                                                                <div class="d-flex mt-4">
                                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                                            <i class="ri-user-2-fill"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-grow-1 overflow-hidden">
                                                                        <p class="mb-1">Company </p>
                                                                        <h6 class="text-truncate mb-0"><?=$row['company']?></h6>
                                                                    </div>
                                                                </div>
																<div class="d-flex mt-4">
                                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                                            <i class="ri-user-2-fill"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-grow-1 overflow-hidden">
                                                                        <p class="mb-1">Lokasi </p>
                                                                        <h6 class="text-truncate mb-0"><?=$row['lokasi']?></h6>
                                                                    </div>
                                                                </div>
																<div class="d-flex mt-4">
                                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                                            <i class="ri-user-2-fill"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-grow-1 overflow-hidden">
                                                                        <p class="mb-1">Divisi </p>
                                                                        <h6 class="text-truncate mb-0"><?=$row['divisi']?></h6>
                                                                    </div>
                                                                </div>
																<div class="d-flex mt-4">
                                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                                            <i class="ri-user-2-fill"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-grow-1 overflow-hidden">
                                                                        <p class="mb-1">Department </p>
                                                                        <h6 class="text-truncate mb-0"><?=$row['department']?></h6>
                                                                    </div>
                                                                </div>
																<div class="d-flex mt-4">
                                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                                            <i class="ri-user-2-fill"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-grow-1 overflow-hidden">
                                                                        <p class="mb-1">Section </p>
                                                                        <h6 class="text-truncate mb-0"><?=$row['section']?></h6>
                                                                    </div>
                                                                </div>
																<div class="d-flex mt-4">
                                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                                            <i class="ri-user-2-fill"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-grow-1 overflow-hidden">
                                                                        <p class="mb-1">Position </p>
                                                                        <h6 class="text-truncate mb-0"><?=$row['position']?></h6>
                                                                    </div>
                                                                </div>
																
																 
                                                            </div>
                                                            <!--end col-->
                                                            <div class="col-6 col-md-4">
                                                                <div class="d-flex mt-4">
                                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                                            <i class="ri-global-line"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-grow-1 overflow-hidden">
                                                                        <p class="mb-1">Klasifikasi </p>
                                                                        <h6 class="text-truncate mb-0"><?=$row['clasifikasi']?></h6>
                                                                    </div>
                                                                </div>
																<div class="d-flex mt-4">
                                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                                            <i class="ri-global-line"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-grow-1 overflow-hidden">
                                                                        <p class="mb-1">Atasan </p>
                                                                        <h6 class="text-truncate mb-0">
																			<?=$row['atasan']?>
																		</h6>
                                                                    </div>
                                                                </div>
																
																<div class="d-flex mt-4">
                                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                                            <i class="ri-global-line"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-grow-1 overflow-hidden">
                                                                        <p class="mb-1">POH </p>
                                                                        <h6 class="text-truncate mb-0"><?=$row['poh']?></h6>
                                                                    </div>
                                                                </div>
																<div class="d-flex mt-4">
                                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                                            <i class="ri-global-line"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-grow-1 overflow-hidden">
                                                                        <p class="mb-1">Roster </p>
                                                                        <h6 class="text-truncate mb-0"><?=$row['roster']?></h6>
                                                                    </div>
                                                                </div>
																<div class="d-flex mt-4">
                                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                                            <i class="ri-global-line"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-grow-1 overflow-hidden">
                                                                        <p class="mb-1">DOH </p>
                                                                        <h6 class="text-truncate mb-0"><?=$row['doh']?></h6>
                                                                    </div>
                                                                </div>
																<div class="d-flex mt-4">
                                                                    <div class="flex-shrink-0 avatar-xs align-self-center me-3">
                                                                        <div class="avatar-title bg-light rounded-circle fs-16 text-primary">
                                                                            <i class="ri-global-line"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="flex-grow-1 overflow-hidden">
                                                                        <p class="mb-1">Status </p>
                                                                        <h6 class="text-truncate mb-0"><?=$row['status']?></h6>
                                                                    </div>
                                                                </div>
																
															
																
                                                            </div>
                                                            <!--end col-->
                                                        </div>
                                                       
                                                    </div>
                                                    <!--end card-body-->
                                                </div><!-- end card -->

                                                <!-- end row -->

                                                <!-- end card -->

                                            </div>
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                    </div>
                                    
                                    <!--end tab-pane-->
                                    
                                    <!--end tab-pane-->
                                    <div class="tab-pane fade" id="documents" role="tabpanel">
                                        <div class="card">
                                            <div class="card-body">
                                                
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="table-responsive">
                                                            <table class="table table-borderless align-middle mb-0">
                                                                <thead class="table-light">
                                                                    <tr>
                                                                        <th scope="col">File Name</th>
                                                                        <th scope="col">Type</th>
                                                                        <th scope="col">Size</th>
                                                                        <th scope="col">Upload Date</th>
                                                                        
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
																	
																		 <?php $sql = mysqli_query($koneksi, "SELECT * FROM dokumen_user WHERE idnik ='" . $_GET['id'] . "'  "); 
																	$nomor=1;
																	while ($row = mysqli_fetch_assoc($sql)) { 

																	?>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="d-flex align-items-center">
                                                                                <div class="avatar-sm">
                                                                                    <div class="avatar-title bg-soft-primary text-primary rounded fs-20">
                                                                                        <i class="ri-file-zip-fill"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="ms-3 flex-grow-1">
                                                                                    <h6 class="fs-15 mb-0"><a href="file/dokumen-user/<?=$row['file_dok']?>"><?=$row['nama_dok']?></a>
                                                                                    </h6>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                        <td><?php $walii="file/dokumen-user/" . $row['file_dok'] . ""; 
																					echo fsize($walii) ?></td>
                                                                        <td><?php 	
																				$x= explode('.', $walii);
																				$ekstensi= strtolower(end($x));
																			 echo "$ekstensi"?></td>
                                                                        <td><?=date(('d F Y'),strtotime($row['tgl_dok']))?></td>
																		
                                                                         
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end tab-pane-->
                                </div>
                                <!--end tab-content-->
                            </div>
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->

          

           