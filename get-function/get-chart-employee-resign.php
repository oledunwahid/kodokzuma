<?php
include "../koneksi.php";

$tahun = isset($_GET['tahun']) ? $_GET['tahun'] : '';

$query = "SELECT
    CONCAT(MONTHNAME(resign), ' ', YEAR(resign)) AS bulan_tahun,
    COUNT(*) AS jumlah_resign
  FROM
    user";

if ($tahun === 'semua') {
    // Batasi data hanya dari tahun 2020 hingga 2024
    $query .= " WHERE YEAR(resign) BETWEEN 2020 AND 2024";
} elseif ($tahun !== '') {
    // Tampilkan hanya data dari tahun yang dipilih
    $query .= " WHERE YEAR(resign) = '$tahun'";
}

$query .= " GROUP BY YEAR(resign), MONTH(resign) ORDER BY YEAR(resign), MONTH(resign);";

$result = $koneksi->query($query);

$data = array();
foreach ($result as $row) {
    $data[] = $row;
}

echo json_encode($data);
$koneksi->close();
?>
