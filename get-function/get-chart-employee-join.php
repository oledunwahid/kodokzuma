<?php
include "../koneksi.php";

$tahun = isset($_GET['tahun']) ? $_GET['tahun'] : '';

$query = "SELECT
    CONCAT(MONTHNAME(doh), ' ', YEAR(doh)) AS bulan_tahun,
    COUNT(*) AS jumlah_doh
  FROM
    user";

if ($tahun === 'semua') {
    // Batasi data hanya dari tahun 2020 hingga 2024
    $query .= " WHERE YEAR(doh) BETWEEN 2020 AND 2024";
} elseif ($tahun !== '') {
    // Tampilkan hanya data dari tahun yang dipilih
    $query .= " WHERE YEAR(doh) = '$tahun'";
}

$query .= " GROUP BY YEAR(doh), MONTH(doh) ORDER BY YEAR(doh), MONTH(doh);";

$result = $koneksi->query($query);

$data = array();
foreach ($result as $row) {
    $data[] = $row;
}

echo json_encode($data);
$koneksi->close();
?>
