<?php
// Sertakan file koneksi
include "../koneksi.php"; 

// Atur zona waktu default
date_default_timezone_set('Asia/Jakarta');

// Inisialisasi array untuk data
$data = array();

// Penyiapan query
$query = "SELECT * FROM user INNER JOIN login ON user.idnik = login.idnik";

// Gunakan prepared statement untuk meningkatkan keamanan
$stmt = $koneksi->prepare($query);

// Cek jika statement berhasil disiapkan
if ($stmt) {
    // Eksekusi query
    $stmt->execute();

    // Dapatkan hasil
    $result = $stmt->get_result();

    // Fetch data
    while ($row = $result->fetch_assoc()) {
        $data[] = $row;
    }

    // Bebaskan result set
    $result->free();

    // Tutup statement
    $stmt->close();
} else {
    // Jika gagal menyiapkan statement, tampilkan error
    echo "Error: " . $koneksi->error;
    exit; // Hentikan eksekusi lebih lanjut
}

// Set header sebagai JSON
header('Content-Type: application/json');

// Cetak data sebagai JSON
echo json_encode(array("data" => $data));

// Tutup koneksi
$koneksi->close();
?>
