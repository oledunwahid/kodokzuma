<!--datatable css-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
<!--datatable responsive css-->
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<?php if (!empty($_SESSION["notif"])) {
    echo $_SESSION["notif"];
    unset($_SESSION["notif"]);
}    ?>

<!-- Won Auctions Table -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h5 class="card-title mb-0 flex-grow-1">Won Auctions</h5>
                </div>
            </div>
            <div class="card-body">
                <table id="won-auctions-datatables" class="display table table-bordered dt-responsive" style="width:100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Lelang</th>
                            <th>ID Lelang</th>
                            <th>Harga Terakhir</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        // Query untuk mendapatkan lelang yang dimenangkan oleh pengguna
                        $sql_won_auctions = mysqli_query($koneksi, "SELECT *, lelang.id_lelang, MAX(penawaran_lelang.harga_penawaran) AS harga_terakhir
                            FROM lelang
                            INNER JOIN penawaran_lelang ON lelang.id_lelang = penawaran_lelang.id_lelang
                            WHERE penawaran_lelang.idnik = $niklogin AND penawaran_lelang.status_penawaran = 'confirm'
                            GROUP BY lelang.id_lelang");
                        $nomor = 1;
                        while ($row_won_auctions = mysqli_fetch_assoc($sql_won_auctions)) { ?>
                            <tr>
                                <td><?= $nomor++ ?></td>
                                <td><?= $row_won_auctions['nama_barang'] ?></td>
                                <td><?= $row_won_auctions['id_lelang'] ?></td>
                                <td><?= rupiah($row_won_auctions['harga_terakhir']) ?></td>
                                <td>Confirm</td>
                                <td>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary btn-approve" data-toggle="modal" data-target="#approvalModal<?= $row_won_auctions['id_lelang'] ?>">
                                        Approval
                                    </button>
                                </td>
                            </tr>
                            <!-- Modal for Approval -->
                            <div class="modal fade" id="approvalModal<?= $row_won_auctions['id_lelang'] ?>" tabindex="-1" role="dialog" aria-labelledby="approvalModalLabel<?= $row_won_auctions['id_lelang'] ?>" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="approvalModalLabel<?= $row_won_auctions['id_lelang'] ?>">Approval Confirmation</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure you want to approve this auction?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <form action="function/update_confirmation.php" method="post">
                                                <input type="hidden" name="id_lelang" value="<?= $row_won_auctions['id_lelang'] ?>">
                                                <button type="submit" class="btn btn-danger" name="cancel">Cancel</button>
                                                <button type="submit" class="btn btn-primary" name="approve">Approve</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Your Bid Table -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h5 class="card-title mb-0 flex-grow-1">Your Bid</h5>
                </div>
            </div>
            <div class="card-body">
                <table id="your-bid-datatables" class="display table table-bordered dt-responsive" style="width:100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Nama Lelang</th>
                            <th>Harga Penawaran</th>
                            <th>Waktu</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql1 = mysqli_query($koneksi, "SELECT *, user.nama, lelang.nama_barang
                            FROM penawaran_lelang
                            INNER JOIN user ON penawaran_lelang.idnik = user.idnik
                            INNER JOIN lelang ON penawaran_lelang.id_lelang = lelang.id_lelang
                            WHERE penawaran_lelang.idnik = $niklogin");
                        $nomor = 1;
                        while ($row1 = mysqli_fetch_assoc($sql1)) { ?>
                            <tr>
                                <td><?= $nomor++ ?></td>
                                <td><?= $row1['nama'] ?></td>
                                <td><?= $row1['nama_barang'] ?></td>
                                <td><?= rupiah($row1['harga_penawaran']) ?></td>
                                <td><?= $row1['waktu_penawaran'] ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<!--datatable js-->
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="assets/js/pages/datatables.init.js"></script>

<script>
    $(document).ready(function() {
        $('#won-auctions-datatables').DataTable();
        // Script to handle modal activation
        $('#won-auctions-datatables').on('click', '.btn-approve', function() {
            var target_modal = $(this).attr('data-target');
            $(target_modal).modal('show');
        });
    });
</script>