 <!--datatable css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <!--datatable responsive css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<?php if(!empty($_SESSION["notif"])){
										   echo $_SESSION["notif"];
										   unset($_SESSION["notif"]);
									   		}	?>
<div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
									<div class="d-flex align-items-center">
                                    <h5 class="card-title mb-0 flex-grow-1">Your Bid</h5>
										
                                </div>
								</div>
								
                                <div class="card-body">
                                     <table id="buttons-datatables" class="display table table-bordered dt-responsive" style="width:100%">
                                        <thead>
                                            <tr>
                                               
                                                <th>No.</th>
												<th>Nama</th>
                                                <th>Nama Lelang</th>
												<th>Harga Penawaran</th>
												<th>Waktu </th>
												                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                              <?php $sql1 = mysqli_query($koneksi, "SELECT	*, user.nama,lelang.nama_barang
FROM
	penawaran_lelang
    INNER JOIN user ON penawaran_lelang.idnik = user.idnik
    INNER JOIN lelang ON penawaran_lelang.id_lelang = lelang.id_lelang

		"); 
											$nomor=1;
                    while ($row1 = mysqli_fetch_assoc($sql1)) { 

                    ?>
                                            <tr>
                                                
                                                <td><?=$nomor++ ?></td>
												<td><?= $row1['nama'] ?></td>
                                                <td><?= $row1['nama_barang'] ?></td>
												<td><?= rupiah($row1['harga_penawaran']) ?></td>
												<td><?= $row1['waktu_penawaran'] ?></td>
                                                
                                              
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


<!--datatable js-->
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="assets/js/pages/datatables.init.js"></script>


