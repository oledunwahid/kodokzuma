<?php
session_start(); // Mulai sesi jika belum dimulai
require_once "../koneksi.php";

$id_penawaran_lelang = $_POST['id_penawaran_lelang'];
$status_penawaran = $_POST['status_penawaran'];
$whatsapp = $_POST['whatsapp']; // Mendapatkan nomor WhatsApp dari input form

function redirectToFacilitiesPage($message, $icon, $id_lelang)
{

    $id_lelang = $_POST['id_lelang'];
    $_SESSION["notif_message"] = $message;
    $_SESSION["notif_type"] = $icon;
    header("location:../index.php?page=ViewAuction&id=$id_lelang");
    exit();
}

// Lakukan kueri untuk mengubah status penawaran menjadi "confirm" hanya untuk pemenang lelang
$update_query = "UPDATE penawaran_lelang SET  status_penawaran = '$status_penawaran'  WHERE id_penawaran_lelang ='$id_penawaran_lelang' ";

$result = mysqli_query($koneksi, $update_query);

if ($result) {
    // Pesan yang akan dikirim melalui WhatsApp
    $message = "Selamat! Anda telah memenangkan lelang. Status penawaran Anda telah diubah menjadi menunggu konfirmasi. \n\n Mohon segera mengonfirmasi pada link berikut: http://localhost/eip.maagroup.co.id/index.php?page=ListBid\n\n Terima kasih.";

    // Mengatur pengiriman pesan melalui API WhatsApp
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.fonnte.com/send',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array(
            'target' => $whatsapp,
            'message' => $message,
            'countryCode' => '62', // Kode negara WhatsApp pengguna
        ),
        CURLOPT_HTTPHEADER => array(
            'Authorization: vXmxpJo3+5kVsDAWt!y+' // Ganti dengan token Anda
        ),
    ));

    // Melakukan request pengiriman pesan WhatsApp
    $response = curl_exec($curl);

    // Menutup koneksi cURL
    curl_close($curl);

    // Jika penawaran berhasil diperbarui, arahkan ke halaman dengan pesan sukses
    redirectToFacilitiesPage("Status lelang berhasil diperbarui!", "success", $lelang_id);
} else {
    // Jika terjadi kesalahan dalam kueri, arahkan ke halaman dengan pesan error
    $error_message = mysqli_error($koneksi);
    redirectToFacilitiesPage("Gagal memperbarui status: $error_message", "error", $lelang_id);
}
