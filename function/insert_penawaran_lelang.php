<?php 
include("../koneksi.php");
if(isset($_POST["masukan"])){
    date_default_timezone_set('Asia/Jakarta');

    $id_lelang = $_POST['id_lelang'];
    $idnik = $_POST['idnik'];
    $number = $_POST['harga_penawaran'];
    $waktu_penawaran = date('Y-m-d H:i:s'); 
    $harga_penawaran = str_replace(',', '', $number);

    // Cek apakah lelang sudah berakhir atau belum mulai
    $cek_lelang = "SELECT * FROM lelang WHERE id_lelang = '$id_lelang' AND tgl_lelang <= '$waktu_penawaran' AND end_tgl_lelang >= '$waktu_penawaran'";
    $hasil_cek = mysqli_query($koneksi, $cek_lelang);

    if (mysqli_num_rows($hasil_cek) > 0) {
        // Jika waktu sekarang berada di antara tgl_lelang dan end_tgl_lelang, proses penawaran
        $query = "INSERT INTO penawaran_lelang VALUES ('','$id_lelang','$idnik','$harga_penawaran','$waktu_penawaran')";
        $wali = mysqli_query($koneksi, $query);

        if ($wali) {
            session_start();
            $_SESSION["notif"] = "<script>
                window.onload = function(){
                    document.getElementById('saved').click();
                }
            </script>";
            header("location:../index.php?page=ViewAuction&id=$id_lelang");  
        } else {
            session_start();
            $_SESSION["notif"] = "<script>
                window.onload = function(){
                    document.getElementById('gagal').click();
                }
            </script>";
            header("location:../index.php?page=ViewAuction&id=$id_lelang");  
        }
    } else {
        // Jika lelang sudah lewat atau belum mulai
          session_start();
            $_SESSION["notif"] = "<script>
                window.onload = function(){
                    document.getElementById('gagal').click();
                }
            </script>";
            header("location:../index.php?page=ViewAuction&id=$id_lelang");
    }
} else {
    die("Akses dilarang...");
}
?>
