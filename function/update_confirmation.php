<?php
session_start();
require_once "../koneksi.php";

if (isset($_POST['confirm'])) {
    $id_lelang = $_POST['id_lelang'];
    // Lakukan pembaruan status penawaran menjadi "Approved"
    $query = "UPDATE penawaran_lelang SET status_penawaran = 'Approved' WHERE id_lelang = '$id_lelang'";
    $result = mysqli_query($koneksi, $query);
    if ($result) {
        $_SESSION['notif'] = "Status penawaran berhasil diperbarui menjadi Approved.";
    } else {
        $_SESSION['notif'] = "Terjadi kesalahan saat memperbarui status penawaran.";
    }
    header("location:../index.php?page=ListBid");
    exit();
} elseif (isset($_POST['cancel'])) {
    $id_lelang = $_POST['id_lelang'];
    // Hapus penawaran dari database
    $query = "DELETE FROM penawaran_lelang WHERE id_lelang = '$id_lelang'";
    $result = mysqli_query($koneksi, $query);
    if ($result) {
        // Jika pembatalan berhasil, update status menjadi "Canceled"
        $query_update_status = "UPDATE penawaran_lelang SET status_penawaran = 'Canceled' WHERE id_lelang = '$id_lelang'";
        $result_update_status = mysqli_query($koneksi, $query_update_status);
        if ($result_update_status) {
            $_SESSION['notif'] = "Penawaran berhasil dibatalkan dan status penawaran diperbarui menjadi Canceled.";
        } else {
            $_SESSION['notif'] = "Penawaran berhasil dibatalkan, tetapi terjadi kesalahan saat memperbarui status penawaran.";
        }
    } else {
        $_SESSION['notif'] = "Terjadi kesalahan saat membatalkan penawaran.";
    }
    header("location:../index.php?page=ListBid");
    exit();
}
