<link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/4.3.0/css/fixedColumns.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.4.2/css/buttons.dataTables.min.css">

<link href="https://cdn.datatables.net/searchbuilder/1.6.0/css/searchBuilder.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/datetime/1.5.1/css/dataTables.dateTime.min.css" rel="stylesheet" type="text/css" />


<script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedcolumns/4.3.0/js/dataTables.fixedColumns.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.html5.min.js"></script>


<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>


<script src="https://cdn.datatables.net/searchbuilder/1.6.0/js/dataTables.searchBuilder.min.js"></script>
<script src="https://cdn.datatables.net/datetime/1.5.1/js/dataTables.dateTime.min.js"></script>
<?php
// Mendapatkan protokol (http atau https)
$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http';

// Mendapatkan nama domain dan direktori
$domain = $_SERVER['HTTP_HOST'];
$directory = dirname($_SERVER['PHP_SELF']);

// Membentuk URL lengkap
$base_url = "{$protocol}://{$domain}{$directory}";


?>

<?php if (!empty($_SESSION["notif"])) {
    echo $_SESSION["notif"];
    unset($_SESSION["notif"]);
}    ?>
<div class="row">
    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <select id="tahun1">
                    <option value="semua">Semua Tahun</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                </select>

                <canvas id="dohChart"></canvas>
            </div>
        </div>
    </div>

    <div class="col-xl-6">
        <div class="card">
            <div class="card-body">
                <select id="tahun2">
                    <option value="semua">Semua Tahun</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                    <option value="2023">2023</option>
                    <option value="2024">2024</option>
                </select>

                <canvas id="resignChart"></canvas>
            </div>
        </div>
    </div>

    <!-- Kolom untuk chart Resign -->




    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex align-items-center">
                    <h5 class="card-title mb-0 flex-grow-1">Employee</h5>
                    <div class="flex-shrink-0">
                        <button class="btn btn-danger add-btn" data-bs-toggle="modal" data-bs-target="#UploadUser"><i class="ri-add-line align-bottom me-1"></i> Upload User</button>
                        <button class="btn btn-danger add-btn" data-bs-toggle="modal" data-bs-target="#UploadLogin"><i class="ri-add-line align-bottom me-1"></i> Upload Login</button>
                    </div>
                </div>
            </div>


            <div class="card-body">

                <table id="example" class="stripe row-border order-column" style="width:100%">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="checkAll" /></th>
                            <th>Idnik</th>
                            <th>Foto </th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Company</th>
                            <th>Lokasi</th>
                            <th>Divisi</th>
                            <th>Status</th>
                            <th>Join</th>
                            <th>Resign</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                </table>
            </div>
        </div>
    </div><!--end col-->


</div>


<div class="modal fade zoomIn" id="UploadUser" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-l">

        <div class="modal-content border-0">
            <div class="modal-header p-3 bg-soft-info">
                <h5 class="modal-title" id="exampleModalLabel">Upload DB User</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
            </div>
            <form action="function/upload_user.php" enctype="multipart/form-data" method="POST">
                <div class="modal-body">
                    <div class="row g-3">
                        <div class="col-lg-12">
                            <div id="modal-id">
                                <label for="orderId" class="form-label">Data Excel</label>
                                <input type="file" accept=application/vnd.openxmlformats-officedocument.spreadsheetml.sheet (.XLSX) class="form-control" name="fileuser" required />
                            </div>
                        </div>


                    </div>

                </div>
                <div class="modal-footer">
                    <div class="hstack gap-2 justify-content-end">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Upload DB User</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade zoomIn" id="UploadLogin" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-l">

        <div class="modal-content border-0">
            <div class="modal-header p-3 bg-soft-info">
                <h5 class="modal-title" id="exampleModalLabel">Upload DB Login</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
            </div>
            <form action="function/upload_login.php" enctype="multipart/form-data" method="POST">
                <div class="modal-body">
                    <div class="row g-3">
                        <div class="col-lg-12">
                            <div id="modal-id">
                                <label for="orderId" class="form-label">Data Excel</label>
                                <input type="file" class="form-control" name="filelogin" accept=application/vnd.openxmlformats-officedocument.spreadsheetml.sheet (.XLSX) required />
                            </div>
                        </div>


                    </div>

                </div>
                <div class="modal-footer">
                    <div class="hstack gap-2 justify-content-end">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Upload Login</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable({
            dom: 'QBfrtip',
            ajax: 'get-function/get-employee.php',
            columns: [

                {
                    "data": 'idnik',
                    "render": function(data, type, row) {
                        if (type === 'display') {
                            data = `<input type="checkbox" class="rowCheckbox" value="${row.idnik}"> `;

                        }
                        return data;
                    }
                },
                {
                    data: "idnik"
                },

                {
                    data: 'file_foto',
                    render: function(data, type, row) {
                        if (type === 'display') {
                            // Anda dapat menyesuaikan URL berikut sesuai dengan struktur direktori Anda
                            let imageUrl = "file/profile/" + data;
                            return `
                            <div class="avatar-group">
                                <a href="javascript: void(0);" class="avatar-group-item" data-img="${data}" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="${row.nama}">
                                    <img src="${imageUrl}" alt="" class="rounded-circle avatar-xs">
                                </a>
                            </div>`;
                        }
                        return data;
                    }
                },
                {
                    "data": 'nama',
                    "render": function(data, type, row) {
                        if (type === 'display') {
                            data = `<a href="<?php $base_url ?>/index.php?page=UpdateUser&id=${row.idnik}">${row.nama}</a>`;
                        }
                        return data;
                    }
                },
                {
                    data: "username"
                },

                {
                    data: "company"
                },
                {
                    data: "lokasi"
                },
                {
                    data: "divisi"
                },
                {
                    data: "status_login"
                },
                {
                    data: "doh"
                },
                {
                    data: "resign"
                },
                {
                    data: 'idnik',
                    render: function(data, type, row) {
                        if (type === 'display') {
                            return `
                            <div class="dropdown d-inline-block">
                                <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="ri-more-fill align-middle"></i>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-end">
                                    <li><a href="index.php?page=ViewProfile&id=${data}" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
                                    <li><a href="index.php?page=UpdateUser&id=${data}" class="dropdown-item edit-item-btn"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                                   
                                </ul>
                            </div>`;
                        }
                        return data;
                    }
                }
            ],
            lengthMenu: [
                [10, 25, 50, 100, -1],
                ['10 rows', '25 rows', '50 rows', '100 rows', 'Show all']
            ],
            buttons: ['pageLength',
                {
                    extend: 'fixedColumns',
                    text: 'FreezeKolom',
                    config: {
                        left: 2,
                        right: 0
                    }
                }, {
                    extend: 'excel',
                    title: 'Data Export IT Support'
                }
            ],
            deferRender: true
        });

        $(document).on('click', '#checkAll', function() {
            var rows = table.rows({
                'search': 'applied'
            }).nodes();
            $('.rowCheckbox', rows).prop('checked', this.checked);
        });
        $('#btnDelete').click(function() {
            var ids = [];
            $('.rowCheckbox:checked').each(function() {
                ids.push($(this).val());
            });

            if (ids.length > 0) {
                if (confirm("Apakah Anda yakin ingin menghapus data terpilih?")) {
                    // Kirim request ke server untuk menghapus data
                    $.ajax({
                        url: 'delete.php',
                        type: 'POST',
                        data: {
                            deleteIds: ids
                        },
                        success: function(response) {
                            table.ajax.reload();
                        }
                    });
                }
            } else {
                alert("Please select at least one row to delete.");
            }
        });
    });
</script>





<script>
    let chart1; // Variabel untuk chart DOH/Join

    $(document).ready(function() {
        // Event handler untuk select box DOH/Join
        $('#tahun1').change(function() {
            loadChartData1();
        });

        // Memuat data awal untuk grafik DOH/Join
        loadChartData1();
    });

    function loadChartData1() {
        var tahun = $('#tahun1').val(); // Mengambil tahun dari select box DOH/Join

        $.ajax({
            url: 'get-function/get-chart-employee-join.php',
            method: 'GET',
            data: {
                tahun: tahun
            },
            success: function(response) {
                var data = JSON.parse(response);
                updateDohChart(data);
            }
        });
    }

    function updateDohChart(data) {
        var labels = data.map(function(item) {
            return item.bulan_tahun;
        });
        var dohData = data.map(function(item) {
            return item.jumlah_doh;
        });

        if (chart1) {
            chart1.destroy();
        }

        chart1 = new Chart(document.getElementById('dohChart'), {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Jumlah Join (DOH)',
                    data: dohData,
                    backgroundColor: 'rgba(0, 123, 255, 0.5)'
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    }
</script>
<script>
    let chart2; // Variabel untuk chart Resign

    $(document).ready(function() {
        // Event handler untuk select box Resign
        $('#tahun2').change(function() {
            loadChartData2();
        });

        // Memuat data awal untuk grafik Resign
        loadChartData2();
    });

    function loadChartData2() {
        var tahun = $('#tahun2').val(); // Mengambil tahun dari select box Resign

        $.ajax({
            url: 'get-function/get-chart-employee-resign.php',
            method: 'GET',
            data: {
                tahun: tahun
            },
            success: function(response) {
                var data = JSON.parse(response);
                updateResignChart(data);
            }
        });
    }

    function updateResignChart(data) {
        var labels = data.map(function(item) {
            return item.bulan_tahun;
        });
        var resignData = data.map(function(item) {
            return item.jumlah_resign;
        });

        if (chart2) {
            chart2.destroy();
        }

        chart2 = new Chart(document.getElementById('resignChart'), {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Jumlah Resign',
                    data: resignData,
                    backgroundColor: 'rgba(255, 99, 132, 0.5)'
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    }
</script>