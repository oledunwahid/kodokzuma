<?php 	$query = mysqli_query($koneksi, "SELECT max(idapply) as kodeTerbesar FROM apply_refferal");
	$data = mysqli_fetch_array($query);
	$kodeid = $data['kodeTerbesar'];
	$urutan = (int) substr($kodeid, 3, 6);
 
	// bilangan yang diambil ini ditambah 1 untuk menentukan nomor urut berikutnya
	$urutan++;
 
	
	$huruf = "APL";
	$kodeid= $huruf . sprintf("%06s", $urutan);
	?>


<section class="py-5 bg-danger position-relative mb-4">
            <div class="bg-overlay bg-overlay-pattern opacity-50"></div>
            <div class="container">
                <div class="row align-items-center gy-4">
                    <div class="col-sm">
                        <div>
                            <h4 class="text-white mb-0 fw-semibold">Employee Refferal Programs</h4>
                        </div>
                    </div>
					
                    <!-- end col -->
                    <div class="col-sm-auto">
                        <div>
                            <a href="file/job/EMPLOYEE REFERRAL PROGRAM.pdf" class="btn bg-gradient btn-danger">Syarat dan Ketentuan</a>
                           
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </section>


                            
                        <div class="d-flex align-items-center mb-4">
							<?php if ($_SESSION['role'] == 'admin') { ?>
                                    <div class="flex-grow-1">
                                         <a href="index.php?page=AddJob" class="btn bg-gradient btn-info">Add Job Application</a>
                                    </div>
						<?php }?>
							 <div class="col-sm">
                            <div class="d-flex justify-content-sm-end gap-2">
                                <div class="search-box ms-2">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <i class="ri-search-line search-icon"></i>
                                </div>

                                
                            </div>
                        </div>
                                    
						</div>
											<?php if(!empty($_SESSION["notif"])){
										   echo $_SESSION["notif"];
										   unset($_SESSION["notif"]);
									   		}	?>
 
<div class="row">
<?php 
	$batas = 9;
	$halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
	$halaman_awal = ($halaman>1) ? ($halaman * $batas) - $batas : 0;	
	$previous = $halaman - 1;
	$next = $halaman + 1;
	$data = mysqli_query($koneksi,"select * from job");
	$jumlah_data = mysqli_num_rows($data);
	$total_halaman = ceil($jumlah_data / $batas);
	
	
	
	$sql = mysqli_query($koneksi, "SELECT * FROM job  ORDER BY idjob ASC limit $halaman_awal, $batas ");   
	$nomor = $halaman_awal+1;
	while ($row = mysqli_fetch_assoc($sql)) { ?>

<div class="col-lg-4 col-md-12">        
	<div class="card">      
		
		<div class="card-body">  
			
			 <div class="d-flex align-items-center mb-3">
			<div class="flex-shrink-0 me-3">
				<div class="avatar-sm  rounded p-1"><img src="file/refferal/logo-refferal.jpg" alt="" class="img-fluid d-block">
				</div>
				
			</div>
				 
			<div class="flex-grow-1">
				<h5 class="fs-14 mb-1">MAA Group</h5>
				
			</div>
				<?php if ($_SESSION['role'] == 'admin') { ?>
				 <div class="col text-end dropdown">
                                                        <a href="javascript:void(0);" id="dropdownMenuLink14" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <i class="ri-more-fill fs-17"></i>
                                                        </a>
                                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink14">
                                                            <li><a class="dropdown-item" href="index.php?page=UpdateJob&id=<?=$row['idjob']?>"><i class="ri-edit-2-line align-bottom me-2 text-muted"></i>Edit</a></li>
                                                            
                                                            <li><a class="dropdown-item"data-bs-toggle="modal" data-bs-target="#delete<?=$row['idjob']?>"><i class="ri-delete-bin-5-line me-2 align-middle"></i>Delete</a></li>
                                                        </ul>
                                                    </div><?php }?>
		</div>
			
			<a href="#!"><h5><?=$row['posisi_job']?></h5></a>                
			<p class="text-muted"><?=$row['namapt']?></p>                
			<div class="d-flex gap-4 mb-3">                    
				<div><i class="ri-map-pin-2-line text-primary me-1 align-bottom"></i> <?=$row['lokasi_job']?></div>                    
				<div><i class="ri-time-line text-primary me-1 align-bottom"></i> <?=$row['tgl_job']?></div>                
			</div>                
			<p class="text-muted"><?= substr($row['diskripsi'], 0, 200) ?>.....</p>                
			<div class="hstack gap-2">
				<span class="badge badge-soft-success"><?=$row['status_job']?></span>
		</div>          
			<div class="mt-4 hstack gap-2">                    
				<button  class="btn btn-soft-primary w-100" data-bs-toggle="modal" data-bs-target="#apply<?=$row['idjob']?>" >Apply Job</button>                    
				<a href="index.php?page=ViewJob&id=<?=$row['idjob'] ?>" class="btn btn-soft-success w-100">Overview</a>                
			</div>            
		</div>
		
	</div>    
</div>
	 <?php } ?>

  </div>



					<?php
						$jumlah_number =1;
						$start_number = ($halaman > $jumlah_number)? $halaman - $jumlah_number : 1;
      					$end_number = ($halaman < ($total_halaman - $jumlah_number))? $halaman + $jumlah_number : $total_halaman;	?>
				<div class="row g-0 text-center text-sm-start align-items-center mb-3">
					<div class="col-sm-6">
                            <div>
                                <p class="mb-sm-0">Showing</p>
                            </div>
                        </div> <!-- end col -->
                        <div class="col-sm-6">	
			<ul class="pagination pagination-separated justify-content-center justify-content-sm-end mb-sm-0">
				<li class="page-item">
					<a class="page-link" <?php if($halaman > 1){ 
						echo "href='?page=Refferal&halaman=$previous'"; } ?>>Previous</a>
				</li>
				<?php 
				for($x = $start_number; $x <= $end_number;$x++){ 
					$link_active = ($halaman == $x)? ' active' : '';
					?> 
					<li class="page-item<?=$link_active?>"><a class="page-link" href="?page=Refferal&halaman=<?php echo $x ?>"><?php echo $x; ?></a></li>
					<?php
				}
				?>				
				<li class="page-item">
					<a  class="page-link" <?php if($halaman < $total_halaman) { echo "href='?page=Refferal&halaman=$next'"; } ?>>Next</a>
				</li>
			</ul>
		</div>
</div>



                           

<?php $sql = mysqli_query($koneksi, "SELECT * FROM job ");

    while ($row = mysqli_fetch_assoc($sql)) { ?>


<div class="modal fade zoomIn" id="apply<?=$row['idjob']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-l">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Apply Job</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/insert_apply_refferal.php" method="POST" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-6">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">No.Apply</label>
                                                    <input type="text"  name="idnik" value="<?=$niklogin?>" hidden>
													<input type="text"  name="idjob" value="<?=$row['idjob']?>" hidden >
													 <input type="text" class="form-control" name="idapply" value="<?=$kodeid?>" readonly required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Tanggal Apply</label>
                                                    <input type="date"  class="form-control" value="<?php echo "$tgl" ?>" name="tgl_apply" readonly required />
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Nama Referensi</label>
                                                    <input type="text" class="form-control" placeholder="Nama Referensi" name="nama_apply" required />
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Hubungan Referensi</label>
                                                    <input type="text" class="form-control" placeholder="Hubungan Referensi" name="hub_apply" required />
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Kontak Referensi</label>
                                                    <input type="text" class="form-control" placeholder="Kontak Referensi" name="kontak_apply" required />
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">File Lamaran</label>
                                                    <input type="file" class="form-control"  name="file_apply" required />
                                                </div>
                                            </div>
                                            
                                           
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Apply Now</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


<div class="modal fade zoomIn" id="delete<?=$row['idjob']?>" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="btn-close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="mt-2 text-center">
                                        <lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>
                                        <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                                            <h4>Are you Sure ?</h4>
                                            <p class="text-muted mx-4 mb-0">Are you Sure You want to Remove this Record <br> <b><?=$row['posisi_job']; ?></b> ?</p>
                                        </div>
                                    </div>
                                    <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                                        <button type="button" class="btn w-sm btn-light" data-bs-dismiss="modal">Close</button>
										<a style="cursor: pointer;" onclick="location.href='function/delete_job.php?aksi=delete&id=<?=$row['idjob']; ?>'" class="btn w-sm btn-danger">Yes, Delete It!</a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
 
<?php } ?>