 <?php $page =  $_GET['page']; ?>
<div class="app-menu navbar-menu">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <!-- Dark Logo-->
                <a href="index.php?page=Dashboard" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="../assets/images/logo_MAA.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="assets/images/logo_MAAA.png" alt="" height="35">
                    </span>
                </a>
                <!-- Light Logo-->
                <a href="index.php?page=Dashboard" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="assets/images/logo_MAA.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="assets/images/logo_MAAA.png" alt="" height="35">
                    </span>
                </a>
                <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
                    <i class="ri-record-circle-line"></i>
                </button>
            </div>

            <div id="scrollbar">
                <div class="container-fluid">

                    <div id="two-column-menu">
                    </div>
					<?php if ($_SESSION['role'] == 'admin') { ?>
                    <ul class="navbar-nav" id="navbar-nav">
                        <li class="menu-title"><span data-key="t-menu">Menu</span></li>
                        <li class="nav-item">
                    <a class="nav-link menu-link<?php if ($page == 'Dashboard') echo ' active'; ?>" href="index.php?page=Dashboard" aria-expanded="false">
                        <i class="ri-dashboard-2-line"></i> <span> Dashboard </span>
                    </a>
                </li>
						
						  <li class="nav-item">
                            <a class="nav-link menu-link<?php if ($page == 'Company') echo ' active'; ?>" href="index.php?page=Company&id=1" aria-expanded="false">
                                <i class="ri-book-line"></i> <span> Company Regulation </span>
                            </a>
                        </li>
						 
						<li class="nav-item">
        <a class="nav-link menu-link<?php if ($page == 'SopForm' || $page == 'InternalMemo' || $page == 'Policy') echo ' active'; ?>" href="#Sop" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSop">
            <i class="ri-shield-check-line"></i> <span>Policy, SOP & Form</span>
        </a>
        <div class="collapse <?php if ($page == 'Policy' || $page == 'SopForm' || $page == 'InternalMemo') echo 'show'; ?> menu-dropdown" id="Sop">
            <ul class="nav nav-sm flex-column">
                <li class="nav-item">
                    <a href="index.php?page=Policy" class="nav-link <?php if ($page == 'Policy') echo 'active'; ?>">Policy</a>
                </li>
                <li class="nav-item">
                    <a href="index.php?page=SopForm" class="nav-link <?php if ($page == 'SopForm') echo 'active'; ?>">SOP & Form</a>
                </li>
                <li class="nav-item">
                    <a href="index.php?page=InternalMemo" class="nav-link <?php if ($page == 'InternalMemo') echo 'active'; ?>">Internal Memo</a>
                </li>
            </ul>
        </div>
    </li>
						 <li class="nav-item">
        <a class="nav-link menu-link<?php if ($page == 'News') echo ' active'; ?>" href="index.php?page=News" aria-expanded="false">
            <i class="ri-newspaper-line"></i> <span>News</span>
        </a>
    </li>
						
						 <li class="nav-item">
        <a class="nav-link menu-link<?php if ($page == 'IdeaBox') echo ' active'; ?>" href="index.php?page=IdeaBox" aria-expanded="false">
            <i class="ri-lightbulb-line"></i> <span>Idea-Box</span>
        </a>
    </li>
						<li class="nav-item">
	<a class="nav-link menu-link<?php if ($page == 'Refferal' || $page == 'ListApply' || $page == 'ListAllApply') echo ' active'; ?>" href="#Refferal" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="Refferal">
        <i class="ri-group-line"></i> <span>Employee Referral</span>
    </a>
	
							
    
    <div class="collapse menu-dropdown<?php if ($page == 'Refferal' || $page == 'ListApply' || $page == 'ListAllApply') echo ' show'; ?>" id="Refferal">
        <ul class="nav nav-sm flex-column">
            <li class="nav-item">
                <a href="index.php?page=Refferal" class="nav-link<?php if ($page == 'Refferal') echo ' active'; ?>">Job List</a>
            </li>
			
            <li class="nav-item">
                <a href="index.php?page=ListApply" class="nav-link<?php if ($page == 'ListApply') echo ' active'; ?>">Apply List</a>
            </li>
            <?php if ($menulogin == '9' || $menulogin == '3') { ?>
            <li class="nav-item">
                <a href="index.php?page=ListAllApply" class="nav-link<?php if ($page == 'ListAllApply') echo ' active'; ?>">View All Apply</a>
            </li>
            <?php } ?>
        </ul>
    </div>
</li>



						<li class="nav-item">
    <a class="nav-link menu-link<?php if ($page == 'Learning' || $page == 'ListLearning' || $page == 'ListAllLearning') echo ' active'; ?>" href="#Elearning" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="Elearning">
        <i class="ri-team-line"></i> <span>E-learning</span>
    </a>
							
    <div class="collapse menu-dropdown<?php if ($page == 'Learning' || $page == 'ListLearning' || $page == 'ListAllLearning') echo ' show'; ?>" id="Elearning">
        <ul class="nav nav-sm flex-column">
            <li class="nav-item">
                <a href="index.php?page=Learning" class="nav-link<?php if ($page == 'Learning') echo ' active'; ?>">E-learning List</a>
            </li>
            <li class="nav-item">
                <a href="index.php?page=ListLearning" class="nav-link<?php if ($page == 'ListLearning') echo ' active'; ?>">My Materi</a>
            </li>
            <?php if ($menulogin == '9' || $menulogin == '4') { ?>
            <li class="nav-item">
                <a href="index.php?page=ListAllLearning" class="nav-link<?php if ($page == 'ListAllLearning') echo ' active'; ?>">View User E-learning</a>
            </li>
            <?php } ?>
        </ul>
    </div>
</li>

						
						
						
						
                     
                        <li class="menu-title"><i class="ri-more-fill"></i> <span data-key="t-components">Self Service</span></li>
    <!-- Conditional items based on login -->
    <?php if ($menulogin == '9' || $menulogin == '6') { ?>
    <li class="nav-item">
        <a class="nav-link menu-link<?php if ($page == 'Employee') echo ' active'; ?>" href="index.php?page=Employee" aria-expanded="false">
            <i class="ri-user-3-line"></i> <span>Employee</span>
        </a>
    </li>
    <?php } ?>
    <li class="nav-item">
        <a class="nav-link menu-link" href="./tiket/index.php?page=Dashboard" aria-expanded="false">
            <i class="ri-building-line"></i> <span>Request Facility</span>
        </a>
    </li>
    <?php if ($menulogin == '9' || $menulogin == '5') { ?>
    <li class="nav-item">
        <a class="nav-link menu-link" href="./masset/index.php?page=Dashboard" aria-expanded="false">
            <i class="ri-bank-line"></i> <span>Asset Management</span>
        </a>
    </li>
						

                      <?php }  ?>


                    </ul>
				<?php	} ?>
					<?php if ($_SESSION['role'] == 'user') { ?>
                    <ul class="navbar-nav" id="navbar-nav">
                        <li class="menu-title"><span data-key="t-menu">Menu</span></li>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($page == 'Dashboard') echo 'active'; ?>" href="index.php?page=Dashboard">
                                <i class="ri-dashboard-2-line"></i> Dashboard
                            </a>
                        </li>
						  <li class="nav-item">
                            <a class="nav-link <?php if ($page == 'Company') echo 'active'; ?>" href="index.php?page=Company&id=1">
                                <i class="ri-dashboard-2-line"></i> Company Regulation
                            </a>
                        </li>
						 
						<li class="nav-item">
                            <a class="nav-link <?php if ($page == 'SopForm' || $page == 'InternalMemo' ) echo 'active'; ?>" href="#Sop" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarRefferal">
                               
                                <i class="ri-dashboard-2-line"></i> Policy, SOP & Form
                            </a>
							<div class="collapse <?php if ($page == 'Policy' || $page == 'SopForm' || $page == 'InternalMemo' ) echo 'show'; ?> menu-dropdown" id="Sop">
                                <ul class="nav nav-sm flex-column">
									<li class="nav-item">
                                        <a href="index.php?page=Policy" class="nav-link <?php if ($page == 'Policy') echo 'active'; ?>" > Policy </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="index.php?page=SopForm" class="nav-link <?php if ($page == 'SopForm') echo 'active'; ?>" > SOP & Form </a>
                                    </li>
									 <li class="nav-item">
                                        <a href="index.php?page=InternalMemo" class="nav-link <?php if ($page == 'InternalMemo') echo 'active'; ?>" > Internal Memo </a>
                                    </li>
                                    
                                    
                                    
                                </ul>
                            </div>
                        </li>
						<li class="nav-item">
                            <a class="nav-link <?php if ($page == 'News') echo 'active'; ?>" href="index.php?page=News">
                                <i class="ri-dashboard-2-line"></i> News
                            </a>
                        </li>
						
						<li class="nav-item">
                            <a class="nav-link <?php if ($page == 'IdeaBox') echo 'active'; ?>" href="index.php?page=IdeaBox">
                                <i class="ri-dashboard-2-line"></i> Idea-Box
                            </a>
                        </li>
						<li class="nav-item">
                            <a class="nav-link <?php if ($page == 'Refferal' || $page == 'ListApply'|| $page == 'ListAllApply' ) echo 'active'; ?>" href="#Refferal" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarRefferal">
                               
                                <i class="ri-dashboard-2-line"></i> Employee Refferal
                            </a>
							<div class="collapse <?php if ($page == 'Refferal' || $page == 'ListApply'|| $page == 'ListAllApply' ) echo 'show'; ?> menu-dropdown" id="Refferal">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="index.php?page=Refferal" class="nav-link <?php if ($page == 'Refferal') echo 'active'; ?>" > Job List </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="index.php?page=ListApply" class="nav-link <?php if ($page == 'ListApply') echo 'active'; ?>" > Apply List</a>
                                    </li>
                                    
                                    
                                </ul>
                            </div>
                        </li>
						<li class="nav-item">
                            <a class="nav-link <?php if ($page == 'Learning' ) echo 'active'; ?> menu-link" href="#Elearning" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarDashboards">
                               
                                <i class="ri-dashboard-2-line"></i> E-learning
                            </a>
							<div class="collapse <?php if ($page == 'Learning' || $page == 'ListLearning' ) echo 'show'; ?> menu-dropdown" id="Elearning">
                                <ul class="nav  nav-sm flex-column ">
                                    <li class="nav-item">
                                        <a href="index.php?page=Learning" class="nav-link  <?php if ($page == 'Learning') echo 'active'; ?>" > E-learning List </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="index.php?page=ListLearning" class="nav-link  <?php if ($page == 'ListLearning') echo 'active'; ?>"> My Materi</a>
                                    </li>
                                    
                                    
                                </ul>
                            </div>
                        </li>
						<li class="nav-item">
    <a class="nav-link<?php if ($page == 'Auction' || $page == 'ListBid' || $page == 'ListAllBid') echo ' active'; ?>" href="#Lelang" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="Lelang">
        <i class="ri-dashboard-2-line"></i> <span>Auction Program</span>
    </a>
    <div class="collapse menu-dropdown<?php if ($page == 'Auction' || $page == 'ListBid' || $page == 'ListAllBid') echo ' show'; ?>" id="Lelang">
        <ul class="nav nav-sm flex-column">
            <li class="nav-item">
                <a href="index.php?page=Auction" class="nav-link<?php if ($page == 'Auction') echo ' active'; ?>">Auction</a>
            </li>
            <li class="nav-item">
                <a href="index.php?page=ListBid" class="nav-link<?php if ($page == 'ListBid') echo ' active'; ?>">List Bid</a>
            </li>
            <?php if ($menulogin == '9' || $menulogin == '5') { ?>
            <li class="nav-item">
                <a href="index.php?page=ListAllBid" class="nav-link<?php if ($page == 'ListAllBid') echo ' active'; ?>">List All Bid</a>
            </li>
            <?php } ?>
        </ul>
    </div>
</li>

                        
                        <li class="nav-item">
                         <a class="nav-link"  href="./tiket/index.php?page=Dashboard">
                                <i class="ri-dashboard-2-line"></i> Request Facility
                            </a>
                        </li>
				
						
                                </ul>
				<?php	} ?>
                </div>
                <!-- Sidebar -->
            </div>

            <div class="sidebar-background"></div>
        </div>