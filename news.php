 <div class="row g-4 mb-3">
                        <div class="col-sm-auto">
							<?php if ($_SESSION['role'] == 'admin') { ?>
                            <div>
                                <a href="index.php?page=AddNews" class="btn btn-success"><i class="ri-add-line align-bottom me-1"></i> Add New</a>
                            </div>
						<?php	}?>
                        </div>
                        <div class="col-sm">
                            <div class="d-flex justify-content-sm-end gap-2">
                                <div class="search-box ms-2">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <i class="ri-search-line search-icon"></i>
                                </div>

                                
                            </div>
                        </div>
                    </div>
<div class="row">
	
	<?php 
	$batas = 12;
	$halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
	$halaman_awal = ($halaman>1) ? ($halaman * $batas) - $batas : 0;	
	$previous = $halaman - 1;
	$next = $halaman + 1;
	$data = mysqli_query($koneksi,"select * from news");
	$jumlah_data = mysqli_num_rows($data);
	$total_halaman = ceil($jumlah_data / $batas);
	
	
	
	
	$sql = mysqli_query($koneksi, "SELECT *, user.nama, user.file_foto FROM news inner join user ON user.idnik = news.idnik ORDER BY idnews ASC limit $halaman_awal, $batas "); // query jika filter dipilih
    $nomor = $halaman_awal+1;
	while ($row = mysqli_fetch_assoc($sql)) { // fetch query yang sesuai ke dalam array
    $tanggalnews = $row['tgl_news'];

	?>
	
	<?php if(!empty($_SESSION["notif"])){
										   echo $_SESSION["notif"];
										   unset($_SESSION["notif"]);
									   		}	?>
 <div class="col-xxl-3 col-sm-12 mb-3 project-card">
                                    <div class="card tasks-box">
                                        <div class="card-body">
                                            <div class="d-flex mb-2">
                                                <a href="javascript:void(0)" class="text-muted fw-medium fs-14 flex-grow-1">#<?=$row['idnews']?></a>
                                                <div class="dropdown">
                                                    <a href="javascript:void(0);" class="text-muted" id="dropdownMenuLink7" data-bs-toggle="dropdown" aria-expanded="false"><i class="ri-more-fill"></i></a>
                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink7">
                                                        <li><a class="dropdown-item" href="index.php?page=ViewNews&id=<?=$row['idnews']?>"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
														<?php if ($_SESSION['role'] == 'admin') { ?>
                                                        <li><a class="dropdown-item" href="index.php?page=UpdateNews&id=<?=$row['idnews']?>"><i class="ri-edit-2-line align-bottom me-2 text-muted"></i> Edit</a></li>
                                                        <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#delete<?=$row['idnews']?>"><i class="ri-delete-bin-5-line align-bottom me-2 text-muted"></i> Delete</a></li>
														
														<?php }?>
														
                                                    </ul>
                                                </div>
                                            </div>							
											<div class="dash-collection overflow-hidden rounded-top position-relative">
											<?php
											$file_media =  substr($row['image_news'],-3);
											if($file_media  == 'mp4'){?>
										 <video height="220" controls>
 											 <source src="file/news/<?=$row['image_news']?>" type="video/mp4">
										</video> 

											<?php 
											}else{?> 
											 <img  height="220" src="file/news/<?=$row['image_news']?>" alt="" />
											<?php }?>
												
												<div class="content position-absolute bottom-0 m-2 p-2 start-0 end-0 rounded d-flex align-items-center">
                                                        <div class="flex-grow-1">
                                                            <a href="#!">
                                                                <p class="text-white fs-16 mb-1"><?=$row['title']?></p>
                                                            </a>
                                                           
                                                        </div>
                                                        
                                                    </div>
											</div>	
												
                                            <p class="text-muted"><?= substr($row['content'], 0, 50) ?></p>
											<!-- Rounded Image -->
                                            <div class="d-flex align-items-center">
                                                <div class="flex-grow-1">
                                                    <span class="badge badge-soft-primary"><?=$row['category']?></span>
                                                </div>
                                                <div class="flex-shrink-0">
                                                    <div class="avatar-group">
                                                        <a href="javascript: void(0);" class="avatar-group-item" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-placement="top" title="<?=$row['nama']?>">
                                                            <img src="file/profile/<?=$row['file_foto']?>" alt="" class="rounded-circle avatar-xxs">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer border-top-dashed">
                                            <div class="d-flex">
                                                <div class="flex-grow-1">
                                                    <span class="text-muted"><i class="ri-time-line align-bottom"></i> <?=date('d F Y', strtotime("$tanggalnews"));?></span>
                                                </div>
                                                
                                            </div>
                                        </div>                    
                            </div>
	              </div>          
	<?php } ?>
	 </div>


					<?php
						$jumlah_number =1;
						$start_number = ($halaman > $jumlah_number)? $halaman - $jumlah_number : 1;
      					$end_number = ($halaman < ($total_halaman - $jumlah_number))? $halaman + $jumlah_number : $total_halaman;	?>
				<div class="row g-0 text-center text-sm-start align-items-center mb-3">
					<div class="col-sm-6">
                            <div>
                                <p class="mb-sm-0">Showing</p>
                            </div>
                        </div> <!-- end col -->
                        <div class="col-sm-6">	
			<ul class="pagination pagination-separated justify-content-center justify-content-sm-end mb-sm-0">
				<li class="page-item">
					<a class="page-link" <?php if($halaman > 1){ 
						echo "href='?page=News&halaman=$previous'"; } ?>>Previous</a>
				</li>
				<?php 
				for($x = $start_number; $x <= $end_number;$x++){ 
					$link_active = ($halaman == $x)? ' active' : '';
					?> 
					<li class="page-item<?=$link_active?>"><a class="page-link" href="?page=News&halaman=<?php echo $x ?>"><?php echo $x; ?></a></li>
					<?php
				}
				?>				
				<li class="page-item">
					<a  class="page-link" <?php if($halaman < $total_halaman) { echo "href='?page=News&halaman=$next'"; } ?>>Next</a>
				</li>
			</ul>
		</div>
</div>
		
		
		
	
	<?php $sql = mysqli_query($koneksi, "SELECT * FROM news ");

    while ($row = mysqli_fetch_assoc($sql)) { ?>
<div class="modal fade zoomIn" id="delete<?=$row['idnews']?>" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="btn-close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="mt-2 text-center">
                                        <lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>
                                        <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                                            <h4>Are you Sure ?</h4>
                                            <p class="text-muted mx-4 mb-0">Are you Sure You want to Remove this Record <br> <b><?=$row['title']; ?></b> ?</p>
                                        </div>
                                    </div>
                                    <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                                        <button type="button" class="btn w-sm btn-light" data-bs-dismiss="modal">Close</button>
										<a style="cursor: pointer;" onclick="location.href='function/delete_news.php?aksi=delete&id=<?=$row['idnews']; ?>'" class="btn w-sm btn-danger">Yes, Delete It!</a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
	<?php }?>
                           