<?php


$sql = mysqli_query($koneksi, "SELECT *, user.nama FROM idea inner join user ON user.idnik = idea.idnik WHERE ididea='" . $_GET['id'] . "' ");

$row = mysqli_fetch_assoc($sql)

?>
<?php $ididea = $_GET['id']; ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card mt-n4 mx-n4 mb-n5">
                                <div class="bg-soft-warning">
                                    <div class="card-body pb-4 mb-5">
                                        <div class="row">
                                            <div class="col-md">
                                                <div class="row align-items-center">
                                                    <div class="col-md-auto">
                                                        <div class="avatar-md mb-md-0 mb-4">
                                                            <div class="avatar-title bg-white rounded-circle">
                                                                <img src="file/refferal/logo-refferal.jpg" alt="" class="avatar-sm" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-md">
                                                        <h4 class="fw-semibold" id="ticket-title">#<?=$row['ididea']?> - <?=$row['judul_idea']?></h4>
                                                        <div class="hstack gap-3 flex-wrap">
                                                         
                                                            <div class="text-muted">User Write <span class="fw-medium " id="create-date"><?=$row['nama']?></span></div>
                                                            <div class="vr"></div>
                                                            <div class="text-muted">Create Date : <span class="fw-medium" id="due-date"><?=date(('d F Y'),strtotime($row['tgl_idea']))?></span></div>
                                                           
                                                            
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                            <!--end col-->
                                            
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                    </div><!-- end card body -->
                                </div>
                            </div><!-- end card -->
                        </div><!-- end col -->
                    </div><!-- end row -->

                    <div class="row">
                        <div class="col-xxl-9">
                            <div class="card">
                                <div class="card-body p-4">
                                    <h6 class="fw-semibold text-uppercase mb-3">Describe About Your Idea ?</h6>
                                    <?=$row['describe_idea']?>
                                    <h6 class="fw-semibold text-uppercase mb-3">Whats Your Reason for Create This Idea ?</h6>
                                    <?=$row['reason_idea']?>
                                    <div class="mt-4">
                                        <h6 class="fw-semibold text-uppercase mb-3">Whats Your Goals For Your Idea ?</h6>
                                         <?=$row['goals_idea']?>
                                    </div>
                                </div>
                                <!--end card-body-->
                                <div class="card-body p-4">
                                    <h5 class="card-title mb-4">Comments</h5>
									
                                    <div data-simplebar style="height: 200px;" class="px-3 mx-n3">
										  <?php $sql = mysqli_query($koneksi, "SELECT *, user.nama, user.file_foto FROM comment_idea INNER JOIN user ON user.idnik = comment_idea.idnik WHERE ididea ='".$_GET['id']."'  "); 
											
											while ($row1 = mysqli_fetch_assoc($sql)) { 

											?>
											<div class="d-flex mb-4">
                                            <div class="flex-shrink-0">
                                                <img src="file/profile/<?=$row1['file_foto']?>" alt="" class="avatar-xs rounded-circle" />
                                            </div>
                                            <div class="flex-grow-1 ms-3">
                                                <h5 class="fs-13"><?=$row1['nama']?> <small class="text-muted"><?=$row1['tgl_com_idea']?></small></h5>
                                                <p class="text-muted"><?=$row1['describe_com_idea']?></p>
                                               
                                                
                                            </div>
                                        </div>
										<?php }?>
                                        
                                        
                                    </div>
                                    <form action="function/insert_comment_idea.php" method="post" class="mt-3">
                                        <div class="row g-3">
                                            <div class="col-lg-12">
                                                <label for="exampleFormControlTextarea1" class="form-label">Leave a Comments</label>
												<input type="text" value="<?=$niklogin?>" name="idnik" hidden>
												<input type="text" value="<?=$_GET['id']?>" name="ididea" hidden >
                                                <textarea class="form-control bg-light border-light" name="describe_comm_idea" rows="3" placeholder="Enter comments"></textarea>
                                            </div>
                                            <div class="col-lg-12 text-end">
                                                <button type="submit" name="masukkan" class="btn btn-success">Post Comments</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- end card body -->
                            </div>
                            <!--end card-->
                        </div>
                        <!--end col-->
                        
                        <!--end col-->
                    </div>
                    <!--end row-->

          