<style type="text/css" media="print, handheld">
    * { display: none; }
</style>

<?php

$pp 	  = $_GET['id'];
$idpolicy = '';
$idsop    = '';
$idim	  = '';
$page     = $_GET['page'];
$cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM histori WHERE idnik='$niklogin' AND pp='$pp'"));

if ($cek > 0) {
    echo 'pp Pernah Dilihat';
} else {
    $query = "INSERT INTO histori 
				VALUES 
				('','$niklogin','$pp','$idpolicy','$idsop','$idim','$page')";
    $wali = mysqli_query($koneksi, $query);
}
?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card mt-n4 mx-n4 mb-n5">
                                <div class="bg-soft-warning">
                                    <div class="card-body pb-4 mb-5">
                                        <div class="row">
                                            <div class="col-md">
                                                <div class="row align-items-center">
                                                    <div class="col-md-auto">
                                                        <div class="avatar-md mb-md-0 mb-4">
                                                            <div class="avatar-title bg-white rounded-circle">
                                                                <img src="file/refferal/logo-refferal.jpg" alt="" class="avatar-sm" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-md">
                                                        <h4 class="fw-semibold" >Peraturan Perusahaan Periode 2023-2025 </h4>
                                                        <div class="hstack gap-3 flex-wrap">
                                                            
                                                           
                                                            <div class="text-muted">Create Date : <span class="fw-medium " id="create-date">07 Jul, 2023</span></div>
                                                            <div class="vr"></div>
                                                            <div class="text-muted">Due Date : <span class="fw-medium" id="due-date">07 Jul, 2025</span></div>
                                                            <div class="vr"></div>
                                                            <div class="badge rounded-pill bg-info fs-12" id="ticket-status">New</div>
                                                            
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                            <!--end col-->
                                            
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                    </div><!-- end card body -->
                                </div>
                            </div><!-- end card -->
                        </div><!-- end col -->
                    </div><!-- end row -->

                    <div class="row">
                        <div class="col-xxl-9">
                            <div class="card">
                                
                                <!--end card-body-->
                                <div class="card-body p-4">
                                    <h5 class="card-title mb-4">Company Regulation</h5>
                                    
                                    <iframe src="https://drive.google.com/file/d/1rE6QOn081OuSYPCVSLUylGRltCntjS4M/preview" width="100%" height="800" >  </iframe>
                                </div>
                                <!-- end card body -->
                            </div>
                              <div class="card">
                                
                                <!--end card-body-->
                                <div class="card-body p-4">
                                    <h5 class="card-title mb-4">Adendum PP MAA</h5>
                                    
                                    <iframe src="https://drive.google.com/file/d/15GGFJsnwbba_TnKQ9Aw14qaqvN_SGHMk/preview" width="100%" height="800" >  </iframe>
                                    
                                </div>
                                <!-- end card body -->
                            </div>
                            <!--end card-->
                        </div>
                       
                        <!--end col-->
                        <div class="col-xxl-3">
                            
                            <!--end card-->
                            <div class="card">
                                <div class="card-header">
                                    <h6 class="card-title fw-semibold mb-0">Files Attachment</h6>
                                </div>
                                <div class="card-body">
                                    <div class="d-flex align-items-center border border-dashed p-2 rounded">
                                        <div class="flex-shrink-0 avatar-sm">
                                            <div class="avatar-title bg-light rounded">
                                                <i class="ri-file-pdf-fill fs-20 text-danger"></i>
                                            </div>
                                        </div>
                                        <div class="flex-grow-1 ms-3">
                                            <h6 class="mb-1"><a href="javascript:void(0);">PP PT. MAA 2023-2025.pdf</a></h6>
                                            <small class="text-muted">1 MB</small>
                                        </div>
                                        <div class="hstack gap-3 fs-16">
                                            <a href="file/dokumen/PP 2023 - 2025.pdf" class="text-muted"><i class="ri-download-2-line"></i></a>
                                            
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
							<div class="card">
                                <div class="card-header d-flex align-items-center">
                                    <h6 class="card-title mb- flex-grow-1">History View PP </h6>
                                    <a class="text-muted" href="">
                                        See All <?php $sql2 = mysqli_query($koneksi,"SELECT idhistori FROM histori where pp = 1");
												$row2 = mysqli_num_rows($sql2);
												?><?=$row2;?> User
										<i class="ri-arrow-right-line align-bottom"></i>
                                    </a>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive table-card">
                                        <div data-simplebar style="max-height: 365px;">
                                            <ul class="list-group list-group-flush">
                                                      <?php $sql = mysqli_query($koneksi, "SELECT
												user.nama, 
												user.divisi,
												user.file_foto
											FROM
												histori
												INNER JOIN `user` ON histori.idnik = `user`.idnik 
											WHERE
												histori.pp = '1'
												ORDER BY histori.idhistori DESC "); 
														
													while ($row = mysqli_fetch_assoc($sql)) { 

                    									?>
                                                <li class="list-group-item list-group-item-action">
                                                    <div class="d-flex align-items-center">
                                                        <img src="file/profile/<?=$row['file_foto']?>" alt="" class="avatar-xs object-cover rounded-circle">
                                                        <div class="ms-3 flex-grow-1">
                                                            
                                                                <h6 class="fs-14 mb-1"><?=$row['nama']?></h6>
                                                         <p class="mb-0 text-muted"><?=$row['divisi']?></p>
                                                        </div>
                                                    </div>
                                                </li>
											<?php	}?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						
                        <!--end col-->
                    </div>
                    <!--end row-->

     
    