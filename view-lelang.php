<?php
if (!empty($_SESSION["notif"])) {
    echo $_SESSION["notif"];
    unset($_SESSION["notif"]);
}

function tglformat($tanggalRaw)
{
    $tanggal = new DateTime($tanggalRaw);
    return $tanggal->format('d M y H:i:s');
}
$sql = mysqli_query($koneksi, "SELECT * FROM lelang WHERE id_lelang='" . $_GET['id'] . "' ");
$row = mysqli_fetch_assoc($sql)
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.34/moment-timezone-with-data.min.js"></script>

<style>
    /* CSS untuk penyesuaian tampilan form bid */
    .info-rules p {
        font-size: 0.9rem;
        color: #555;
        background-color: #f8f9fa;
        border-left: 3px solid #007bff;
        padding: 10px;
        margin-top: 5px;
    }

    /* Custom CSS untuk meningkatkan tampilan */
    .bid-input {
        font-size: 1.25rem;
        /* Meningkatkan ukuran font */
        box-shadow: 0 0 5px rgba(0, 123, 255, 0.5);
        /* Menambahkan bayangan biru */
        text-align: center;
        /* Membuat teks di tengah */
    }

    .current-price {
        font-size: 1.1rem;
        /* Meningkatkan ukuran font untuk harga tertinggi */
        display: block;
        /* Membuat teks menjadi blok sehingga berada di baris baru */
        text-align: center;
        /* Membuat teks di tengah */
        color: #007bff;
        /* Memberikan warna biru */
    }

    /* Styling untuk tombol dan form secara keseluruhan */
    #modal-id {
        text-align: center;
        /* Menengahkan konten dalam div */
    }

    .btn-secondary.mt-2 {
        font-size: 1rem;
        /* Meningkatkan ukuran font untuk tombol */
        padding: .375rem .75rem;
        /* Menyesuaikan padding */
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row gx-lg-5">
                    <div class="col-xl-4 col-md-8 mx-auto">
                        <div class="product-img-slider sticky-side-div">
                            <div class="swiper product-thumbnail-slider p-2 rounded bg-light">
                                <div class="swiper-wrapper">
                                    <?php $sql1 = mysqli_query($koneksi, "SELECT * FROM foto_lelang where id_lelang = '" . $_GET['id'] . "' ");

                                    while ($row1 = mysqli_fetch_assoc($sql1)) { ?>
                                        <div class="swiper-slide">
                                            <img src="file/lelang/<?= $row1['foto'] ?>" alt="" class="img-fluid d-block" />
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                            <!-- end swiper thumbnail slide -->
                            <div class="swiper product-nav-slider mt-2">


                                <div class="discount-time" data-start-time="<?= $row['tgl_lelang'] ?>" data-end-time="<?= $row['end_tgl_lelang'] ?>">
                                    <h5 id="auction-time-<?= $row['id_lelang'] ?>" class="mb-0 text-center"></h5>
                                </div>

                                <?php
                                $tanggalSekarang = date("Y-m-d H:i:s");
                                if ($tanggalSekarang >= $row['tgl_lelang'] && $tanggalSekarang <= $row['end_tgl_lelang']) : ?>

                                    <a class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#bid">BID</a>
                                <?php endif; ?>

                                <div class="swiper-wrapper">

                                    <?php $sql1 = mysqli_query($koneksi, "SELECT * FROM foto_lelang where id_lelang = '" . $_GET['id'] . "' ");

                                    while ($row1 = mysqli_fetch_assoc($sql1)) { ?>
                                        <div class="swiper-slide">
                                            <div class="nav-slide-item">
                                                <img src="file/lelang/<?= $row1['foto'] ?>" alt="" class="img-fluid d-block" />
                                            </div>
                                        </div>

                                    <?php } ?>

                                </div>


                            </div>

                            <div class="hstack gap-2">
                                <button hidden class="btn btn-success w-100" data-bs-toggle="modal" data-bs-target="#bid">Place Bid</button>


                            </div>
                            <!-- end swiper nav slide -->
                        </div>

                    </div>
                    <!-- end col -->
                    <div class="col-xl-8">
                        <div class="mt-xl-0 mt-5">
                            <div class="d-flex">
                                <div class="flex-grow-1">
                                    <h4><?= $row['nama_barang'] ?></h4>
                                    <div class="hstack gap-3 flex-wrap">
                                        <div class="text-muted">Merk : <span class="text-body fw-medium"><?= $row['merk_barang'] ?></div>
                                        <div class="vr"></div>
                                        <div class="text-muted">Kategori : <span class="text-body fw-medium"><?= $row['kategori_barang'] ?></span></div>
                                        <div class="vr"></div>
                                        <div class="text-muted">Published : <span class="text-body fw-medium"><?= $row['tgl_lelang'] ?></span></div>
                                    </div>

                                </div> <?php if ($_SESSION['role'] == 'admin' && $menulogin == '5' || $menulogin == '9') { ?>
                                    <div class="flex-shrink-0">
                                        <div>
                                            <a href="index.php?page=UpdateAuction&id=<?= $row['id_lelang'] ?>" class="btn btn-light" data-bs-toggle="tooltip" data-bs-placement="top" title="Edit"><i class="ri-pencil-fill align-bottom"></i></a>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="row mt-4">
                                <div class="col-lg-3 col-sm-6">
                                    <div class="p-2 border border-dashed rounded">
                                        <div class="d-flex align-items-center">
                                            <div class="avatar-sm me-2">
                                                <div class="avatar-title rounded bg-transparent text-success fs-24">
                                                    <i class="ri-currency-line "></i>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1">
                                                <p class="text-muted mb-1">Bottom Line :</p>
                                                <h5 class="mb-0"><?= rupiah($row['harga_lelang']) ?></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end col -->
                                <div class="col-lg-3 col-sm-6">
                                    <div class="p-2 border border-dashed rounded">
                                        <div class="d-flex align-items-center">
                                            <div class="avatar-sm me-2">
                                                <div class="avatar-title rounded bg-transparent text-success fs-24">
                                                    <i class="ri-calendar-event-line"></i>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1">
                                                <p class="text-muted mb-1">Tahun Barang :</p>
                                                <h5 class="mb-0"><?= $row['tahun_barang'] ?></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="p-2 border border-dashed rounded">
                                        <div class="d-flex align-items-center">
                                            <div class="avatar-sm me-2">
                                                <div class="avatar-title rounded bg-transparent text-success fs-24">
                                                    <i class="ri-file-copy-2-fill"></i>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1">
                                                <p class="text-muted mb-1">Type :</p>
                                                <h5 class="mb-0"><?= $row['type_barang'] ?></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div class="p-2 border border-dashed rounded">
                                        <div class="d-flex align-items-center">
                                            <div class="avatar-sm me-2">
                                                <div class="avatar-title rounded bg-transparent text-success fs-24">
                                                    <i class="ri-file-copy-2-fill"></i>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1">
                                                <p class="text-muted mb-1">Grade :</p>
                                                <h5 class="mb-0"><?= $row['grade'] ?></h5>
                                            </div>

                                        </div>
                                        <button class="btn btn-danger w-100" data-bs-toggle="modal" data-bs-target=".bs-example-modal-xl">View Inspection Result</button>


                                        <!-- Large modal -->


                                    </div>

                                </div>
                                <!-- end col -->

                                <!-- end col -->

                                <!-- end col -->
                            </div>
                            <!-- end row -->
                            <div class="mt-4 text-muted">
                                <h5 class="fs-14">Product Description :</h5>
                                <p><?= $row['diskripsi'] ?></p>
                            </div>
                            <div class="product-content mt-5">
                                <h5 class="fs-14 mb-3">User Bid :</h5>
                                <nav>
                                    <ul class="nav nav-tabs nav-tabs-custom nav-success" id="nav-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="nav-speci-tab" data-bs-toggle="tab" href="#nav-speci" role="tab" aria-controls="nav-speci" aria-selected="true">User Bid</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="nav-detail-tab" data-bs-toggle="tab" href="#nav-detail" role="tab" aria-controls="nav-detail" aria-selected="false">Current Best Deal</a>
                                        </li>
                                    </ul>
                                </nav>
                                <div class="tab-content border border-top-0 p-4" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-speci" role="tabpanel" aria-labelledby="nav-speci-tab">
                                        <div class="table-responsive">
                                            <table class="table mb-0">
                                                <tbody>
                                                    <?php $sql1 = mysqli_query($koneksi, "SELECT
																			id_lelang,nama,harga_penawaran,waktu_penawaran,file_foto,divisi 
																		FROM
																			penawaran_lelang
																			INNER JOIN user ON penawaran_lelang.idnik = user.idnik 
																		WHERE
																			id_lelang = '" . $_GET['id'] . "' ORDER BY waktu_penawaran DESC LIMIT 10 ");
                                                    while ($row1 = mysqli_fetch_assoc($sql1)) { ?>
                                                        <tr>

                                                            <td>
                                                                <div class="d-flex align-items-center">
                                                                    <img src="file/profile/<?= $row1['file_foto'] ?>" alt="" class="avatar-xs rounded object-cover" />
                                                                    <div class="ms-2">
                                                                        <a href="#!">
                                                                            <h6 class="mb-1"><?= $row1['nama'] ?></h6>
                                                                        </a>
                                                                        <p class="text-muted mb-0"><?= $row1['divisi'] ?></p>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td><?= rupiah($row1['harga_penawaran']) ?></td>
                                                            <td><?= tglformat($row1['waktu_penawaran']) ?></td>

                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="nav-detail" role="tabpanel" aria-labelledby="nav-detail-tab">
                                        <div>
                                            <div class="table-responsive">
                                                <table class="table mb-0">
                                                    <tbody>
                                                        <?php
                                                        $sql1 = mysqli_query($koneksi, "SELECT
                        id_lelang, nama, harga_penawaran, waktu_penawaran, file_foto, divisi, status_penawaran
                    FROM
                        penawaran_lelang
                        INNER JOIN user ON penawaran_lelang.idnik = user.idnik
                    WHERE
                        id_lelang = '" . $_GET['id'] . "' AND (status_penawaran = 'Bidding' OR status_penawaran = 'Confirm' OR status_penawaran = 'Approve')
                    ORDER BY
                        CASE 
                            WHEN status_penawaran = 'Approve' THEN 0
                            WHEN status_penawaran = 'Confirm' THEN 1
                            ELSE 2 
                        END,
                        harga_penawaran DESC
                    LIMIT 1 ");

                                                        while ($row1 = mysqli_fetch_assoc($sql1)) {
                                                            // Menangani logika waktu dan status lelang sama seperti sebelumnya
                                                            $current_time = time();
                                                            $sql_end_time = "SELECT end_tgl_lelang FROM lelang WHERE id_lelang = '" . $_GET['id'] . "'";
                                                            $result_end_time = mysqli_query($koneksi, $sql_end_time);
                                                            $row_end_time = mysqli_fetch_assoc($result_end_time);
                                                            $end_time = strtotime($row_end_time['end_tgl_lelang']);
                                                            $lelang_berlangsung = $current_time < $end_time;
                                                        ?>
                                                            <tr>
                                                                <td>
                                                                    <div class="d-flex align-items-center">
                                                                        <img src="file/profile/<?= $row1['file_foto'] ?>" alt="" class="avatar-xs rounded object-cover" />
                                                                        <div class="ms-2">
                                                                            <a href="#!">
                                                                                <h6 class="mb-1"><?= $row1['nama'] ?></h6>
                                                                            </a>
                                                                            <p class="text-muted mb-0"><?= $row1['divisi'] ?></p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td><?= rupiah($row1['harga_penawaran']) ?></td>
                                                                <td><?= $row1['waktu_penawaran'] ?></td>
                                                                <td>
                                                                    <?php
                                                                    // Mengatur tampilan berdasarkan status penawaran
                                                                    if ($row1['status_penawaran'] == 'Bidding' && $_SESSION['role'] == 'admin' && in_array($menulogin, ['5', '9'])) {
                                                                        // Menampilkan tombol untuk status 'Bidding'
                                                                        $button_class = $lelang_berlangsung ? 'btn-secondary' : 'btn-primary';
                                                                        $button_text = $lelang_berlangsung ? '<i class="ri-lock-line"></i> Bidding in progress' : 'Confirm Winner';
                                                                        $button_disabled = $lelang_berlangsung ? 'disabled' : '';
                                                                        echo "<button class='btn $button_class btn-confirm' data-id='{$row1['id_lelang']}' data-bs-toggle='modal' data-bs-target='#confirmModal' $button_disabled>$button_text</button>";
                                                                    } elseif ($row1['status_penawaran'] == 'Confirm') {
                                                                        // Hanya menampilkan teks untuk status 'Confirm'
                                                                        echo "Confirmed";
                                                                    } elseif ($row1['status_penawaran'] == 'Approve') {
                                                                        // Hanya menampilkan teks untuk status 'Approve'
                                                                        echo "Approved";
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end card body -->
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end card body -->
        </div>
        <!-- end card -->
    </div>
    <!-- end col -->
</div>
<!-- end row -->

<?php
$query = "SELECT MAX(harga_penawaran) AS harga_tertinggi FROM penawaran_lelang WHERE id_lelang = '" . $_GET['id'] . "'";
$result = mysqli_query($koneksi, $query);
if ($result) {
    $row_penawaran = mysqli_fetch_assoc($result);
    // Cek jika sudah ada penawaran
    if ($row_penawaran['harga_tertinggi'] != NULL) {
        $harga_tampil = $row_penawaran['harga_tertinggi']; // Harga penawaran tertinggi
    } else {
        // Jika belum ada penawaran, gunakan harga lelang awal
        // Pastikan $row['harga_lelang'] sudah berisi data dari query sebelumnya
        $harga_tampil = $row['harga_lelang'];
    }
    // Format harga untuk tampilan
    $harga_format = number_format($harga_tampil, 0, '.', ',');
} else {
    echo "Error: " . mysqli_error($conn);
}
?>

<!-- modal bid price -->
<div class="modal fade zoomIn" id="bid" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-s">
        <div class="modal-content border-0">
            <div class="modal-header p-3 bg-soft-info">
                <h5 class="modal-title" id="exampleModalLabel">Bid Price</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
            </div>
            <form action="function/insert_penawaran_lelang.php" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row g-3 justify-content-center">
                        <div class="col-lg-6"> <!-- Mengubah ukuran kolom untuk penengahan -->
                            <div id="modal-id" class="shadow-sm p-3 mb-5 bg-body rounded"> <!-- Menambahkan shadow -->
                                <label class="form-label" for="harga_penawaran">Start Bidding</label>
                                <input type="text" name="idnik" value="<?= $niklogin ?>" hidden>
                                <input type="text" name="id_lelang" value="<?= $_GET['id'] ?>" hidden>
                                <input type="text" class="form-control bid-input" value="<?= $harga_format ?>" readonly name="harga_penawaran" id="harga_penawaran">
                                <!-- Button to increment bid -->
                                <button type="button" class="btn btn-secondary mt-2" id="increment-btn">+Rp.50,000</button>
                            </div>
                            <i class="current-price">Harga Tertinggi saat ini <?= $harga_format ?></i>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12 mt-3">
                            <div class="info-rules">
                                <p>Please enter a bid that is greater than the starting bid and meets the bid increment criteria. Your bid must be in accordance with the rules and regulations of the auction. A bid once made cannot be retracted. Make sure you review your bid amount before submitting.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="hstack gap-2 justify-content-end">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Bid Now</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- modal Inspection -->
<div class="modal fade bs-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myExtraLargeModalLabel">File Inspection Result </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="d-flex">
                    <div class="flex-shrink-0">
                    </div>
                    <div class="flex-grow-1 ms-2">
                        <iframe src="<?= $row['link_inspeksi'] ?>" width="100%" height="800"> </iframe>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- Modal konfirmasi -->
<div class="modal fade" id="confirmModal" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmModalLabel">Konfirmasi Tindakan</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="confirmationForm" method="post" action="function/update_bid_status.php">
                <?php
                // Query untuk mendapatkan best deal terakhir
                $sql1 = mysqli_query($koneksi, "SELECT id_penawaran_lelang, id_lelang, status_penawaran
                                                        FROM
                                                            penawaran_lelang
                                                          WHERE
                                                            id_lelang = '" . $_GET['id'] . "' ORDER BY harga_penawaran DESC LIMIT 1 ");

                $row1 = mysqli_fetch_assoc($sql1);
                ?>
                <input type="text" name="id_penawaran_lelang" value="<?= $row1['id_penawaran_lelang'] ?>">
                <input type="text" name="id_lelang" value="<?= $row1['id_lelang'] ?>">
                <input type="hidden" name="status_penawaran" value="Confirm">
                <div class="modal-body">
                    <p>Apakah Anda yakin ingin mengonfirmasi tindakan ini? Jika ya, silakan isi nomor WhatsApp pemenang.</p>
                    <div class="mb-3">
                        <label for="whatsapp" class="form-label">Nomor WhatsApp Pemenang:</label>
                        <input type="text" class="form-control" id="whatsapp" name="whatsapp" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!--Swiper slider js-->
<script src="assets/libs/swiper/swiper-bundle.min.js"></script>

<!-- ecommerce product details init -->
<script src="assets/js/pages/ecommerce-product-details.init.js"></script>
<script src="assets/libs/cleave.js/cleave.min.js"></script>
<!-- form masks init -->
<script src="assets/js/pages/form-masks.init.js"></script>
<script src="assets/js/pages/apps-nft-auction.init.js"></script>

<script>
    document.addEventListener("DOMContentLoaded", function() {
        var countdownElements = document.querySelectorAll(".discount-time");

        countdownElements.forEach(function(element) {
            // Gunakan moment-timezone untuk parsing waktu dengan zona waktu Asia/Jakarta
            var startTime = moment.tz(element.getAttribute("data-start-time"), "Asia/Jakarta").valueOf();
            var endTime = moment.tz(element.getAttribute("data-end-time"), "Asia/Jakarta").valueOf();
            var now = moment.tz("Asia/Jakarta").valueOf();
            var targetTime = now < startTime ? startTime : endTime;
            var elementId = element.querySelector("h5").id;

            setInterval(function() {
                countDownTimer(targetTime, elementId, now < startTime);
            }, 1000);
        });

        function countDownTimer(targetTime, elementId, isPreAuction) {
            var timeLeft = targetTime - moment.tz("Asia/Jakarta").valueOf(),
                days = Math.floor(timeLeft / 864e5),
                hours = Math.floor(timeLeft % 864e5 / 3600000),
                minutes = Math.floor(timeLeft % 3600000 / 60000),
                seconds = Math.floor(timeLeft % 60000 / 1000);
            days = days < 10 ? "0" + days : days;
            hours = hours < 10 ? "0" + hours : hours;
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            var countdownText = days + " : " + hours + " : " + minutes + " : " + seconds;
            if (isPreAuction) {
                countdownText = "Starts in " + countdownText;
            }
            document.getElementById(elementId).textContent = countdownText;

            if (timeLeft < 0) {
                document.getElementById(elementId).textContent = isPreAuction ? "Auction has started" : "Auction has ended";
            }
        }
    });
</script>


<script>
    document.addEventListener("DOMContentLoaded", function() {
        const incrementBtn = document.getElementById("increment-btn");
        const bidInput = document.getElementById("harga_penawaran");

        incrementBtn.addEventListener("click", function() {
            let currentBid = parseInt(bidInput.value.replace(/\D/g, '')) || 0; // Remove non-numeric characters and parse
            currentBid += 50000; // Add 10,000
            bidInput.value = currentBid.toLocaleString(); // Update the input value, formatted
        });
    });
</script>