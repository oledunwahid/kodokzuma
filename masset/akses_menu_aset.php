
<?php
$page = (isset($_GET['page'])) ? $_GET['page'] : '';

if (isset($_SESSION['username'])) {
	if ($_SESSION['role'] == 'admin') {

		switch ($page) {

			case 'Dashboard':
				include "home-aset.php";
				break;
			case 'Delivery':
				include "delivery.php";
				break;
			case 'ViewDelivery':
				include "view-delivery.php";
				break;	
			case 'AddDelivery':
				include "add-delivery.php";
				break;	
			case 'Aset':
				include "aset.php";
				break;
			case 'ViewAset':
				include "view-aset.php";
				break;
			case 'UpdateAset':
				include "edit-aset.php";
				break;	
			case 'AddAset':
				include "add-aset.php";
				break;	
			case 'AddAset2':
				include "add-aset2.php";
				break;		
			case 'RFM':
				include "rfm.php";
				break;
			case 'ViewRFM':
				include "view-rfm.php";
				break;	
			case 'TypeAset':
				include "type-aset.php";
				break;
			case 'KategoriAset':
				include "kategori-aset.php";
				break;	
			case 'LokasiAset':
				include "lokasi-aset.php";
				break;
			case 'PT':
				include "pt-aset.php";
				break;
			case 'bast':
				include "bast.php";
				break;
				case 'QR':
				include "qr.php";
				break;
				
			
			default:
				include "../pages-404.php";
				break;
		}
	}
	if ($_SESSION['role'] == 'user') { // Jika user yang login adalah user
		// Berikut halaman yang bisa di akses :
		switch ($page) {
			case 'Dashboard':
				include "home.php";
				break;
			
			default:
				include "pages-404.php";
				break;
		}
	}
} else
	include "login.php";

?>
