<!--datatable css-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
<!--datatable responsive css-->
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">



<?php
$id_rfm    = $_GET['id'];

$sql = mysqli_query($koneksi, "SELECT * FROM rfm WHERE id_rfm='" . $_GET['id'] . "' ");
$row = mysqli_fetch_assoc($sql)
?>

<div class="row">

    <div class="col-lg-4">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title mb-3">Info RFM : <?= $row['id_rfm'] ?> </h5>




                <div>
                    <form action="" method="POST">
						
                        <label for="client_nameName-field" class="form-label">List Barang RFM</label>
						
                      	<select  class="form-control"  data-plugin="customselect" id="choices-single-default" data-choices  name="list_rfm">
						 <?php $sql2 = mysqli_query($koneksi, "SELECT * FROM list_rfm where id_rfm = '" . $_GET['id'] . "' && jumlah > 0  ");
                            while ($row2 = mysqli_fetch_assoc($sql2)) { ?>
                                <option value="<?=$row2['id_list_rfm'] ?>" > <?=$row2['nama_desc'];} ?> </option> 	
							
						</select>
					
						
                </div>




                <div class="mt-4 hstack gap-2">
                    <button class="btn btn-soft-primary w-100" name="cari" type="submit">Cari</button>

                </div>
                </form>

            </div><!-- end card body -->
        </div><!-- end card -->
    </div>
                                    <?php 
										if(isset($_POST["cari"]))
										{ $list_rfm = $_POST["list_rfm"];
												
										$sql5 = mysqli_query($koneksi, "SELECT
												rfm.id_rfm,
												list_rfm.nama_desc, 
												list_rfm.id_list_rfm, 
												list_rfm.kategori_aset, 
												list_rfm.jumlah
											FROM
												list_rfm
												INNER JOIN rfm ON rfm.id_rfm = list_rfm.id_rfm
											WHERE list_rfm.id_list_rfm = $list_rfm
												
										");
										$row5 = mysqli_fetch_assoc($sql5);
										                               
										?>
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                <form action="function/insert_aset.php" method="POST">
                    <div class="row g-3">
						 <div class="col-lg-6">
                            <div id="modal-id">
                                <label for="orderId" class="form-label">No. Aset</label>
                                <input type="text" class="form-control" placeholder="Masukan No. Aset" name="id_aset" required />
                            </div>
                        </div>
						<div class="col-lg-6">
                            <div id="modal-id">
                                <label for="orderId" class="form-label">Nama Barang</label>
								 <input type="text"  name="id_list_rfm" hidden value="<?=$row5['id_list_rfm']?>"  >
								 <input type="text"  name="jumlah" hidden value="<?=$row5['jumlah']?>"  >
                                <input type="text" class="form-control"  name="nama_desc" value="<?=$row5['nama_desc']?>"  required readonly/>
								
                            </div>
                        </div>
                       
                        <div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">No. RFM</label>
                                <input type="text" class="form-control" name="id_rfm" value="<?=$row5['id_rfm']?>" required readonly />

                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">No. PO</label>
                                <input type="text" class="form-control" name="no_po" required />
                             </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Tanggal Aset</label>
                                <input type="date" class="form-control" value="<?=$tgl?>" name="tgl_aset" required />
                             </div>
                        </div>
                        <div class="col-lg-6">
                            <div>
                                <label for="client_nameName-field" class="form-label">Kategori Aset</label>
                                  <select class="form-control" data-choices name="kategori_aset" id="choices-single-default"  required>
                                                            <option value="">Masukan Kategori</option>
														    <?php $sql = mysqli_query($koneksi, "SELECT * FROM kategori_aset "); 
															while ($row = mysqli_fetch_assoc($sql)) { ?>
														<option value="<?=$row['id_kategori_aset']?>"><?=$row['nama_kategori_aset']?></option> <?php }?> 
                                                        </select>
                            </div>
                        </div>
						 
						<div class="col-lg-6">
                            <div>
                                <label for="client_nameName-field" class="form-label">Type Aset</label>
                                 <select class="form-control" data-choices name="id_type_aset" id="choices-single-default"  required>
                                                            <option value="">Masukan Type</option>
														    <?php $sql = mysqli_query($koneksi, "SELECT * FROM type_aset "); 
															while ($row = mysqli_fetch_assoc($sql)) { ?>
														<option value="<?=$row['id_type_aset']?>"><?=$row['nama_type_aset']?></option> <?php }?> 
                                                        </select>
                            </div>
                        </div>
						 <div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Serial Number</label>
                                <input type="text" class="form-control" name="serial_number" required />

                            </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Part Number</label>
                                <input type="text" class="form-control" name="part_number" required />

                            </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">No Jurnal</label>
                                <input type="text" class="form-control" name="no_jurnal" required />

                            </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Merk</label>
                                <input type="text" class="form-control" name="merk" required />

                            </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Insurance</label>
                                <input type="text" class="form-control" name="insurance" required />

                            </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Model</label>
                                <input type="text" class="form-control" name="model" required />

                            </div>
                        </div>
						
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Price</label>
                                <input type="text" class="form-control" placeholder="Enter Your Best Price"  name="price_aset" id="cleave-numeral">

                            </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Status</label>
                                <select class="form-control" data-choices name="status_aset" id="choices-single-default"  required>
                                                            <option value="Aktif">Aktif</option>
											 				<option value="Aktif">Non-Aktif</option>
														    
                                                        </select>

                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Detail Spek</label>
                               <textarea class="form-control"  name="detail_spek"rows="5"></textarea>

                            </div>
                        </div>
                       

       

           






                    </div>
                    <div class="modal-footer">
                        <div class="mt-4 hstack gap-2">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add Aset</button>

                        </div>
                    </div>
                </form>




            </div>
        </div>

    </div>
	<?php }?>

</div>


<div class="modal fade zoomIn" id="addrfm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-l">

        <div class="modal-content border-0">
            <div class="modal-header p-3 bg-soft-info">
                <h5 class="modal-title" id="exampleModalLabel">Create pt Aset</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
            </div>
            <form action="function/insert_list_rfm.php" method="POST">
                <div class="modal-body">
                    <div class="row g-3">
                        <div class="col-lg-12">
                            <div id="modal-id">
                                <label for="orderId" class="form-label">Nama Barang Descripsi</label>
                                <input type="text" class="form-control" placeholder="Maskan Nama Desc" name="nama_desc" required />
                                <input type="text" hidden value="<?= $_GET['id'] ?>" name="id_rfm" />
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Kategori Barang</label>
                                <input type="text" class="form-control" placeholder=" Kategori Barang " name="id_kategori_aset" />
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Jumlah</label>
                                <input type="text" class="form-control" placeholder="Masukan Jumlaht" name="jumlah" />
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <div class="hstack gap-2 justify-content-end">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add pt Aset</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade zoomIn" id="prosesrfm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-l">

        <div class="modal-content border-0 ">
            <div class="modal-header p-3 bg-soft-info">

                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
            </div>
            <form action="function/update_rfm.php" method="POST">
                <div class="modal-body">
                    <div class="row g-3">
                        <div class="col-lg-12">
                            <div id="modal-id">
                                <div class="mt-2 text-center">

                                    <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                                        <h4>PROSES RFM DENGAN NO <?= $_GET['id']; ?> ?</h4>

                                    </div>
                                </div>
                                <input type="text" hidden value="<?= $_GET['id'] ?>" name="id_rfm" />
                                <input type="text" hidden value="Proses" name="status_rfm" />
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" name="proses">Proses</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>




    








<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<!--datatable js-->
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="../assets/libs/cleave.js/cleave.min.js"></script>
<script src="../assets/js/pages/form-masks.init.js"></script>
<script src="../assets/js/pages/datatables.init.js"></script>