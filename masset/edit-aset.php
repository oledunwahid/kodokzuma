<?php $sql = mysqli_query($koneksi, "SELECT * FROM aset  WHERE id_aset='" . $_GET['id'] . "' ");
$row = mysqli_fetch_assoc($sql)
?>

<?php if (!empty($_SESSION["notif"])) {
    echo $_SESSION["notif"];
    unset($_SESSION["notif"]);
}    ?>


<!--datatable css-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
<!--datatable responsive css-->
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">





<div class="row">

    
                                
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                <form action="function/update_aset.php" method="POST">
                    <div class="row g-3">
						 <div class="col-lg-6">
                            <div id="modal-id">
                                <label for="orderId" class="form-label">No. Aset</label>
                                <input type="text" class="form-control" placeholder="Masukan No. Aset" name="id_aset" value="<?=$row['id_aset']?>" readonly required />
                            </div>
                        </div>
						<div class="col-lg-6">
                            <div id="modal-id">
                                <label for="orderId" class="form-label">Nama Barang</label>
                                <input type="text" class="form-control" value="<?=$row['nama_desc']?>" name="nama_desc"  required />
								
                            </div>
                        </div>
                       
                        <div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">No. RFM</label>
                                <input type="text" class="form-control"  value="<?=$row['id_rfm']?>" name="id_rfm"  required />

                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">No. PO</label>
                                <input type="text" class="form-control" value="<?=$row['no_po']?>" name="no_po" required />
                             </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Tanggal Aset</label>
                                <input type="date" class="form-control" value="<?=$row['tgl_aset']?>"  name="tgl_aset" required />
                             </div>
                        </div>
                        <div class="col-lg-6">
                            <div>
                                <label for="client_nameName-field" class="form-label">Kategori Aset</label>
                                  <select class="form-control" data-choices name="kategori_aset" id="choices-single-default"  required>
                                         <option value="<?=$row['kategori_aset']?>">
									  <?php $sql3 = mysqli_query($koneksi, "SELECT * FROM kategori_aset  WHERE id_kategori_aset='" . $row['kategori_aset'] . "' ");
										$row3 = mysqli_fetch_assoc($sql3);
									 echo $row3['nama_kategori_aset'];
											 ?>
									  
									  </option>
														    <?php $sql2 = mysqli_query($koneksi, "SELECT * FROM kategori_aset "); 
															while ($row2 = mysqli_fetch_assoc($sql2)) { ?>
									<option value="<?=$row2['id_kategori_aset']?>"><?=$row2['nama_kategori_aset']?></option> <?php }?> 
                                                        </select>
                            </div>
                        </div>
						
						<div class="col-lg-6">
                            <div>
                                <label for="client_nameName-field" class="form-label">Type Aset</label>
                                 <select class="form-control" data-choices  name="id_type_aset" id="choices-single-default"  required>
                                           <option value="<?=$row['id_type_aset']?>">
									  <?php $sql4 = mysqli_query($koneksi, "SELECT * FROM type_aset  WHERE id_type_aset='" . $row['id_type_aset'] . "' ");
										$row3 = mysqli_fetch_assoc($sql4);
									 echo $row3['nama_type_aset'];
											 ?>
									  
									  </option>
														    <?php $sql1 = mysqli_query($koneksi, "SELECT * FROM type_aset "); 
															while ($row1 = mysqli_fetch_assoc($sql1)) { ?>
														<option value="<?=$row1['id_type_aset']?>"><?=$row1['nama_type_aset']?></option> <?php }?>  
                                                        </select>
                            </div>
                        </div>
						 <div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Serial Number</label>
                                <input type="text" class="form-control" value="<?=$row['serial_number']?>" name="serial_number" required />

                            </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Part Number</label>
                                <input type="text" class="form-control" value="<?=$row['part_number']?>" name="part_number" required />

                            </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Merk</label>
                                <input type="text" class="form-control" value="<?=$row['merk']?>" name="merk" required />

                            </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Insurance</label>
                                <input type="text" class="form-control" value="<?=$row['insurance']?>" name="insurance" required />

                            </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Model</label>
                                <input type="text" class="form-control" value="<?=$row['model']?>" name="model" required />

                            </div>
                        </div>
						
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Price</label>
                                <input type="text" class="form-control" value="<?=$row['price_aset']?>"  placeholder="Enter Your Best Price"  name="price_aset" id="cleave-numeral">

                            </div>
                        </div>
						<div class="col-lg-6">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Status</label>
                                        <select class="form-control" data-choices name="status_aset" id="choices-single-default"  required>				
															   <option value="<?=$row['status_aset']?>"><?=$row['status_aset']?></option>
											 				<option value="Aktif">Aktif</option>
														<option value="Non-Aktif">Non-Aktif</option>
														    
                                                        </select>

                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Detail Spek</label>
                               <textarea class="form-control" name="detail_spek" rows="5"><?=$row['detail_spek']?></textarea>

                            </div>
                        </div>

       

           






                    </div>
                    <div class="modal-footer">
                        <div class="mt-4 hstack gap-2">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Update Aset</button>

                        </div>
                    </div>
                </form>




            </div>
        </div>

    </div>

</div>


<div class="modal fade zoomIn" id="addrfm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-l">

        <div class="modal-content border-0">
            <div class="modal-header p-3 bg-soft-info">
                <h5 class="modal-title" id="exampleModalLabel">Create pt Aset</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
            </div>
            <form action="function/insert_list_rfm.php" method="POST">
                <div class="modal-body">
                    <div class="row g-3">
                        <div class="col-lg-12">
                            <div id="modal-id">
                                <label for="orderId" class="form-label">Nama Barang Descripsi</label>
                                <input type="text" class="form-control" placeholder="Maskan Nama Desc" name="nama_desc" required />
                                <input type="text" hidden value="<?= $_GET['id'] ?>" name="id_rfm" />
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Kategori Barang</label>
                                <input type="text" class="form-control" placeholder=" Kategori Barang " name="id_kategori_aset" />
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div>
                                <label for="tasksTitle-field" class="form-label">Jumlah</label>
                                <input type="text" class="form-control" placeholder="Masukan Jumlaht" name="jumlah" />
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <div class="hstack gap-2 justify-content-end">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add pt Aset</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade zoomIn" id="prosesrfm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-l">

        <div class="modal-content border-0 ">
            <div class="modal-header p-3 bg-soft-info">

                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
            </div>
            <form action="function/update_rfm.php" method="POST">
                <div class="modal-body">
                    <div class="row g-3">
                        <div class="col-lg-12">
                            <div id="modal-id">
                                <div class="mt-2 text-center">

                                    <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                                        <h4>PROSES RFM DENGAN NO <?= $_GET['id']; ?> ?</h4>

                                    </div>
                                </div>
                                <input type="text" hidden value="<?= $_GET['id'] ?>" name="id_rfm" />
                                <input type="text" hidden value="Proses" name="status_rfm" />
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" name="proses">Proses</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<?php $sql = mysqli_query($koneksi, "SELECT * FROM pt_aset ");

while ($row = mysqli_fetch_assoc($sql)) { ?>
    <div class="modal fade zoomIn" id="delete<?= $row['id_pt_aset'] ?>" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="btn-close"></button>
                </div>
                <div class="modal-body">
                    <div class="mt-2 text-center">
                        <lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>
                        <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                            <h4>Are you Sure ?</h4>
                            <p class="text-muted mx-4 mb-0">Are you Sure You want to Remove this Record <br> <b><?= $row['pt_aset']; ?></b> ?</p>
                        </div>
                    </div>
                    <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                        <button type="button" class="btn w-sm btn-light" data-bs-dismiss="modal">Close</button>
                        <a style="cursor: pointer;" onclick="location.href='function/delete_pt_aset.php?aksi=delete&id=<?= $row['id_pt_aset']; ?>'" class="btn w-sm btn-danger">Yes, Delete It!</a>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade zoomIn" id="update<?= $row['id_pt_aset'] ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-l">

            <div class="modal-content border-0">
                <div class="modal-header p-3 bg-soft-info">
                    <h5 class="modal-title" id="exampleModalLabel">Update Type Aset</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                </div>
                <form action="function/update_pt_aset.php" method="POST">
                    <div class="modal-body">
                        <div class="row g-3">
                            <div class="col-lg-12">
                                <div id="modal-id">
                                    <label for="orderId" class="form-label">Nama pt</label>
                                    <input type="text" name="id_pt_aset" hidden value="<?= $row['id_pt_aset'] ?>" />
                                    <input type="text" class="form-control" name="pt_aset" value="<?= $row['pt_aset'] ?>" required />
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div>
                                    <label for="tasksTitle-field" class="form-label">Keterangan pt</label>
                                    <input type="text" class="form-control" name="ket_pt" value="<?= $row['ket_pt'] ?>" />
                                </div>
                            </div>




                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="hstack gap-2 justify-content-end">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Update pt</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>






<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<!--datatable js-->
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="../assets/libs/cleave.js/cleave.min.js"></script>
<script src="../assets/js/pages/form-masks.init.js"></script>
<script src="../assets/js/pages/datatables.init.js"></script>