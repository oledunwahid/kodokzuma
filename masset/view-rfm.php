 <!--datatable css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <!--datatable responsive css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<?php if(!empty($_SESSION["notif"])){
echo $_SESSION["notif"];
unset($_SESSION["notif"]);
				}	?>

<?php
$idrfm     = $_GET['id'];

$sql = mysqli_query($koneksi, "SELECT * FROM rfm WHERE id_rfm='" . $_GET['id'] . "' "); // query jika filter dipilih

$row = mysqli_fetch_assoc($sql) // fetch query yang sesuai ke dalam array

?>

<div class="row">
    
	<div class="col-lg-4">
                            <div class="card">
                                                    <div class="card-body">
                                                        <h5 class="card-title mb-3">Info RFM : <?=$row['id_rfm']?> </h5>
                                                        <div class="table-responsive">
                                                            <table class="table table-borderless mb-0">
                                                                <tbody>
																	<tr>
                                                                        <th class="ps-0" scope="row">Nama Input :</th>
                                                                        <td class="text-muted"><?=$row['idnik']?></td>
																		
                                                                    </tr>
																	<tr>
                                                                        <th class="ps-0" scope="row">Tanggal :</th>
                                                                        <td class="text-muted"><?=$row['tgl_rfm']?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th class="ps-0" scope="row">Lokasi :</th>
                                                                        <td class="text-muted"><?=$row['id_lokasi_aset']?></td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <th class="ps-0" scope="row">PT :</th>
                                                                        <td class="text-muted"><?=$row['id_pt_aset']?></td>
                                                                    </tr>
																	
                                                                    <tr>
                                                                        <th class="ps-0" scope="row">Requestor :</th>
                                                                        <td class="text-muted"><?=$row['request_rfm']?></td>
                                                                    </tr>
																	
                                                                    
                                                                </tbody>
                                                            </table>
															<?php if ($row['status_rfm'] == 'Pending') { ?>
															<div class="mt-4 hstack gap-2">                    
																<button  class="btn btn-soft-primary w-100" data-bs-toggle="modal" data-bs-target="#prosesrfm">Proses</button>           
																<a href="index.php?page=RFM" class="btn btn-soft-success w-100">Back</a>                
															</div>
															<?php }else{ ?> <div class="mt-4 hstack gap-2">                    
																<button  class="btn btn-soft-primary w-100">Print</button>           
																               
															</div>
															<?php }?>
                                                        </div>
                                                    </div><!-- end card body -->
                                                </div><!-- end card -->
                        </div>
	
	<div class="col-lg-8">
                            <div class="card">
                                <div class="card-header">
									<div class="d-flex align-items-center">
                                    <h5 class="card-title mb-0 flex-grow-1">Form RFM  <?=$row['id_rfm']?>  </h5>
										<?php if ($_SESSION['role'] == 'admin' && $row['status_rfm'] == 'Pending') { ?>
										<div class="flex-shrink-0">
									<button class="btn btn-danger add-btn" data-bs-toggle="modal" data-bs-target="#addrfm"><i class="ri-add-line align-bottom me-1"></i> Create Material</button>
											</div>
										<?php }?>
                                </div>
								</div>
								
                                <div class="card-body">
                                     <table id="buttons-datatables" class="display table table-bordered dt-responsive" style="width:100%">
                                        <thead>
                                            <tr>
                                               
                                                <th>No.</th>
                                                <th>Material Desc </th>
												<th>Kategori Barang</th>
												<th>Jumlah</th>
											
                                                
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                              <?php $sql1 = mysqli_query($koneksi, "SELECT * FROM list_rfm where id_rfm = '$idrfm' "); 
											$nomor=1;
                    while ($row1 = mysqli_fetch_assoc($sql1)) { 

                    ?>
                                            <tr>
                                                
                                                <td><?=$nomor++ ?></td>
                                                <td><?= $row1['nama_desc'] ?></td>
                                                <td><?= $row1['kategori_aset'] ?></td>
												 <td><?= $row1['jumlah_fix'] ?></td>
                                             
                             
                                                <td>
													   <?php if($row['status_rfm'] == 'Pending') { ?>
                                                    <a href="function/delete_list_rfm.php?aksi=delete&id=<?=$row1['id_list_rfm']?>&idd=<?=$idrfm?>" class="btn btn-danger btn-sm" >Delete</a>
													<?php }  ?>
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
	
</div>


		<div class="modal fade zoomIn" id="addrfm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-l">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Create pt Aset</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/insert_list_rfm.php" method="POST">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-12">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">Nama Barang Descripsi</label>
                                                    <input type="text" class="form-control" placeholder="Maskan Nama Desc" name="nama_desc" required/>
													 <input type="text" hidden value="<?=$_GET['id']?>" name="id_rfm" />
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Kategori Barang</label>
                                                   <select class="form-control" data-choices name="kategori_aset" id="choices-single-default" required >
                                                            <option value="">Masukan Kategori</option>
														    <option value="Aset">Aset</option> 
													        <option value="Low Value Aset">Low Value Aset</option> 
                                                        </select>
                                                </div>
                                            </div>
											<div class="col-lg-12">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Jumlah</label>
                                                    <input type="text" class="form-control" placeholder="Masukan Jumlah" name="jumlah"  />
                                                </div>
                                            </div>
                                           
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add pt Aset</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

<div class="modal fade zoomIn" id="prosesrfm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-l">
						
                            <div class="modal-content border-0 ">
                                <div class="modal-header p-3 bg-soft-info">
                                   
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/update_rfm.php" method="POST">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-12">
                                                <div id="modal-id">
                                                    <div class="mt-2 text-center">
                                        
                                        <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                                            <h4>PROSES RFM DENGAN NO <?=$_GET['id'];?> ?</h4>
                                           
                                        </div>
                                    </div>
													<input type="text"  hidden value="<?=$_GET['id']?>" name="id_rfm" />
                                                    <input type="text" hidden  value="Proses" name="status_rfm" />
                                                </div>
                                            </div>
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="proses" >Proses</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

		


<script>
		$(document).on('click', '.hapus_data', function(){
    var id = $(this).attr('id');
    $.ajax({
        type: 'POST',
        url: "function/delete_list_rfm.php",
        data: {id:id},
        success: function() {
            $('.data').load("data.php");
        }, error: function(response){
            console.log(response.responseText);
        }
    });
});
	</script>		

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<!--datatable js-->
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="../assets/js/pages/datatables.init.js"></script>