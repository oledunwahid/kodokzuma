 <!--datatable css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <!--datatable responsive css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">



<?php if(!empty($_SESSION["notif"])){
										   echo $_SESSION["notif"];
										   unset($_SESSION["notif"]);
									   		}	?>
<div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
									<div class="d-flex align-items-center">
                                    <h5 class="card-title mb-0 flex-grow-1">Request Form Material </h5>
										<?php if ($_SESSION['role'] == 'admin') { ?>
										<div class="flex-shrink-0">
									<button class="btn btn-danger add-btn" data-bs-toggle="modal" data-bs-target="#showModal"><i class="ri-add-line align-bottom me-1"></i> Create RFM</button>
											</div>
										<?php }?>
                                </div>
								</div>
								
                                <div class="card-body">
                                     <table id="buttons-datatables" class="display table table-bordered dt-responsive" style="width:100%">
                                        <thead>
                                            <tr>
                                               
                                                <th>No.</th>
                                                <th>No RFM</th>
												<th>Tanggal</th>
												<th>Lokasi</th>
												<th>Nama PT</th>
												<th>Requestor</th>
												<th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
											
											
                              <?php 
											
										
								$sql = mysqli_query($koneksi, "SELECT
								user.nama,
								rfm.status_rfm,
								rfm.id_rfm,
								rfm.tgl_rfm,
								rfm.request_rfm,
								lokasi_aset.lokasi_aset,
								pt_aset.pt_aset 
								FROM
								rfm
								INNER JOIN user ON rfm.request_rfm = user.idnik
								INNER JOIN lokasi_aset ON rfm.id_lokasi_aset = lokasi_aset.id_lokasi_aset
								INNER JOIN pt_aset ON rfm.id_pt_aset = pt_aset.id_pt_aset 
								"); 									
									$nomor=1;
                    				while ($row = mysqli_fetch_assoc($sql)) { 

                    ?>
                                            <tr>
                                                
                                                <td><?=$nomor++ ?></td>
                                                <td><a href="index.php?page=ViewRFM&id=<?= $row['id_rfm']; ?>" > <?= $row['id_rfm'] ?></a></td>
                                                <td><?= $row['tgl_rfm'] ?></td>
												<td><?= $row['lokasi_aset'] ?></td>
												<td><?= $row['pt_aset'] ?></td>
												<td><?= $row['nama']?></td>
												<td>
												<?=$row['status_rfm'] ?>
																						
												</td>
												 
                                
                                                <td>
                                                    <div class="dropdown d-inline-block">
                                                        <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <i class="ri-more-fill align-middle"></i>
                                                        </button>
                                                        <ul class="dropdown-menu dropdown-menu-end">
                                                            <li><a href="index.php?page=ViewRFM&id=<?= $row['id_rfm']; ?>" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
															<?php if ($_SESSION['role'] == 'admin') { ?>
                                                            <li>
															<a class="dropdown-item edit-item-btn" data-bs-toggle="modal" data-bs-target="#update<?=$row['id_rfm']?>"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                                                            <li>
                                                             <a class="dropdown-item remove-item-btn" data-bs-toggle="modal" data-bs-target="#delete<?=$row['id_rfm']?>"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Delete</a>
                                                            </li>
															<?php }?>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
									
									
                                </div>
                            </div>
                        </div><!--end col-->
                    </div>


<div class="modal fade zoomIn" id="showModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Create RFM</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/insert_rfm.php" method="POST">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-6">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">No. RFM</label>
                                                    <input type="text" class="form-control" placeholder="Masukan No. RFM" name="id_rfm" required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Tanggal</label>
                                                    <input type="date" class="form-control" value="<?php echo "$tgl" ?>" name="tgl_rfm" required />
													<input type="text" class="form-control" value="<?php echo "$niklogin" ?>" name="idnik" hidden />
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Lokasi</label>
                                                     <select class="form-control" data-choices name="id_lokasi_aset" id="choices-single-default" >
                                                            <option value="">Masukan Nama Lokasi</option>
														    <?php $sql = mysqli_query($koneksi, "SELECT * FROM lokasi_aset "); 
															while ($row = mysqli_fetch_assoc($sql)) { ?>
														<option value="<?=$row['id_lokasi_aset']?>"><?=$row['lokasi_aset']?></option> <?php }?> 
                                                        </select>
                                                </div>
                                            </div>
											 <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Nama PT</label>
                                                      <select class="form-control" data-choices name="id_pt_aset" id="choices-single-default" >
                                                            <option value="">Masukan Nama Lokasi</option>
														    <?php $sql = mysqli_query($koneksi, "SELECT * FROM pt_aset "); 
															while ($row = mysqli_fetch_assoc($sql)) { ?>
														<option value="<?=$row['id_pt_aset']?>"><?=$row['pt_aset']?></option> <?php }?> 
                                                        </select>
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Requestor</label>
                                                     <select class="form-control" data-choices name="request_rfm" id="choices-single-default" >
                                                            <option value="">Masukan Nama Lokasi</option>
														    <?php $sql = mysqli_query($koneksi, "SELECT * FROM user "); 
															while ($row = mysqli_fetch_assoc($sql)) { ?>
														<option value="<?=$row['idnik']?>"><?=$row['nama']?></option> <?php }?> 
                                                        </select>
                                                </div>
                                            </div>
											
											
                                            
										<div class="col-lg-12">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Keterangan</label>
                                                    <input type="text" class="form-control"  name="ket_rfm" required />
													
                                                </div>
                                            </div>	
                                           
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add RFM</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

<?php $sql = mysqli_query($koneksi, "SELECT * FROM rfm ");

    while ($row = mysqli_fetch_assoc($sql)) { ?>
<div class="modal fade zoomIn" id="delete<?=$row['id_rfm']?>" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="btn-close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="mt-2 text-center">
                                        <lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>
                                        <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                                            <h4>Are you Sure ?</h4>
                                            <p class="text-muted mx-4 mb-0">Are you Sure You want to Remove this Record <br> <b><?=$row['id_rfm']; ?></b> ?</p>
                                        </div>
                                    </div>
                                    <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                                        <button type="button" class="btn w-sm btn-light" data-bs-dismiss="modal">Close</button>
										<a style="cursor: pointer;" onclick="location.href='function/delete_rfm.php?aksi=delete&id=<?=$row['id_rfm']; ?>'" class="btn w-sm btn-danger">Yes, Delete It!</a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<div class="modal fade zoomIn" id="update<?=$row['id_rfm']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Update facility</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/update_rfm.php" method="POST">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-6">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">No. RFM</label>
                                                    <input type="text" class="form-control" value="<?=$row['id_rfm']?>" name="id_rfm" readonly/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Tanggal</label>
                                                    <input type="date" class="form-control" value="<?=$row['tgl_rfm']?>" name="tgl_rfm"  />
													</div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Lokasi</label>
                                                     <select class="form-control" data-choices name="id_lokasi_aset" id="choices-single-default" >
                                                            <option value="<?=$row['id_lokasi_aset']?>">
														   <?php 
													         $idlokasi= $row['id_lokasi_aset'];
															$sql2 = mysqli_query($koneksi, "SELECT * FROM lokasi_aset WHERE id_lokasi_aset= '".$idlokasi."' ");
											 				$row2 = mysqli_fetch_assoc($sql2)?>
														 	<?=$row2['lokasi_aset']?>
														   </option>
														    <?php $sql1 = mysqli_query($koneksi, "SELECT * FROM lokasi_aset "); 
															while ($row1 = mysqli_fetch_assoc($sql1)) { ?>
														<option value="<?=$row1['id_lokasi_aset']?>"><?=$row1['lokasi_aset']?></option> <?php }?> 
                                                        </select>
                                                </div>
                                            </div>
											 <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Nama PT</label>
                                                      <select class="form-control" data-choices name="id_pt_aset" id="choices-single-default" >
                                                            <option value="<?=$row['id_pt_aset']?>">
														   <?php 
													        $idpt= $row['id_pt_aset'];
															$sql2 = mysqli_query($koneksi, "SELECT * FROM pt_aset WHERE id_pt_aset= '".$idpt."' ");
											 				$row2 = mysqli_fetch_assoc($sql2)?>
														 	<?=$row2['pt_aset']?>
														  </option>
														    <?php $sql1 = mysqli_query($koneksi, "SELECT * FROM pt_aset "); 
															while ($row1 = mysqli_fetch_assoc($sql1)) { ?>
														<option value="<?=$row1['id_pt_aset']?>"><?=$row1['pt_aset']?></option> <?php }?> 
                                                        </select>
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Requestor</label>
                                                     <select class="form-control" data-choices name="request_rfm" id="choices-single-default" >
                                                            <option value="<?=$row['request_rfm']?>">
														  <?php 
													        $iduser= $row['request_rfm'];
															$sql2 = mysqli_query($koneksi, "SELECT * FROM user WHERE idnik= '".$iduser."' ");
											 				$row2 = mysqli_fetch_assoc($sql2)?>
														 	<?=$row2['nama']?>
														 </option>
														    <?php $sql1 = mysqli_query($koneksi, "SELECT * FROM user "); 
															while ($row1 = mysqli_fetch_assoc($sql1)) { ?>
														<option value="<?=$row1['idnik']?>"><?=$row1['nama'];}?></option>  
                                                        </select>
                                                </div>
                                            </div>
											
											
                                            
										<div class="col-lg-12">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Keterangan</label>
                                                    <input type="text" class="form-control" value="<?=$row['ket_rfm']?>"  name="ket_rfm" required />
													
                                                </div>
                                            </div>	
                                           
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add RFM</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
<?php }?>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


<!--datatable js-->
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="../assets/js/pages/datatables.init.js"></script>


   

