 <!--datatable css-->
 <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
 <!--datatable responsive css-->
 <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

 <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">




 <!-- Notifikasi Jika Sukses / Gagal Input, Edit dan Delete -->

 <?php if (!empty($_SESSION["notif"])) {
		echo $_SESSION["notif"];
		unset($_SESSION["notif"]);
	} ?>

 <div class="row">
 	<div class="col-lg-12">
 		<div class="card">
 			<div class="card-header">
 				<div class="d-flex align-items-center">
 					<h5 class="card-title mb-0 flex-grow-1">Request Form Material </h5>
 					<?php if ($_SESSION['role'] == 'admin') { ?>
 						<div class="flex-shrink-0">
 							<button class="btn btn-danger add-btn" data-bs-toggle="modal" data-bs-target="#showModal"><i class="ri-add-line align-bottom me-1"></i> Create Aset</button>

 						</div>
 					<?php } ?>
 				</div>
 			</div>

 			<div class="card-body">
 				<table id="buttons-datatables" class="display table table-bordered dt-responsive" style="width:100%">
 					<thead>
 						<tr>

 							<th>No.</th>
 							<th>No. Aset</th>
 							<th>Nama Aset</th>
 							<th>Tgl Aset</th>
 							<th>Status Aset</th>


 							<th>Action</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php $sql = mysqli_query($koneksi, "SELECT * FROM aset");
							$nomor = 1;
							while ($row = mysqli_fetch_assoc($sql)) {

							?>
 							<tr>

 								<td><?= $nomor++ ?></td>
 								<td><a href="index.php?page=ViewAset&id=<?= $row['id_aset']; ?>"> <?= $row['id_aset'] ?></a></td>
 								<td><?= $row['nama_desc'] ?></td>
 								<td><?= $row['tgl_aset'] ?></td>
 								<td><?= $row['status_aset'] ?></td>




 								<td>
 									<div class="dropdown d-inline-block">
 										<button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
 											<i class="ri-more-fill align-middle"></i>
 										</button>
 										<ul class="dropdown-menu dropdown-menu-end">
 											<li><a href="index.php?page=ViewAset&id=<?= $row['id_aset']; ?>" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
 											<?php if ($_SESSION['role'] == 'admin') { ?>
 												<li>
 													<a href="index.php?page=UpdateAset&id=<?= $row['id_aset']; ?>" class="dropdown-item"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a>
 												</li>
 												<li>
 													<a class="dropdown-item remove-item-btn" data-bs-toggle="modal" data-bs-target="#delete<?= $row['id_aset'] ?>"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Delete</a>
 												</li>
 											<?php } ?>
 										</ul>
 									</div>
 								</td>
 							</tr>
 						<?php } ?>
 					</tbody>
 				</table>


 			</div>
 		</div>
 	</div><!--end col-->
 </div>

 <div class="modal fade zoomIn" id="showModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
 	<div class="modal-dialog modal-dialog-centered modal-lg">

 		<div class="modal-content border-0">
			
 							

 						
 			<div class="modal-header p-3 bg-soft-info">
 				<h5 class="modal-title" id="exampleModalLabel">Create Aset</h5>
				

 				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
 			</div>

 			<table class="display table table-bordered dt-responsive" style="width:100%">
 				<thead>
 					<tr>

 						<th>No.</th>
 						<th>No RFM </th>
 						<th>Nama PT</th>
 						
 					</tr>
 				</thead>
 				<tbody>
 					<?php $sql5 = mysqli_query($koneksi, "SELECT
							rfm.id_rfm,
							list_rfm.jumlah,
							list_rfm.nama_desc,
							list_rfm.id_list_rfm,
							rfm.status_rfm,
							pt_aset.pt_aset,
							SUM( jumlah ) AS B 
						FROM
							rfm
							INNER JOIN pt_aset ON rfm.id_pt_aset = pt_aset.id_pt_aset
							INNER JOIN list_rfm ON rfm.id_rfm = list_rfm.id_rfm 
						WHERE
							rfm.status_rfm = 'Proses' 
							AND list_rfm.jumlah > 0 
						GROUP BY
							id_rfm  ");
						$nomor = 1;
						while ($row5 = mysqli_fetch_assoc($sql5)) {

						?>

 						<tr>

 							<td><?= $nomor++ ?></td>
 							<td><a href="index.php?page=AddAset&id=<?= $row5['id_rfm']; ?>"> <?= $row5['id_rfm'] ?></a></td>
 							<td><?= $row5['pt_aset'] ?></td>


 						</tr>


 					<?php }

						?>

 				</tbody>
 			</table>
			<a class="btn btn-danger add-btn" href="index.php?page=AddAset2" ><i class="ri-add-line align-bottom me-1"></i>Create Aset No RFM</a>

 		</div>
 	</div>
 </div>


 <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


 <!--datatable js-->
 <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
 <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

 <script src="../assets/js/pages/datatables.init.js"></script>