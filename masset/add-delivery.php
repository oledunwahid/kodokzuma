
    
    <?php 	$query = mysqli_query($koneksi, "SELECT max(id_delivery) as kodeTerbesar FROM delivery");
            $data = mysqli_fetch_array($query);
            $kodeid = $data['kodeTerbesar'];
            $urutan = (int) substr($kodeid,3,5);
    
            // bilangan yang diambil ini ditambah 1 untuk menentukan nomor urut berikutnya
            $urutan++;
    
    
            $huruf = "DLV";
            $kodeid = $huruf . sprintf("%05s", $urutan);
    
            if (!empty($_SESSION["notif"])) {
                echo $_SESSION["notif"];
                unset($_SESSION["notif"]);
            }
    
            ?>
    
    
    <div class="row">
    <div class="col-lg-4">
            <div class="card">
                <div class="card-body">
                    
                   <div>
                        <form action="function/insert_list_delivery.php" method="POST">
                            <input type="text" value="<?= $kodeid?>" name="id_delivery" hidden>
                            <label for="client_nameName-field" class="form-label">List Aset</label>
                            
                              <select  class="form-control"  data-plugin="customselect" id="choices-single-default" data-choices  name="id_aset">
                                 <option value="">Cari ID Aset</option> 
                             <?php $sql2 = mysqli_query($koneksi, "SELECT * FROM aset   ");
                                while ($row2 = mysqli_fetch_assoc($sql2)) { ?>
                                    <option value="<?=$row2['id_aset'] ?>" > <?=$row2['id_aset']?> || <?=$row2['nama_desc'];} ?> </option> 	
                            </select>
                        
                    </div>
    
                    <div class="mt-4 hstack gap-2">
                        <button class="btn btn-soft-primary" name="masukan" type="submit">Add</button>
    
                    </div>
                    </form>
    
                </div><!-- end card body -->
            </div><!-- end card -->
        </div>	    
        <div class="col-lg-8">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="d-flex align-items-center">
                                        <h5 class="card-title mb-0 flex-grow-1">List Aset  </h5>
                                              
                                    </div>
                                    </div>
                                    <div class="card-body">
                                        <table id="buttons-datatables" class="display table table-bordered dt-responsive" style="width:100%">
                                            <thead>
                                                <tr>
                                                   
                                                    <th>No.</th>
                                                    <th>id_aset</th>
                                                    <th>Action</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                
                                  <?php 
                                                
                                            
                                    $sql = mysqli_query($koneksi, "SELECT * FROM list_delivery WHERE id_delivery = '$kodeid'
                                    "); 									
                                        $nomor=1;
                                        while ($row = mysqli_fetch_assoc($sql)) { 
    
                        ?>
                                                <tr>
                                                    
                                                    <td><?=$nomor++ ?></td>                                          
                                                   
                                                    <td><?= $row['id_aset'] ?></td>
                                                
                                                    <td>
                                                        <a href="function/delete_list_delivery.php?aksi=delete&id=<?=$row['id_list_delivery']?>" class="btn btn-danger btn-sm" >Delete</a>
                                                    </td>
                                                </tr>
                                                <?php }?>
                                            </tbody>
                                        </table>
                                         
                                    </div>
                                </div>
                            </div>
        
        
        <div class="col-lg-8">
                                <div class="card">
									 <form action="function/insert_delivery.php" method="POST">
                                                        <div class="card-body">
                                                            
                                                            
                                                             <div class="row">
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label class="form-label" for="manufacturer-name-input">No Delivery</label>
                                                                <input type="text" class="form-control" value="<?= $kodeid?>" name="id_delivery" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="mb-3">
                                                                <label class="form-label" for="manufacturer-brand-input">Date Delivery</label>
                                                                <input type="date" class="form-control" value="<?=$tgl?>" name="tgl_delivery">
                                                            </div>
                                                        </div>
                                                    </div>
                                                            <div class="row">
                                                        <div class="col-lg-3 col-sm-6">
                                                            <div class="mb-3">
                                                                <label class="form-label" for="stocks-input">Delivery By</label>
                                                                <input type="text" class="form-control" name="kurir_delivery">
                                                           
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-sm-6">
                                                            <div class="mb-3">
                                                                <label class="form-label" for="product-price-input">PIC Delivery</label>
                                                                <div class="input-group has-validation mb-3">
                                                                
                                                                    <input type="text" class="form-control" name="pic_delivery" required>
                                                                    <div class="invalid-feedback">Please Enter a product price.</div>
                                                                </div>
    
                                                            </div>
                                                        </div>
                                                        
                                                                 <div class="col-lg-3 col-sm-6">
                                                            <div class="mb-3">
                                                                <label class="form-label" for="product-discount-input">Proses</label>
                                                                <div class="input-group mb-3">
                                                                    <button type="submit" class="btn btn-success" name="masukan" >Add Delivery</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <!-- end col -->
                                                    </div>
                                                                  
                                                        </div><!-- end card body -->
									</form>
                                                    </div><!-- end card -->
                            </div>
    
        
    </div>
    
    
    <div class="modal fade zoomIn" id="addrfm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-l">
                            
                                <div class="modal-content border-0">
                                    <div class="modal-header p-3 bg-soft-info">
                                        <h5 class="modal-title" id="exampleModalLabel">Create pt Aset</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                    </div>
                                     <form action="function/insert_list_rfm.php" method="POST">
                                        <div class="modal-body">
                                            <div class="row g-3">
                                                <div class="col-lg-12">
                                                    <div id="modal-id">
                                                        <label for="orderId" class="form-label">Nama Barang Descripsi</label>
                                                        <input type="text" class="form-control" placeholder="Maskan Nama Desc" name="nama_desc" required/>
                                                         <input type="text" hidden value="<?=$_GET['id']?>" name="id_rfm" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div>
                                                        <label for="tasksTitle-field" class="form-label">Kategori Barang</label>
                                                       <select class="form-control" data-choices name="kategori_aset" id="choices-single-default" required >
                                                                <option value="">Masukan Kategori</option>
                                                                <option value="Aset">Aset</option> 
                                                                <option value="Low Value Aset">Low Value Aset</option> 
                                                            </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div>
                                                        <label for="tasksTitle-field" class="form-label">Jumlah</label>
                                                        <input type="text" class="form-control" placeholder="Masukan Jumlah" name="jumlah"  />
                                                    </div>
                                                </div>
                                               
                                            </div>
    
                                        </div>
                                        <div class="modal-footer">
                                            <div class="hstack gap-2 justify-content-end">
                                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add pt Aset</button>
                                               
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
    
    <div class="modal fade zoomIn" id="prosesrfm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-l">
                            
                                <div class="modal-content border-0 ">
                                    <div class="modal-header p-3 bg-soft-info">
                                       
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                    </div>
                                     <form action="function/update_rfm.php" method="POST">
                                        <div class="modal-body">
                                            <div class="row g-3">
                                                <div class="col-lg-12">
                                                    <div id="modal-id">
                                                        <div class="mt-2 text-center">
                                            
                                            <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                                                <h4>PROSES RFM DENGAN NO <?=$_GET['id'];?> ?</h4>
                                               
                                            </div>
                                        </div>
                                                        <input type="text"  hidden value="<?=$_GET['id']?>" name="id_rfm" />
                                                        <input type="text" hidden  value="Proses" name="status_rfm" />
                                                    </div>
                                                </div>
                                                
                                            </div>
    
                                        </div>
                                        <div class="modal-footer">
                                            <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-success" name="proses" >Proses</button>
                                               
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
    
            
    
   
            
                
    
    
    
    
        <script src="../assets/js/pages/datatables.init.js"></script>