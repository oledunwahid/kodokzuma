<?php $sql = mysqli_query($koneksi, "SELECT * FROM aset  WHERE id_aset='" . $_GET['id'] . "' ");
$row = mysqli_fetch_assoc($sql)



?>

<?php if (!empty($_SESSION["notif"])) {
    echo $_SESSION["notif"];
    unset($_SESSION["notif"]);
}    ?>


<div class="position-relative mx-n4 mt-n4">
    <div class="profile-wid-bg profile-setting-img">
        <img src="assets/images/profile-bg.jpg" class="profile-wid-img" alt="">
        <div class="overlay-content">
            <div class="text-end p-3">
                <div class="text-end p-3">
                    <div class="p-0 ms-auto rounded-circle profile-photo-edit">
                        <input id="profile-foreground-img-file-input" type="file" class="profile-foreground-img-file-input">
                        <label for="profile-foreground-img-file-input" class="profile-photo-edit btn btn-light">
                            <i class="ri-image-edit-line align-bottom me-1"></i> Change Cover
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include "phpqrcode/qrlib.php"; 
$penyimpanan = "file/qr/";
if (!file_exists($penyimpanan))
 mkdir($penyimpanan);
$idaset = 	$row['id_aset'];										   
$isi = "http://localhost/eip2/masset/index.php?page=ViewAset&id=$idaset"; 

					 
					 
QRcode::png($isi, $penyimpanan.$row['id_aset']); 
?>
<div class="row">
    <div class="col-xxl-3">
        <div class="card mt-n5">
            <div class="card-body p-4">
                <div class="text-center mb-3">
                    <div class="profile-user position-relative d-inline-block mx-auto  mb-4">
                        <img src="file/qr/<?=$row['id_aset']?>" class="avatar-xl user-profile-image" alt="user-profile-image">
							<h5><?=$row['id_aset']?></h5>
							<h5><?=$row['nama_desc']?></h5>
                                    
                                    <a href="index.php?page=QR&id=<?= $row['id_aset']?>" class="btn btn-danger">Print Barcode</a>

                    </div>

                  

                </div>



            </div>
        </div>


    </div>
    <!--end col-->
    <div class="col-xxl-9">
        <div class="card mt-xxl-n5">
            <div class="card-header">
                <ul class="nav nav-tabs-custom rounded card-header-tabs border-bottom-0" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-bs-toggle="tab" href="#personalDetails" role="tab">
                            <i class="fas fa-home"></i> Aset Details
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#changePassword" role="tab">
                            <i class="far fa-user"></i> Delivery Aset
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#dokumen" role="tab">
                            <i class="far fa-envelope"></i> Histrory Aset
                        </a>
                    </li>
					   <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#bast" role="tab">
                            <i class="far fa-envelope"></i> BAST
                        </a>
                    </li>

                </ul>
            </div>
            <div class="card-body p-4">
                <div class="tab-content">
                    <div class="tab-pane active" id="personalDetails" role="tabpanel">
                        <form action="function/update_user.php" method="POST">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="firstnameInput" class="form-label"> ID ASET</label>
                                        <div class="text-muted fs-5"><?= $row['id_aset'] ?> </div>


                                    </div>
                                </div>

                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="company" class="form-label">Nama Barang</label>
                                        <div class="text-muted fs-5"><?= $row['nama_desc'] ?> </div>

                                    </div>
                                </div>
                                <!--end col-->

                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="phonenumberInput" class="form-label">No RFM</label>
                                        <div class="text-muted fs-5"><?= $row['id_rfm'] ?> </div>

                                    </div>
                                </div>
                                <!--end col-->

                                <!--end col-->

                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="divisi" class="form-label">No PO </label>
                                        <div class="text-muted fs-5"><?= $row['no_po'] ?> </div>
                                    </div>
                                </div>
                                <!--end col-->

                                <!--end col-->

                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="department" class="form-label">Kategori Aset</label>
										<?php $sql2 = mysqli_query($koneksi, "SELECT * FROM kategori_aset  WHERE id_kategori_aset ='" . $row['kategori_aset']. "' "); $row2 = mysqli_fetch_assoc($sql2) ?>
										
                                        <div class="text-muted fs-5"><?= $row2['nama_kategori_aset'] ?> </div>
                                    </div>
                                </div>
                                <!--end col-->

                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="section" class="form-label">Tanggal Aset</label>
                                        <div class="text-muted fs-5"><?= $row['tgl_aset'] ?> </div>
                                    </div>
                                </div>

                                <!--end col-->



                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="section" class="form-label">Type Aset</label>
										<?php $sql3 = mysqli_query($koneksi, "SELECT * FROM type_aset  WHERE id_type_aset ='" . $row['id_type_aset']. "' "); $row3 = mysqli_fetch_assoc($sql3) ?>
                                        <div class="text-muted fs-5"><?= $row3['nama_type_aset'] ?> </div>
                                    </div>
                                </div>

                                <!--end col-->

                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="text" class="form-label">Serial Number</label>
                                        <div class="text-muted fs-5"><?= $row['serial_number'] ?> </div>
                                    </div>
                                </div>
                                <!--end col-->

                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="atasan" class="form-label">Part Number</label>
                                        <div class="text-muted fs-5"><?= $row['part_number'] ?> </div>
                                    </div>
                                </div>
                                <!--end col-->

                                <!--end col-->

                                <!--end col-->

                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="roster" class="form-label">No Jurnal </label>
                                        <div class="text-muted fs-5"><?= $row['no_jurnal'] ?> </div>
                                    </div>
                                </div>

                                <!--end col-->

                                <!--end col-->
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="date" class="form-label">Merk</label>
                                        <div class="text-muted fs-5"><?= $row['merk'] ?> </div>
                                    </div>
                                </div>


                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="company" class="form-label">Status</label>
                                        <div class="text-muted fs-5"><?= $row['status_aset'] ?> </div>
                                    </div>

                                </div>
                                <div class="col-lg-6">
                                    <div class="mb-3">
                                        <label for="domisili" class="form-label">Detail Spek</label>
                                        <div class="text-muted fs-5"><?= $row['detail_spek'] ?> </div>



                                    </div>
                                </div>

                            </div>
                            <!--end row-->
                        </form>
                    </div>
                    <!--end tab-pane-->
                    <div class="tab-pane" id="changePassword" role="tabpanel">
                        <div class="col-lg-8">


                            <div class="card-body">
                                <table id="buttons-datatables" class="display table table-bordered dt-responsive" style="width:100%">
                                    <thead>
                                        <tr>

                                            <th>No.</th>


                                            <th>Tgl</th>
                                            <th>Kurir Delivery</th>
                                            <th>PIC Delivery</th>
                                            <th>Status Delivery</th>



                                        </tr>
                                    </thead>
                                    <tbody>


                                        <?php


                                        $sql = mysqli_query($koneksi, "SELECT
	delivery.tgl_delivery, 
	delivery.kurir_delivery, 
	delivery.pic_delivery, 
	delivery.status_delivery, 
	list_delivery.id_aset
FROM
	delivery
	INNER JOIN
	list_delivery
	ON 
		delivery.id_delivery = list_delivery.id_delivery
WHERE
	list_delivery.id_aset = '" . $_GET['id'] . "'");
                                        $nomor = 1;
                                        while ($row = mysqli_fetch_assoc($sql)) {

                                        ?>
                                            <tr>

                                                <td><?= $nomor++ ?></td>

                                                <td><?= $row['tgl_delivery'] ?></td>
                                                <td><?= $row['kurir_delivery'] ?></td>
                                                <td><?= $row['pic_delivery'] ?></td>
                                                <td><?= $row['status_delivery'] ?></td>





                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                            </div>

                        </div>

                    </div>

                    <div class="tab-pane" id="dokumen" role="tabpanel">
                        <div class="d-flex align-items-center mb-4">
                            <h5 class="card-title flex-grow-1 mb-0">Documents</h5>
                            <div class="flex-shrink-0">
                                <button class="btn btn-danger add-btn" data-bs-toggle="modal" data-bs-target="#AddHistory"><i class="ri-add-line align-bottom me-1"></i> In/Out Stock</button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table class="table table-borderless align-middle mb-0">
                                        <thead class="table-light">
                                            <tr>
                                                <th scope="col">Tanggal CekIn/Out</th>
                                                <th scope="col">Status</th>
												<th scope="col">Karyawan</th>
												<th scope="col">Gudang</th>
												<th scope="col">Detail Lokasi</th>
                                                <th scope="col">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>



                                            <?php $sql3 = mysqli_query($koneksi, "SELECT * FROM history_aset WHERE id_aset ='" . $_GET['id'] . "' ORDER BY id_history DESC ");
                                            $nomor = 1;
                                            while ($row3 = mysqli_fetch_assoc($sql3)) {

                                            ?>


                                                <tr>
                                                    <td>
                                                        <div class="d-flex align-items-center">

                                                            <div class="ms-3 flex-grow-1">
                                                                <h6 class="fs-15 mb-0">
                                                                    <?= $row3['tgl_history'] ?>
                                                                </h6>
                                                            </div>
                                                        </div>
													</td>

                                                    <td> <?=$row3['status_history'] ?></td>
													
													<td><?php if($row3['idnik']>0){
												
												$sql6 = mysqli_query($koneksi, "SELECT  nama FROM user  WHERE idnik='" . $row3['idnik'] . "' ");
												$row6 = mysqli_fetch_assoc($sql6);
												echo $row6['nama'];
												
												}else{
												
												echo '-';
											} ?></td>
													<td><?php if($row3['id_lokasi_aset']>0){
												
												$sql8 = mysqli_query($koneksi, "SELECT lokasi_aset FROM lokasi_aset  WHERE id_lokasi_aset='" . $row3['id_lokasi_aset'] . "' ");
												$row8 = mysqli_fetch_assoc($sql8);
												echo $row8['lokasi_aset'];
											}else{
												
												echo 'Out';
											} ?> </td>
													<td><?=$row3['detail_lokasi']?></td>




                                                    <td>
                                                           
                                                                    <button class="dropdown-item" data-bs-toggle="modal" data-bs-target="#delete<?= $row['iddok'] ?>"><i class="ri-delete-bin-5-line me-2 align-middle"></i>Delete</button>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>


                    </div>
					<div class="tab-pane" id="bast" role="tabpanel">
                        <div class="d-flex align-items-center mb-4">
                            <h5 class="card-title flex-grow-1 mb-0">BAST</h5>
                            <div class="flex-shrink-0">
                                <button class="btn btn-danger add-btn" data-bs-toggle="modal" data-bs-target="#AddBast"><i class="ri-add-line align-bottom me-1"></i> BAST </button>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                              <div class="row justify-content-center mb-4">
								  <table id="buttons-datatables" class="display table table-bordered dt-responsive" style="width:100%">
 					<thead>
 						<tr>

 							<th>No.</th>
 							<th>No BAST</th>
 							<th>ID aset</th>
 						


 							<th>Action</th>
 						</tr>
 					</thead>
 					<tbody>
 						<?php $sql6 = mysqli_query($koneksi, "SELECT * FROM bast_aset  WHERE id_aset='" . $_GET['id'] . "'");
							$nomor = 1;
							while ($row6 = mysqli_fetch_assoc($sql6)) {

							?>
 							<tr>

 								<td><?= $nomor++ ?></td>
 								<td><?= $row6['id_bast'] ?></td>
 								<td><?= $row6['id_aset'] ?></td>

 								<td>
 									<div class="dropdown d-inline-block">
 										<button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
 											<i class="ri-more-fill align-middle"></i>
 										</button>
 										<ul class="dropdown-menu dropdown-menu-end">
 											
											<li>
											<a class="dropdown-item edit-item-btn" href="index.php?page=bast&id=<?= $row6['id_bast'] ?>"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> View</a></li>
											
											
 											
 										</ul>
 									</div>
 								</td>
 							</tr>
 						<?php } ?>
 					</tbody>
 				</table>
                        
                    </div>   

                            </div>
                        </div>


                    </div>



                </div>
            </div>
        </div>
</div>
</div>


        <div class="modal fade zoomIn" id="AddHistory" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered ">

                <div class="modal-content border-0">
                    <div class="modal-header p-3 bg-soft-info">
                        <h5 class="modal-title" id="exampleModalLabel">In/Out Stock</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                    </div>
                    <form action="function/insert_history.php" method="POST">
                        <div class="modal-body">
                            <div class="row g-3">
                                <?php

                                $sql3 = mysqli_query($koneksi, "SELECT *
																		FROM
																			history_aset
																		WHERE
																			history_aset.id_aset = '" . $_GET['id'] . "'
																		ORDER BY
																			history_aset.id_history DESC LIMIT 1");
                                $row3 = mysqli_fetch_assoc($sql3)
                                ?>



                                <div class="col-lg-12">
                                    <label for="date-field" class="form-label">Status Stock</label>
                                    <input type="text" class="form-control" name="id_aset" value="<?= $_GET['id'] ?>" hidden>
                                    <input type="text" class="form-control" name="status_history" readonly value="<?php if (mysqli_num_rows($sql3) < 1) {
                                                                                                                        echo 'In Stock';
                                                                                                                    } elseif ($row3['status_history'] == 'In Stock') {
                                                                                                                        echo "Out Stock";
                                                                                                                    } elseif ($row3['status_history'] == 'Out Stock') {
                                                                                                                        echo "In Stock";
                                                                                                                    }

                                                                                                                    ?>">
                                    <input type="date" class="form-control" name="tgl_history" hidden value="<?= $tgl ?>">
                                    <?php if (mysqli_num_rows($sql3) < 1) { ?>
									<label for="date-field" class="form-label">Nama Lokasi</label>
									 <select class="form-control" data-choices name="id_lokasi_aset" id="choices-single-default" >
                                                            <option value="">Masukan Nama Lokasi</option>
														    <?php $sql4 = mysqli_query($koneksi, "SELECT * FROM lokasi_aset "); 
															while ($row4 = mysqli_fetch_assoc($sql4)) { ?>
														<option value="<?=$row4['id_lokasi_aset']?>"><?=$row4['lokasi_aset']?></option> <?php }?> 
                                                        </select>
									
										
                                        <input type="text" class="form-control" name="idnik" hidden readonly value="0">
                                    <?php
                                    } elseif ($row3['status_history'] == 'Out Stock') { ?>
									
									<label for="date-field" class="form-label">Lokasi Aset</label>
									 <select class="form-control" data-choices name="id_lokasi_aset" id="choices-single-default" >
                                                            <option value="">Masukan Nama Lokasi</option>
														    <?php $sql4 = mysqli_query($koneksi, "SELECT * FROM lokasi_aset "); 
															while ($row4 = mysqli_fetch_assoc($sql4)) { ?>
														<option value="<?=$row4['id_lokasi_aset']?>"><?=$row4['lokasi_aset']?></option> <?php }?> 
                                                        </select>
									
									
									
                                    <input type="text" class="form-control" name="idnik" hidden readonly value="0"> <?php 
																					  }elseif ($row3['status_history'] == 'In Stock') {?>
									<input type="text" class="form-control" name="id_lokasi_aset" hidden  value="0">
									<label for="date-field" class="form-label">Nama User</label>
									<select class="form-control" data-plugin="customselect" id="choices-single-default" data-choices name="idnik">
                                            <?php $sql2 = mysqli_query($koneksi, "SELECT idnik,nama FROM user  ");
                                                  while ($row2 = mysqli_fetch_assoc($sql2)) { ?>
                                                <option value="<?= $row2['idnik'] ?>"> <?= $row2['nama']; } ?> </option>

                                    </select> <?php }?>

								<label for="date-field" class="form-label">Detail Lokasi</label>
									<input type="text" class="form-control" name="detail_lokasi">

                                </div>


                            </div>

                        </div>
                        <div class="modal-footer">
                            <div class="hstack gap-2 justify-content-end">
                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

<?php 		$query = mysqli_query($koneksi, "SELECT max(id_bast) as kodeTerbesar FROM bast_aset");
            $data = mysqli_fetch_array($query);
            $kodeid = $data['kodeTerbesar'];
            $urutan = (int) substr($kodeid,3,5);
    
            // bilangan yang diambil ini ditambah 1 untuk menentukan nomor urut berikutnya
            $urutan++;
    
    
            $huruf = "BAS";
            $kodeid = $huruf . sprintf("%05s", $urutan);
    
    
            ?>

<div class="modal fade zoomIn" id="AddBast" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered ">

                <div class="modal-content border-0">
                    <div class="modal-header p-3 bg-soft-info">
                        <h5 class="modal-title" id="exampleModalLabel">BAST</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                    </div>
                    <form action="function/insert_bast.php" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="row g-3">
                               


                                <div class="col-lg-12">
                                    <label for="date-field" class="form-label">File BAST</label>
									<input type="text" class="form-control" name="id_bast" readonly hidden value="<?=$kodeid?>">
                                	 <input type="text" class="form-control" name="id_aset" readonly hidden value="<?= $_GET['id'] ?>">
                                    <input type="file" class="form-control" name="file_bast" >
                                
				
									
									



                                </div>


                            </div>

                        </div>
                        <div class="modal-footer">
                            <div class="hstack gap-2 justify-content-end">
                                <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


 
        <script src="assets/js/pages/profile-setting.init.js"></script>