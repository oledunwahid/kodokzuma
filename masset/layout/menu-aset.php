 <?php $page =  $_GET['page']; ?>
<div class="app-menu navbar-menu">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <!-- Dark Logo-->
                <a href="index.php?page=Dashboard" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="../assets/images/logo_MAA.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="assets/images/logo_MAAA.png" alt="" height="17">
                    </span>
                </a>
                <!-- Light Logo-->
                <a href="index.php?page=Dashboard" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="assets/images/logo_MAA.png" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="assets/images/logo_MAAA.png" alt="" height="39">
                    </span>
                </a>
                <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
                    <i class="ri-record-circle-line"></i>
                </button>
            </div>

            <div id="scrollbar">
                <div class="container-fluid">

                    <div id="two-column-menu">
                    </div>
					<?php if ($_SESSION['role'] == 'admin') { ?>
                    <ul class="navbar-nav" id="navbar-nav">
                        <li class="menu-title"><span data-key="t-menu">Menu</span></li>
                        <li class="nav-item">
                            <a class="nav-link <?php if ($page == 'Dashboard') echo 'active'; ?>" href="index.php?page=Dashboard">
                                <i class="ri-dashboard-2-line"></i> Dashboard
                            </a>
                        </li>
						  <li class="nav-item">
                            <a class="nav-link <?php if ($page == 'RFM') echo 'active'; ?>" href="index.php?page=RFM">
                                <i class="ri-dashboard-2-line"></i> RFM
                            </a>
                        </li>
						<li class="nav-item">
                            <a class="nav-link <?php if ($page == 'Aset') echo 'active'; ?>" href="index.php?page=Aset">
                                <i class="ri-dashboard-2-line"></i> Aset
                            </a>
                        </li>
						<li class="nav-item">
                            <a class="nav-link <?php if ($page == 'Delivery') echo 'active'; ?>" href="index.php?page=Delivery">
                                <i class="ri-dashboard-2-line"></i> Delivery
                            </a>
                        </li>
						 
						<li class="nav-item">
                            <a class="nav-link <?php if ($page == 'LokasiAset' || $page == 'PT' || $page == 'TypeAset' || $page ==  'KategoriAset' ) echo 'active'; ?>" href="#Sop" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarRefferal">
                               
                                <i class="ri-dashboard-2-line"></i> Administration
                            </a>
							<div class="collapse <?php if ($page == 'LokasiAset' || $page == 'PT' || $page == 'TypeAset' || $page ==  'KategoriAset' ) echo 'show'; ?> menu-dropdown" id="Sop">
                                <ul class="nav nav-sm flex-column">
									<li class="nav-item">
                                        <a href="index.php?page=LokasiAset" class="nav-link <?php if ($page == 'LokasiAset') echo 'active'; ?>" > Lokasi Aset </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="index.php?page=PT" class="nav-link <?php if ($page == 'PT') echo 'active'; ?>" > PT </a>
                                    </li>
									 <li class="nav-item">
                                        <a href="index.php?page=TypeAset" class="nav-link <?php if ($page == 'TypeAset') echo 'active'; ?>" > Type Aset </a>
                                    </li>
									<li class="nav-item">
                                        <a href="index.php?page=KategoriAset" class="nav-link <?php if ($page == 'KategoriAset') echo 'active'; ?>" > Kategori Aset </a>
                                    </li>
                                    
                                    
                                    
                                </ul>
                            </div>
                        </li>
						
						
						
                    </ul>
				<?php	} ?>
					<?php if ($_SESSION['role'] == 'user') { ?>
                    
				<?php	} ?>
                </div>
                <!-- Sidebar -->
            </div>

            <div class="sidebar-background"></div>
        </div>