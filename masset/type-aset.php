 <!--datatable css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <!--datatable responsive css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<?php if(!empty($_SESSION["notif"])){
										   echo $_SESSION["notif"];
										   unset($_SESSION["notif"]);
									   		}	?>
<div class="row">
    
	<div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
									<div class="d-flex align-items-center">
                                    <h5 class="card-title mb-0 flex-grow-1">Type Aset </h5>
										<?php if ($_SESSION['role'] == 'admin') { ?>
										<div class="flex-shrink-0">
									<button class="btn btn-danger add-btn" data-bs-toggle="modal" data-bs-target="#addType"><i class="ri-add-line align-bottom me-1"></i> Create Type</button>
											</div>
										<?php }?>
                                </div>
								</div>
								
                                <div class="card-body">
                                     <table id="buttons-datatables2" class="display table table-bordered dt-responsive" style="width:100%">
                                        <thead>
                                            <tr>
                                               
                                                <th>No.</th>
                                                <th>Nama Type </th>
												<th>Keterangan Type</th>
											
                                                
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                              <?php $sql = mysqli_query($koneksi, "SELECT * FROM type_aset "); 
											$nomor=1;
                    while ($row = mysqli_fetch_assoc($sql)) { 

                    ?>
                                            <tr>
                                                
                                                <td><?=$nomor++ ?></td>
                                                <td><?= $row['nama_type_aset'] ?></td>
                                                <td><?= $row['ket_type_aset'] ?></td>
                                             
                                
                                                <td>
                                                    <div class="dropdown d-inline-block">
                                                        <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <i class="ri-more-fill align-middle"></i>
                                                        </button>
                                                        <ul class="dropdown-menu dropdown-menu-end">

															<?php if ($_SESSION['role'] == 'admin') { ?>
                                                            <li>
															<a class="dropdown-item edit-item-btn" data-bs-toggle="modal" data-bs-target="#update<?=$row['id_type_aset']?>"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                                                            <li>
                                                                							<a class="dropdown-item remove-item-btn" data-bs-toggle="modal" data-bs-target="#delete<?=$row['id_type_aset']?>"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Delete</a>
                                                            </li>
															<?php }?>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
	
	
</div>


		<div class="modal fade zoomIn" id="addType" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-l">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Create Type Aset</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/insert_type_aset.php" method="POST">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-12">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">Nama Type Aset</label>
                                                    <input type="text" class="form-control" placeholder="Masukkan Type Aset" name="nama_type_aset" required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Keterangan Type Aset</label>
                                                    <input type="text" class="form-control" placeholder="Masukan Keterangan Aset" name="ket_type_aset"  />
                                                </div>
                                            </div>
                                           
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add Type Aset</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

		

			<?php $sql = mysqli_query($koneksi, "SELECT * FROM type_aset ");

    while ($row = mysqli_fetch_assoc($sql)) { ?>
			<div class="modal fade zoomIn" id="delete<?=$row['id_type_aset']?>" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="btn-close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="mt-2 text-center">
                                        <lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>
                                        <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                                            <h4>Are you Sure ?</h4>
                                            <p class="text-muted mx-4 mb-0">Are you Sure You want to Remove this Record <br> <b><?=$row['nama_type_aset']; ?></b> ?</p>
                                        </div>
                                    </div>
                                    <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                                        <button type="button" class="btn w-sm btn-light" data-bs-dismiss="modal">Close</button>
										<a style="cursor: pointer;" onclick="location.href='function/delete_type_aset.php?aksi=delete&id=<?=$row['id_type_aset']; ?>'" class="btn w-sm btn-danger">Yes, Delete It!</a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<div class="modal fade zoomIn" id="update<?=$row['id_type_aset']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-l">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Update Type Aset</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/update_type_aset.php" method="POST">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-12">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">Nama Type Aset</label>
													<input type="text"  name="id_type_aset" hidden value="<?=$row['id_type_aset']?>" />
                                                    <input type="text" class="form-control"  name="nama_type_aset" value="<?=$row['nama_type_aset']?>" required/>
                                                </div>
                                            </div>
                                           
                                            <div class="col-lg-12">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Keterangan Type Aset</label>
                                                    <input type="text" class="form-control"  name="ket_type_aset" value="<?=$row['ket_type_aset']?>" />
                                                </div>
                                            </div>
                                            
                                           
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Update Type</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
<?php }?>


<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<!--datatable js-->
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="../assets/js/pages/datatables.init.js"></script>