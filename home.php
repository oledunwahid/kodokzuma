<?php if (!empty($_SESSION["notif"])) {
    echo $_SESSION["notif"];
    unset($_SESSION["notif"]);
}
?>
<div class="h-100">
    <div class="row mb-3 pb-1">
        <div class="col-12">
            <div class="d-flex align-items-lg-center flex-lg-row flex-column">
                <div class="flex-grow-1">
                    <h4 class="fs-16 mb-1">Welcome, <?= $namalogin ?> </h4>
                    <p class="text-muted mb-0">Silahkan Klik Bagian Menu Kiri, Untuk mempelajari seluruh informasi mengenai Perusahaan. </p>
                </div>

            </div><!-- end card header -->
        </div>
        <!--end col-->
    </div>
</div>
<!--end row-->

<div class="row">
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-animate">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-uppercase fw-medium text-muted text-truncate mb-0"> Total Employee</p>
                    </div>

                </div>
                <div class="d-flex align-items-end justify-content-between mt-4">
                    <div>
                        <h4 class="fs-22 fw-semibold ff-secondary mb-4"> <?php
                                                                            $sql = mysqli_query($koneksi, "SELECT
																				login.status_login, 
																				user.nama, 
																				user.lokasi
																			FROM
																				login
																				INNER JOIN
																				user
																				ON 
																					login.idnik = user.idnik
																			WHERE
																				login.status_login = 'Aktif' ");
                                                                            $total_karyawan = mysqli_num_rows($sql);
                                                                            echo "$total_karyawan";
                                                                            ?> </h4>

                    </div>
                    <div class="avatar-sm flex-shrink-0">
                        <span class="avatar-title bg-soft-success rounded fs-3">
                            <i class="bx bx-user-circle text-warning"></i>
                        </span>
                    </div>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-animate">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-uppercase fw-medium text-muted text-truncate mb-0"> Employee HO</p>
                    </div>

                </div>
                <div class="d-flex align-items-end justify-content-between mt-4">
                    <div>
                        <h4 class="fs-22 fw-semibold ff-secondary mb-4"> <?php
                                                                            $sql = mysqli_query($koneksi, "SELECT
																						login.status_login, 
																						user.nama, 
																						user.lokasi
																					FROM
																						login
																						INNER JOIN
																						user
																						ON 
																							login.idnik = user.idnik
																					WHERE
																						login.status_login = 'Aktif' AND
																						user.lokasi = 'HO'");
                                                                            $total_karyawan = mysqli_num_rows($sql);
                                                                            echo "$total_karyawan";
                                                                            ?> </h4>

                    </div>
                    <div class="avatar-sm flex-shrink-0">
                        <span class="avatar-title bg-soft-success rounded fs-3">
                            <i class="bx bx-user-circle text-warning"></i>
                        </span>
                    </div>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-animate">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-uppercase fw-medium text-muted text-truncate mb-0"> Employee OBI</p>
                    </div>

                </div>
                <div class="d-flex align-items-end justify-content-between mt-4">
                    <div>
                        <h4 class="fs-22 fw-semibold ff-secondary mb-4"> <?php
                                                                            $sql = mysqli_query($koneksi, "SELECT
																					login.status_login, 
																					user.nama, 
																					user.lokasi
																				FROM
																					login
																					INNER JOIN
																					user
																					ON 
																						login.idnik = user.idnik
																				WHERE
																					login.status_login = 'Aktif' AND
																					user.lokasi = 'OBI'");
                                                                            $total_karyawan = mysqli_num_rows($sql);
                                                                            echo "$total_karyawan";
                                                                            ?> </h4>

                    </div>
                    <div class="avatar-sm flex-shrink-0">
                        <span class="avatar-title bg-soft-success rounded fs-3">
                            <i class="bx bx-user-circle text-warning"></i>
                        </span>
                    </div>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-animate">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="flex-grow-1 overflow-hidden">
                        <p class="text-uppercase fw-medium text-muted text-truncate mb-0"> Employee BCPM</p>
                    </div>

                </div>
                <div class="d-flex align-items-end justify-content-between mt-4">
                    <div>
                        <h4 class="fs-22 fw-semibold ff-secondary mb-4"> <?php
                                                                            $sql = mysqli_query($koneksi, "SELECT
																					login.status_login, 
																					user.nama, 
																					user.lokasi
																				FROM
																					login
																					INNER JOIN
																					user
																					ON 
																						login.idnik = user.idnik
																				WHERE
																					login.status_login = 'Aktif' AND
																					user.lokasi = 'BCPM'");
                                                                            $total_karyawan = mysqli_num_rows($sql);
                                                                            echo "$total_karyawan";
                                                                            ?> </h4>

                    </div>
                    <div class="avatar-sm flex-shrink-0">
                        <span class="avatar-title bg-soft-success rounded fs-3">
                            <i class="bx bx-user-circle text-warning"></i>
                        </span>
                    </div>
                </div>
            </div><!-- end card body -->
        </div><!-- end card -->
    </div><!-- end col -->

</div>


<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title mb-0">News</h4>
        </div><!-- end card header -->
        <div class="card-body">
            <p class="text-muted">Informasi Employee Portal Terbaru</p>

            <!-- Swiper -->
            <div class="swiper mySwiper pb-4">
                <div class="swiper-wrapper">

                    <?php $sql = mysqli_query($koneksi, "SELECT *
											FROM
												news
												
												 ");

                    while ($row = mysqli_fetch_assoc($sql)) {
                    ?>
                        <div class="swiper-slide">
                            <div class="gallery-box ">

                                <div class="dash-collection overflow-hidden rounded-top position-relative">
                                    <?php
                                    $file_media =  substr($row['image_news'], -3);
                                    if ($file_media  == 'mp4') { ?>
                                        <video height="220" controls>
                                            <source src="file/news/<?= $row['image_news'] ?>" type="video/mp4">
                                        </video>

                                    <?php
                                    } else { ?>
                                        <img height="220" src="file/news/<?= $row['image_news'] ?>" alt="" />
                                    <?php } ?>

                                    <div class="content position-absolute bottom-0 m-2 p-2 start-0 end-0 rounded d-flex align-items-center">
                                        <div class="flex-grow-1">
                                            <a href="index.php?page=ViewNews&id=<?= $row['idnews'] ?>">
                                                <p class="text-white fs-16 mb-1"><?= $row['title'] ?></p>
                                            </a>

                                        </div>

                                    </div>
                                </div>


                                <div class="box-content">
                                    <div class="d-flex align-items-center mt-1">
                                        <div class="flex-grow-1 text-muted">Category: <a href="" class="text-body text-truncate"><?= $row['category'] ?></a></div>
                                        <div class="flex-shrink-0">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>



                </div>
                <div class="swiper-pagination swiper-pagination-dark"></div>
            </div>
        </div><!-- end card-body -->
    </div><!-- end card -->
</div>





<script src="assets/libs/swiper/swiper-bundle.min.js"></script>
<script src="assets/js/pages/nft-landing.init.js"></script>