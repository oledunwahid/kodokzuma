<?php if (!empty($_SESSION["notif"])) {
    echo $_SESSION["notif"];
    unset($_SESSION["notif"]);
}
function tglformat($tanggalRaw)
{
    $tanggal = new DateTime($tanggalRaw);
    return $tanggal->format('d M y H:i');
}
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.34/moment-timezone-with-data.min.js"></script>

<div class="row g-4 mb-3">
    <div class="col-sm-auto">
        <?php if ($_SESSION['role'] == 'admin' && in_array($menulogin, ['5', '9'])) : ?>
            <div>
                <a href="index.php?page=AddAuction" class="btn btn-success">
                    <i class="ri-add-line align-bottom me-1"></i> Add New
                </a>
            </div>
        <?php endif; ?>
    </div>
    <div class="col-sm">
        <!-- Konten kolom kedua bisa ditambahkan di sini -->
    </div>
</div>





<?php
$batas = 12;
$halaman = isset($_GET['halaman']) ? (int)$_GET['halaman'] : 1;
$halaman_awal = ($halaman > 1) ? ($halaman * $batas) - $batas : 0;
$previous = $halaman - 1;
$next = $halaman + 1;
$data = mysqli_query($koneksi, "select id_lelang from lelang");
$jumlah_data = mysqli_num_rows($data);
$total_halaman = ceil($jumlah_data / $batas); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <?php
            $sql = mysqli_query($koneksi, "SELECT lelang.*, foto_lelang.foto, (SELECT COUNT(*) FROM penawaran_lelang WHERE id_lelang = lelang.id_lelang) AS total_peserta, (SELECT COUNT(*) FROM action_history WHERE id_history = lelang.id_lelang) AS total_view, (SELECT MAX(harga_penawaran) FROM penawaran_lelang WHERE id_lelang = lelang.id_lelang) AS harga_tawaran_tertinggi FROM lelang INNER JOIN foto_lelang ON foto_lelang.id_lelang = lelang.id_lelang WHERE lelang.status_lelang = 'Aktif' GROUP BY lelang.id_lelang ORDER BY lelang.id_lelang ASC LIMIT $halaman_awal, $batas");
            while ($row = mysqli_fetch_assoc($sql)) {
            ?>
                <div class="col-xxl-3 col-lg-4 col-md-6 product-item upto-15">
                    <?php if (in_array($_SESSION['role'], ['admin']) && in_array($menulogin, ['5', '9'])) { ?>
                        <div class="dropdown">
                            <a href="#" class="text-muted" data-bs-toggle="dropdown"><i class="ri-more-fill"></i></a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="index.php?page=UpdateAuction&id=<?= $row['id_lelang'] ?>"><i class="ri-edit-2-line"></i> Edit</a></li>
                                <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#delete<?= $row['id_lelang'] ?>"><i class="ri-delete-bin-5-line"></i> Delete</a></li>
                            </ul>
                        </div>
                    <?php } ?>
                    <div class="card explore-box card-animate">
                        <div class="position-relative rounded overflow-hidden">
                            <img src="file/lelang/<?= $row['foto'] ?>" alt="" class="card-img-top explore-img">
                            <div class="discount-time" data-start-time="<?= $row['tgl_lelang'] ?>" data-end-time="<?= $row['end_tgl_lelang'] ?>">
                                <h5 id="auction-time-<?= $row['id_lelang'] ?>" class="mb-0 text-white"></h5>
                            </div>
                        </div>

                        <div class="card-body">


                            <!-- Penambahan ID Lelang di sini -->
                            <h5 class="mb-2">ID: <?= $row['id_lelang'] ?></h5>


                            <h6 class="fs-16 mb-3"><a href="index.php?page=ViewAuction&id=<?= $row['id_lelang'] ?>"><?= $row['nama_barang'] ?></a></h6>
                            <div class="mb-2">
                                <span class="text-muted float-end"><?= $row['kategori_barang'] ?></span>
                                <span class="text-muted"><?= $row['merk_barang'] ?></span>

                            </div>
                            <div class="d-flex justify-content-between">
                                <span class="text-muted "><i class="ri-team-fill  align-bottom me-1"></i><?= $row['total_peserta'] ?></span>
                                <span class="text-muted"><i class="ri-eye-fill  align-bottom me-1"></i><?= $row['total_view'] ?></span>
                                <span class="text-muted">
                                    <i class="ri-funds-fill align-bottom me-1"></i>
                                    <?php
                                    // Cek apakah ada tawaran untuk lelang ini
                                    if ($row['harga_tawaran_tertinggi'] !== NULL) {
                                        // Tampilkan harga tawaran tertinggi jika ada
                                        echo rupiah($row['harga_tawaran_tertinggi']);
                                    } else {
                                        // Tampilkan harga lelang awal jika belum ada tawaran
                                        echo rupiah($row['harga_lelang']);
                                    }
                                    ?>
                                </span>
                            </div>




                        </div>
                        <div class="card-footer border-top border-top-dashed">
                            <div class="d-flex align-items-center">

                                <div class="flex-grow-1 fs-14">
                                    Starting Bid: <span class="fw-medium">


                                        <h3 class="text-success"><i class="ri-price-tag-3-fill  align-bottom me-1"></i><?= rupiah($row['harga_lelang']) ?>
                                    </span></h3>

                                    <div class="mt-4 hstack gap-2">
                                        <a href="index.php?page=ViewAuction&id=<?= $row['id_lelang'] ?>" class="btn btn-soft-primary w-100">View</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

</div>

<?php
$jumlah_number = 1;
$start_number = ($halaman > $jumlah_number) ? $halaman - $jumlah_number : 1;
$end_number = ($halaman < ($total_halaman - $jumlah_number)) ? $halaman + $jumlah_number : $total_halaman;    ?>
<div class="row g-0 text-center text-sm-start align-items-center mb-3">
    <div class="col-sm-6">
        <?php
        // Hitung index awal dan akhir item yang ditampilkan pada halaman saat ini
        $index_awal = $halaman_awal + 1; // Karena index dimulai dari 1 untuk tampilan user
        $index_akhir = min($halaman_awal + $batas, $jumlah_data);

        // Tampilkan informasi showing
        echo "<p class='mb-sm-0'>Showing $index_awal to $index_akhir of $jumlah_data entries</p>";
        ?>
    </div>
    <div class="col-sm-6">
        <ul class="pagination pagination-separated justify-content-center justify-content-sm-end mb-sm-0">
            <li class="page-item">
                <a class="page-link" <?= ($halaman > 1) ? "href='?page=Auction&halaman=$previous'" : ''; ?>>Previous</a>
            </li>
            <?php for ($x = $start_number; $x <= $end_number; $x++) { ?>
                <li class="page-item<?= ($halaman == $x) ? ' active' : ''; ?>">
                    <a class="page-link" href="?page=Auction&halaman=<?= $x; ?>"><?= $x; ?></a>
                </li>
            <?php } ?>
            <li class="page-item">
                <a class="page-link" <?= ($halaman < $total_halaman) ? "href='?page=Auction&halaman=$next'" : ''; ?>>Next</a>
            </li>
        </ul>
    </div>
</div>






<?php $sql = mysqli_query($koneksi, "SELECT * FROM lelang ");

while ($row = mysqli_fetch_assoc($sql)) { ?>



    <div class="modal fade zoomIn" id="delete<?= $row['id_lelang'] ?>" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="btn-close"></button>
                </div>
                <div class="modal-body">
                    <div class="mt-2 text-center">
                        <lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>
                        <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                            <h4>Are you Sure ?</h4>
                            <p class="text-muted mx-4 mb-0">Are you Sure You want to Remove this Record <br> <b><?= $row['nama_barang']; ?></b> ?</p>
                        </div>
                    </div>
                    <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                        <button type="button" class="btn w-sm btn-light" data-bs-dismiss="modal">Close</button>
                        <a style="cursor: pointer;" onclick="location.href='function/delete_lelang.php?aksi=delete&id=<?= $row['id_lelang']; ?>'" class="btn w-sm btn-danger">Yes, Delete It!</a>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>



<script>
    document.addEventListener("DOMContentLoaded", function() {
        var countdownElements = document.querySelectorAll(".discount-time");

        countdownElements.forEach(function(element) {
            // Gunakan moment-timezone untuk parsing waktu dengan zona waktu Asia/Jakarta
            var startTime = moment.tz(element.getAttribute("data-start-time"), "Asia/Jakarta").valueOf();
            var endTime = moment.tz(element.getAttribute("data-end-time"), "Asia/Jakarta").valueOf();
            var now = moment.tz("Asia/Jakarta").valueOf();
            var targetTime = now < startTime ? startTime : endTime;
            var elementId = element.querySelector("h5").id;

            setInterval(function() {
                countDownTimer(targetTime, elementId, now < startTime);
            }, 1000);
        });

        function countDownTimer(targetTime, elementId, isPreAuction) {
            var timeLeft = targetTime - moment.tz("Asia/Jakarta").valueOf(),
                days = Math.floor(timeLeft / 864e5),
                hours = Math.floor(timeLeft % 864e5 / 3600000),
                minutes = Math.floor(timeLeft % 3600000 / 60000),
                seconds = Math.floor(timeLeft % 60000 / 1000);
            days = days < 10 ? "0" + days : days;
            hours = hours < 10 ? "0" + hours : hours;
            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            var countdownText = days + " : " + hours + " : " + minutes + " : " + seconds;
            if (isPreAuction) {
                countdownText = "Starts in " + countdownText;
            }
            document.getElementById(elementId).textContent = countdownText;

            if (timeLeft < 0) {
                document.getElementById(elementId).textContent = isPreAuction ? "Auction has started" : "Auction has ended";
            }
        }
    });
</script>