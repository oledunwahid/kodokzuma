<link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/4.3.0/css/fixedColumns.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.4.2/css/buttons.dataTables.min.css">

<link href="https://cdn.datatables.net/searchbuilder/1.6.0/css/searchBuilder.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/datetime/1.5.1/css/dataTables.dateTime.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.19/dist/sweetalert2.min.css">

<script src="https://code.jquery.com/jquery-3.7.0.js"></script>
<script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/fixedcolumns/4.3.0/js/dataTables.fixedColumns.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.4.2/js/buttons.html5.min.js"></script>

<script src="https://cdn.datatables.net/searchbuilder/1.6.0/js/dataTables.searchBuilder.min.js"></script>
<script src="https://cdn.datatables.net/datetime/1.5.1/js/dataTables.dateTime.min.js"></script>

<script src="assets/js/pages/project-create.init.js"></script>
<script src="assets/js/pages/ticketdetail.init.js"></script>
<!-- Sweet Alert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.19/dist/sweetalert2.all.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAb1CBsZR16dK-kgFlEqvM61elLrieLhJk&libraries=places"></script>


<style>
    .alert-warning {
        margin-top: 20px;
        /* Tambahkan margin atas agar tidak terlalu dekat dengan elemen sebelumnya */
        margin-bottom: 20px;
        /* Tambahkan margin bawah agar tidak terlalu dekat dengan tabel */
        font-weight: bold;
        /* Jika ingin teksnya bold */
    }
</style>
<?php
$sql = mysqli_query($koneksi, "SELECT * FROM access_level WHERE idnik = $niklogin");
$row = mysqli_fetch_assoc($sql);
?>
<?php if (isset($row['admin']) && ($row['admin'] == '1' || $row['driver'] == '1')) : ?>
    <?php
    $sql1 = mysqli_query($koneksi, "SELECT
                user.nama AS nama_request,
                rf_fleet_booking.id_booking,
                rf_fleet_booking.id_user,
                rf_fleet_booking.datetime_booking_start,
                rf_fleet_booking.datetime_booking_end,
                rf_fleet_booking.pick_up_point,
                rf_fleet_booking.drop_off_point,
                rf_fleet_booking.drop_off_link,
                rf_fleet_booking.pick_up_link,
                rf_fleet_booking.description
            FROM
                rf_fleet_booking
            INNER JOIN
                `user`
            ON 
                rf_fleet_booking.id_user = `user`.idnik
            WHERE
                status_booking = 'Approved'
                AND id_driver = '$niklogin' 
                AND DATE(datetime_booking_start) = CURDATE()
            ORDER BY
                datetime_booking_start ASC
            LIMIT 1;                
            ");
    $row1 = mysqli_fetch_assoc($sql1);

    if ($row1) { ?>
        <div class="row">
            <div class="col-lg-6">
                <div class="card border-primary">
                    <div class="card-body">
                        <div class="row mb-4">
                            <h6 class="card-title mb-3 text-uppercase text-center">
                                <strong>User Booking Details</strong>
                            </h6>
                            <div class="col-md-12 text-center mb-3">
                                <img src="file/profile/laki-laki.jpg" class="img-fluid rounded-circle" alt="Profile Image" style="width: 100px;">
                            </div>
                            <p class="mb-3 text-center"><span class="text-uppercase"><strong><?= $row1["nama_request"] ?></strong></span></p>
                            <div class="col-md-12 text-start">
                                <p class="mb-2" style="text-align: justify;"><strong>Pick-up:</strong><br> <span class="text-muted"><?= $row1["pick_up_point"] ?></span></p>
                                <p class="mb-2" style="text-align: justify;"><strong>Drop-off:</strong><br><span class="text-muted"><?= $row1["drop_off_point"] ?></span></p>
                                <p class="mb-2" style="text-align: justify;"><strong>Description:</strong><br><span class="text-muted"><?= $row1["description"] ?></span></p>
                            </div>

                            <!-- Tombol pick-up dan drop-off -->
                            <div class="d-flex justify-content-center pt-3">
                                <button class="btn btn-primary me-2 ">
                                    <a href="<?= $row1["pick_up_link"] ?>" class="text-white">Pick-up Link</a>
                                </button>
                                <button class="btn btn-primary">
                                    <a href="<?= $row1["drop_off_link"] ?>" class="text-white">Drop-off Link</a>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card border-primary">
                    <div class="card-body text-center">
                        <div class="mb-4">
                            <h6 class="card-title mb-2 text-uppercase">
                                <strong>Driver Trip Button </strong>
                                <h3 class="mb-3">
                                    <?= $row1["id_booking"] ?>
                                </h3>
                            </h6>
                            <div class="mb-2">
                                <lord-icon src="https://cdn.lordicon.com/kbtmbyzy.json" trigger="loop" colors="primary:#405189,secondary:#02a8b5" style="width:90px;height:90px"></lord-icon>
                            </div>
                        </div>

                        <div class="mb-4">
                            <h5 class="fs-14 mb-3">Button Update Timestamp</h5>
                            <?php
                            $sql_booking = mysqli_query($koneksi, "SELECT
                                rf_fleet_booking.id_booking, 
                                rf_fleet_booking.status_booking, 
                                rf_fleet_booking.datetime_booking_start, 
                                rf_fleet_booking.id_driver,
                                rf_fleet_booking.datetime_booking_end,
                                rf_fleet_booking.pick_up_point,
                                rf_fleet_booking.drop_off_point,
                                rf_fleet_booking.description
                            FROM
                                rf_fleet_booking
                                INNER JOIN
                                `user`
                                ON 
                            rf_fleet_booking.id_driver = `user`.idnik
                            WHERE status_booking = 'Approved' AND id_driver = '$niklogin' 
                            AND  DATE(datetime_booking_start) = CURDATE()
                            ORDER BY datetime_booking_start ASC
                            LIMIT 1;");
                            $row_booking = mysqli_fetch_assoc($sql_booking);
                            $id_booking = isset($row_booking['id_booking']) ? $row_booking['id_booking'] : '';

                            $cekbooking = mysqli_num_rows($sql_booking);

                            if ($cekbooking == 1) {
                                $sql_log = mysqli_query($koneksi, "SELECT *
                                FROM rf_driver_log where id_booking = '$id_booking' ORDER BY timestamp_log DESC ");
                                $row_log = mysqli_fetch_assoc($sql_log);

                                if (empty($row_log['status_driver_activity'])) {
                            ?>
                                    <form method="POST" action="function/insert_driver_log.php">
                                        <input type="text" hidden value="<?= $id_booking ?>" name="id_booking" />
                                        <input type="text" hidden value="<?= $id_booking ?>" name="id_booking" />
                                        <input type="text" hidden id="latitudeSaatIni" name="latitude" />
                                        <input type="text" hidden id="longitudeSaatIni" name="longitude" />
                                        <div id="map"></div>

                                        <button class="btn btn-success btn-sm btn-scheduled" name="actionOTW" value="On The Way">
                                            <i class="ri-play-circle-line align-bottom me-1"></i>Start Trip (On The Way)
                                        </button>
                                    </form>
                                <?php
                                } elseif ($row_log['status_driver_activity'] == 'On The Way') {
                                ?>
                                    <form method="POST" action="function/insert_driver_log.php">
                                        <input type="text" hidden value="<?= $id_booking ?>" name="id_booking" />
                                        <input type="text" hidden value="<?= $id_booking ?>" name="id_booking" />
                                        <input type="text" hidden value="<?= $id_booking ?>" name="id_booking" />
                                        <input type="text" hidden id="latitudeSaatIni" name="latitude" />
                                        <input type="text" hidden id="longitudeSaatIni" name="longitude" />
                                        <div id="map"></div>
                                        <button class="btn btn-success btn-sm btn-on-the-way" name="actionArrived" value="Arrived">
                                            <i class="ri-play-circle-line align-bottom me-1"></i>Arrived
                                        </button>
                                    </form>
                                <?php
                                } elseif ($row_log['status_driver_activity'] == 'Arrived') {
                                ?>
                                    <form method="POST" action="function/insert_driver_log.php">
                                        <input type="text" hidden value="<?= $id_booking ?>" name="id_booking" />
                                        <input type="text" hidden value="<?= $id_booking ?>" name="id_booking" />
                                        <input type="text" hidden value="<?= $id_booking ?>" name="id_booking" />
                                        <input type="text" hidden id="latitudeSaatIni" name="latitude" />
                                        <input type="text" hidden id="longitudeSaatIni" name="longitude" />
                                        <div id="map"></div>
                                        <button class="btn btn-success btn-sm btn-arrived" name="actionFinished" value="Finished">
                                            <i class="ri-play-circle-line align-bottom me-1"></i>Finished Trip
                                        </button>
                                        <button class="btn btn-success btn-sm btn-arrived" name="actionWaiting" value="Waiting">
                                            <i class="ri-play-circle-line align-bottom me-1"></i>Waiting
                                        </button>
                                    </form>

                                <?php } elseif ($row_log['status_driver_activity'] == 'Waiting') { ?>
                                    <form method="POST" action="function/insert_driver_log.php">
                                        <input type="text" hidden value="<?= $id_booking ?>" name="id_booking" />
                                        <input type="text" hidden value="<?= $id_booking ?>" name="id_booking" />
                                        <input type="text" hidden id="latitudeSaatIni" name="latitude" />
                                        <input type="text" hidden id="longitudeSaatIni" name="longitude" />
                                        <div id="map"></div>
                                        <input type="text" hidden value="<?= $id_booking ?>" name="id_booking" />
                                        <button class="btn btn-success btn-sm btn-arrived" name="actionFinished" value="Finished">
                                            <i class="ri-play-circle-line align-bottom me-1"></i>Finished Trip
                                        </button>
                                    </form>
                            <?php
                                }
                            } else {
                                echo "<div class='alert alert-warning text-center' role='alert'>TIDAK ADA BOOKING HARI INI</div>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
    } else {
        echo "<div class='alert alert-warning text-center' role='alert'>TIDAK ADA BOOKING HARI INI</div>";
    }
    ?>
<?php endif; ?>



<script src="assets/libs/@ckeditor/ckeditor5-build-classic/build/ckeditor.js"></script>
<script src="assets/js/pages/project-create.init.js"></script>
<script src="assets/js/pages/ticketdetail.init.js"></script>
<!-- Sweet Alert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.19/dist/sweetalert2.all.min.js"></script>

<script>
    var map, directionsService, directionsRenderer;
    var autocompleteAwal, autocompleteTujuan;

    function initializeMapAndAutocomplete() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: -6.2088,
                lng: 106.8456
            },
            zoom: 10
        });
        directionsService = new google.maps.DirectionsService();
        directionsRenderer = new google.maps.DirectionsRenderer();
        directionsRenderer.setMap(map);

        autocompleteAwal = new google.maps.places.Autocomplete(document.getElementById('alamatAwal'));
        autocompleteTujuan = new google.maps.places.Autocomplete(document.getElementById('alamatTujuan'));

        autocompleteAwal.addListener('place_changed', function() {
            if (document.getElementById('alamatTujuan').value) {
                calculateAndDisplayRoute();
            }
        });

        autocompleteTujuan.addListener('place_changed', function() {
            if (document.getElementById('alamatAwal').value) {
                calculateAndDisplayRoute();
            }
        });

        getCurrentLocation();
    }


    function getCurrentLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;
                document.getElementById('latitudeSaatIni').value = lat;
                document.getElementById('longitudeSaatIni').value = lng;
                // Optionally, update the map center to the current location
                map.setCenter(new google.maps.LatLng(lat, lng));
            }, function() {
                alert("Geolocation is not supported by this browser.");
            });
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    }
    google.maps.event.addDomListener(window, 'load', initializeMapAndAutocomplete);
</script>