<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
<!--datatable responsive css-->
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">



<div class="row">
    <?php
    $sql7 = mysqli_query($koneksi, "SELECT * FROM access_level WHERE idnik = $niklogin");
    $row7 = mysqli_fetch_assoc($sql7);
    ?>

    <div class="col-xxl-3 col-sm-6">
        <div class="card card-animate">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        <?php if (isset($row7['admin']) && ($row7['admin'] == '1' || ($row7['ga_pic_driver'] == '1'))) {
                            $sql = mysqli_query($koneksi, "SELECT id_booking FROM rf_fleet_booking");
                            $totalTicket = mysqli_num_rows($sql);
                        } else {
                            $sql = mysqli_query($koneksi, "SELECT id_booking FROM rf_fleet_booking where id_user='$niklogin' ");
                            $totalTicket = mysqli_num_rows($sql);
                        }
                        ?>
                        <p class="fw-medium text-muted mb-0">Total Booking</p>
                        <h2 class="mt-4 ff-secondary fw-semibold">
                            <span class="counter-value" data-target="<?= htmlspecialchars($totalTicket) ?>"></span>
                        </h2>
                    </div>

                    <div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-soft-info text-info rounded-circle fs-4">
                                <i class="ri-ticket-2-line"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end col-->

    <!-- (Pending, Closed, On Process) -->
    <div class="col-xxl-3 col-sm-6">
        <div class="card card-animate">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        <?php
                        if (isset($row7['admin']) && ($row7['admin'] == '1' || ($row7['ga_pic_driver'] == '1'))) {
                            $sql1 = mysqli_query($koneksi, "SELECT id_booking FROM rf_fleet_booking WHERE status_booking = 'Pending'");
                            $PendingTiket = mysqli_num_rows($sql1);
                        } else {
                            $sql1 = mysqli_query($koneksi, "SELECT id_booking FROM rf_fleet_booking WHERE status_booking = 'Pending' AND (id_user = '$niklogin' OR id_driver = '$niklogin')");
                            $PendingTiket = mysqli_num_rows($sql1);
                        }
                        ?>
                        <p class="fw-medium text-muted mb-0">Created Booking</p>
                        <h2 class="mt-4 ff-secondary fw-semibold"><span class="counter-value" data-target="<?= $PendingTiket ?>">0</span></h2>
                    </div>
                    <div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-soft-info text-info rounded-circle fs-4">
                                <i class="mdi mdi-timer-sand"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xxl-3 col-sm-6">
        <div class="card card-animate">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        <?php
                        if (isset($row7['admin']) && ($row7['admin'] == '1' || ($row7['ga_pic_driver'] == '1'))) {
                            $sql1 = mysqli_query($koneksi, "SELECT id_booking FROM rf_fleet_booking WHERE status_booking = 'On Process'");
                            $ProcessTicket = mysqli_num_rows($sql1);
                        } else {
                            $sql1 = mysqli_query($koneksi, "SELECT id_booking FROM rf_fleet_booking WHERE status_booking = 'On Process' AND (id_user = '$niklogin' OR id_driver = '$niklogin')");
                            $ProcessTicket = mysqli_num_rows($sql1);
                        }
                        ?>
                        <p class="fw-medium text-muted mb-0">Approve Booking</p>
                        <h2 class="mt-4 ff-secondary fw-semibold"><span class="counter-value" data-target="<?= $ProcessTicket ?>">0</span></h2>
                    </div>
                    <div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-soft-info text-info rounded-circle fs-4">
                                <i class="mdi mdi-timer-sand"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xxl-3 col-sm-6">
        <div class="card card-animate">
            <div class="card-body">
                <div class="d-flex justify-content-between">
                    <div>
                        <?php
                        if (isset($row7['admin']) && ($row7['admin'] == '1' || ($row7['ga_pic_driver'] == '1'))) {
                            $sql2 = mysqli_query($koneksi, "SELECT id_booking FROM rf_fleet_booking WHERE status_booking = 'Closed'");
                            $ClosedTicket = mysqli_num_rows($sql2);
                        } else {
                            $sql2 = mysqli_query($koneksi, "SELECT id_booking FROM rf_fleet_booking WHERE status_booking = 'Closed' AND (id_user = '$niklogin' OR id_driver = '$niklogin')");
                            $ClosedTicket = mysqli_num_rows($sql2);
                        }
                        ?>

                        <p class="fw-medium text-muted mb-0">Done Booking</p>
                        <h2 class="mt-4 ff-secondary fw-semibold"><span class="counter-value" data-target="<?= $ClosedTicket ?>">0</span></h2>

                    </div>
                    <div>
                        <div class="avatar-sm flex-shrink-0">
                            <span class="avatar-title bg-soft-info text-info rounded-circle fs-4">
                                <i class="ri-mail-close-line"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="card" id="ticketsList">
            <div class="card-header border-0">
                <div class="d-flex align-items-center">
                    <div class="card-title mb-0 flex-grow-1 flex">
                        <h5>Driver Booking List</h5>
                        <h6>Manages requests related to driver bookings for transportation needs.
                        </h6>
                    </div>
                    <div class="flex-shrink-0">
                        <a href="index.php?page=AddBooking" class="btn btn-danger add-btn">
                            <i class="ri-add-line align-bottom me-1"></i> Create Booking
                        </a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <!-- Main Table -->
                    <div class="card-body border border-dashed border-end-0 border-start-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table id="model-datatables" class="table table-bordered nowrap table-striped align-middle dt-responsive" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>ID Booking Driver</th>
                                                    <th>Create Book Time</th>
                                                    <th>Start Date</th>
                                                    <th>End Date</th>
                                                    <th>Pick-Up Address</th>
                                                    <th>Drop-Off Address</th>
                                                    <th>Nama Request</th>
                                                    <th>Description</th>
                                                    <th>Status Booking</th>
                                                    <th>Driver</th>
                                                    <th>Status Driver</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sql7 = mysqli_query($koneksi, "SELECT * FROM access_level WHERE idnik = $niklogin ");
                                                $row7 = mysqli_fetch_assoc($sql7);
                                                ?>
                                                <?php
                                                $sql7 = mysqli_query($koneksi, "SELECT
                                                user.nama AS nama_request,
                                                rf_fleet_booking.id_booking,
                                                rf_fleet_booking.create_booking_time,
                                                rf_fleet_booking.id_driver,
                                                rf_fleet_booking.id_user,
                                                rf_fleet_booking.datetime_booking_start,
                                                rf_fleet_booking.datetime_booking_end,
                                                rf_fleet_booking.pick_up_point,
                                                rf_fleet_booking.drop_off_point,
                                                rf_fleet_booking.description,
                                                rf_fleet_booking.status_booking,
                                                COALESCE(MAX(rf_driver_log.status_driver_activity), 'scheduled') AS status_log
                                            FROM
                                                rf_fleet_booking
                                            LEFT JOIN
                                                rf_driver_log ON rf_fleet_booking.id_booking = rf_driver_log.id_booking
                                            LEFT JOIN 
                                                user ON rf_fleet_booking.id_user = user.idnik
                                            WHERE rf_fleet_booking.id_user = '$niklogin'
                                            GROUP BY
                                                rf_fleet_booking.id_booking,
                                                rf_fleet_booking.create_booking_time,
                                                rf_fleet_booking.id_driver,
                                                rf_fleet_booking.id_user,
                                                rf_fleet_booking.datetime_booking_start,
                                                rf_fleet_booking.datetime_booking_end,
                                                rf_fleet_booking.pick_up_point,
                                                rf_fleet_booking.drop_off_point,
                                                rf_fleet_booking.description,
                                                rf_fleet_booking.status_booking");
                                                while ($row6 = mysqli_fetch_assoc($sql7)) {
                                                ?>
                                                    <tr>
                                                        <td><?= $row6['id_booking']; ?></td>
                                                        <td><?= $row6['create_booking_time'] ?></td>
                                                        <td><?= $row6['datetime_booking_start'] ?></td>
                                                        <td><?= $row6['datetime_booking_end'] ?></td>
                                                        <td><?= $row6['pick_up_point'] ?></td>
                                                        <td><?= $row6['drop_off_point'] ?></td>
                                                        <td><?= $row6['nama_request'] ?></td>
                                                        <td><?= $row6['description'] ?></td>
                                                        <td><?= $row6['status_booking'] ?></td>
                                                        <td><?= $row6['id_driver'] ?></td>
                                                        <td><?= $row6['status_log'] ?></td>
                                                    </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--end row-->
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->
        </div>
        <!--end col-->
    </div>
    <!--end row-->
    <!-- table all user -->
    <div class="row">
        <?php if (isset($row7['admin']) && ($row7['admin'] == '1' || ($row7['ga_pic_driver'] == '1'))) { ?>
            <div class="col-lg-12">
            <?php } else { ?>
                <div class="col-lg-8">
                <?php } ?>
                <div class="card" id="ticketsList">
                    <div class="card-header border-0">
                        <div class="d-flex align-items-center">
                            <?php if (isset($row7['admin']) && ($row7['admin'] == '1' || ($row7['ga_pic_driver'] == '1'))) { ?>
                                <div class="card-title mb-0 flex-grow-1 flex">
                                    <h5>Total Driver Booking Requests</h5>
                                </div>
                            <?php } else { ?>
                                <div class="card-title mb-0 flex-grow-1 flex">
                                    <h5>Driver Booking Requests from Other Users</h5>
                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="card-body border border-dashed border-end-0 border-start-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table id="model-datatables" class="table table-bordered nowrap table-striped align-middle dt-responsive" style="width:100%">
                                            <thead class="table-light">
                                                <?php if (isset($row7['admin']) && ($row7['admin'] == '1' || ($row7['ga_pic_driver'] == '1'))) { ?>
                                                    <tr>
                                                        <th>ID Booking Driver</th>
                                                        <th>Nama Request</th>
                                                        <th>Create Book</th>
                                                        <th>Start Date</th>
                                                        <th>End Date</th>
                                                        <th>Pick-Up Address</th>
                                                        <th>Drop-Off Address</th>
                                                        <th>Description</th>
                                                        <th>Status Booking</th>
                                                        <th>Driver</th>
                                                        <th>Status Driver</th>
                                                        <th>Action</th>
                                                    </tr>
                                                <?php } else { ?>
                                                    <tr>
                                                        <th>Nama Request</th>
                                                        <th>Create Book</th>
                                                        <th>Start Date</th>
                                                        <th>End Date</th>
                                                        <th>Status Booking</th>
                                                        <th>Driver</th>
                                                        <th>Status Driver</th>
                                                    </tr>
                                                <?php } ?>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $sql7 = mysqli_query($koneksi, "SELECT * FROM access_level WHERE idnik =$niklogin ");
                                                $row7 = mysqli_fetch_assoc($sql7);
                                                ?>
                                                <?php
                                                if (isset($row7['admin']) && ($row7['admin'] == '1' || ($row7['ga_pic_driver'] == '1'))) {
                                                    $sql7 = mysqli_query($koneksi, "SELECT
                                                user_request.nama AS nama_request,
                                                driver.nama AS nama_driver,
                                                rf_fleet_booking.id_booking,
                                                rf_fleet_booking.create_booking_time,
                                                rf_fleet_booking.id_driver,
                                                rf_fleet_booking.id_user,
                                                rf_fleet_booking.datetime_booking_start,
                                                rf_fleet_booking.datetime_booking_end,
                                                rf_fleet_booking.pick_up_point,
                                                rf_fleet_booking.drop_off_point,
                                                rf_fleet_booking.description,
                                                rf_fleet_booking.status_booking,
                                                COALESCE ( ( SELECT rf_driver_log.status_driver_activity FROM rf_driver_log WHERE rf_driver_log.id_booking = rf_fleet_booking.id_booking ORDER BY rf_driver_log.timestamp_log DESC LIMIT 1 ), 'scheduled' ) AS status_log 
                                            FROM
                                                rf_fleet_booking
                                                LEFT JOIN user AS user_request ON rf_fleet_booking.id_user = user_request.idnik 
                                                LEFT JOIN user AS driver ON rf_fleet_booking.id_driver = driver.idnik                                            
                                            GROUP BY
                                                rf_fleet_booking.id_booking,
                                                rf_fleet_booking.create_booking_time,
                                                rf_fleet_booking.id_driver,
                                                rf_fleet_booking.id_user,
                                                rf_fleet_booking.datetime_booking_start,
                                                rf_fleet_booking.datetime_booking_end,
                                                rf_fleet_booking.pick_up_point,
                                                rf_fleet_booking.drop_off_point,
                                                rf_fleet_booking.description,
                                                rf_fleet_booking.status_booking,
                                                user_request.nama,
                                                driver.nama 
                                            ORDER BY
                                                rf_fleet_booking.create_booking_time DESC;
                                            ;");
                                                    while ($row6 = mysqli_fetch_assoc($sql7)) {
                                                ?>
                                                        <tr>
                                                            <td><a href="index.php?page=DetailBooking&id=<?= $row6['id_booking']; ?>"><?= $row6['id_booking'] ?></a></td>
                                                            <td><?= $row6['nama_request'] ?></td>
                                                            <td><?= $row6['create_booking_time'] ?></td>
                                                            <td><?= $row6['datetime_booking_start'] ?></td>
                                                            <td><?= $row6['datetime_booking_end'] ?></td>
                                                            <td><?= $row6['pick_up_point'] ?></td>
                                                            <td><?= $row6['drop_off_point'] ?></td>
                                                            <td><?= $row6['description'] ?></td>
                                                            <td><?= $row6['status_booking'] ?></td>
                                                            <td><?= $row6['nama_driver'] ?></td>
                                                            <td><?= $row6['status_log'] ?></td>
                                                            <td>
                                                                <div class="dropdown d-inline-block">
                                                                    <button class="btn btn-soft-secondary btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                                        <i class="ri-more-fill align-middle"></i>
                                                                    </button>
                                                                    <ul class="dropdown-menu dropdown-menu-end">
                                                                        <li class="text-center"> <!-- Tambahkan class text-center di sini -->
                                                                            <a href="index.php?page=DetailBooking&id=<?= $row6['id_booking'] ?>">
                                                                                <i class="ri-pencil-fill align-middle me-2 text-muted"></i>Update Booking
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    }
                                                } else {
                                                    $sql7 = mysqli_query($koneksi, "SELECT
                                                user_request.nama AS nama_request,
                                                driver.nama AS nama_driver,
                                                rf_fleet_booking.id_booking,
                                                rf_fleet_booking.create_booking_time,
                                                rf_fleet_booking.id_driver,
                                                rf_fleet_booking.id_user,
                                                rf_fleet_booking.datetime_booking_start,
                                                rf_fleet_booking.datetime_booking_end,
                                                rf_fleet_booking.status_booking,
                                                COALESCE ( ( SELECT rf_driver_log.status_driver_activity FROM rf_driver_log WHERE rf_driver_log.id_booking = rf_fleet_booking.id_booking ORDER BY rf_driver_log.timestamp_log DESC LIMIT 1 ), 'scheduled' ) AS status_log 
                                            FROM
                                                rf_fleet_booking
                                                LEFT JOIN user AS user_request ON rf_fleet_booking.id_user = user_request.idnik
                                                LEFT JOIN user AS driver ON rf_fleet_booking.id_driver = driver.idnik 
                                            GROUP BY
                                                rf_fleet_booking.id_booking,
                                                rf_fleet_booking.create_booking_time,
                                                rf_fleet_booking.id_driver,
                                                rf_fleet_booking.id_user,
                                                rf_fleet_booking.datetime_booking_start,
                                                rf_fleet_booking.datetime_booking_end,
                                                rf_fleet_booking.status_booking,
                                                nama_request,
                                                nama_driver 
                                            ORDER BY
                                                rf_fleet_booking.create_booking_time DESC;");
                                                    while ($row6 = mysqli_fetch_assoc($sql7)) {
                                                    ?>
                                                        <tr>
                                                            <td><?= $row6['nama_request'] ?></td>
                                                            <td><?= $row6['create_booking_time'] ?></td>
                                                            <td><?= $row6['datetime_booking_start'] ?></td>
                                                            <td><?= $row6['datetime_booking_end'] ?></td>
                                                            <td><?= $row6['status_booking'] ?></td>
                                                            <td><?= $row6['nama_driver'] ?></td>
                                                            <td><?= $row6['status_log'] ?></td>
                                                        </tr>
                                                <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end card-body-->
                    </div>
                    <!--end card-->
                </div>
                <!--end col-->
                </div>

                <?php
                // Check if the user is an admin or has specific permissions
                if (isset($row7['admin']) && ($row7['admin'] == '1' || ($row7['ga_pic_driver'] == '1'))) {
                ?>
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-header align-items-center d-flex">
                                <h4 class="card-title mb-0 flex-grow-1 py-1">Status Driver Approve</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive table-card">
                                    <table class="table table-borderless table-nowrap table-centered align-middle mb-0">
                                        <thead class="table-light text-muted">
                                            <tr>
                                                <th scope="col">Name Driver</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">ID Booking</th>
                                                <th scope="col">By Request </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql = "SELECT
                            u.idnik AS id_driver,
                            u.nama AS nama_driver,
                        CASE
                                WHEN upcoming_booking.id_booking IS NOT NULL 
                                AND latest_log.id_log IS NULL THEN
                                    'Scheduled' 
                                    WHEN latest_log.status_driver_activity IS NOT NULL 
                                    AND latest_log.status_driver_activity != 'Finished' THEN
                                        latest_log.status_driver_activity ELSE 'Available' 
                                        END AS status,
                                    COALESCE ( upcoming_booking.id_booking, latest_log.id_booking ) AS idbooking,
                                    COALESCE ( upcoming_booking.id_user, ongoing_booking.id_user ) AS id_user,
                                    user_request.nama AS nama_user 
                                FROM
                                    user u
                                    INNER JOIN access_level al ON u.idnik = al.idnik 
                                    AND al.driver = 1
                                    LEFT JOIN ( SELECT id_driver, MIN( datetime_booking_start ) AS earliest_booking_start, id_booking AS upcoming_id_booking FROM rf_fleet_booking WHERE status_booking = 'Approved' GROUP BY id_driver ) AS earliest_booking ON u.idnik = earliest_booking.id_driver
                                    LEFT JOIN rf_fleet_booking upcoming_booking ON u.idnik = upcoming_booking.id_driver 
                                    AND upcoming_booking.id_booking = earliest_booking.upcoming_id_booking
                                    LEFT JOIN ( SELECT id_booking, MAX( id_log ) AS last_log FROM rf_driver_log GROUP BY id_booking ) AS last_log ON upcoming_booking.id_booking = last_log.id_booking
                                    LEFT JOIN rf_driver_log latest_log ON latest_log.id_log = last_log.last_log
                                    LEFT JOIN rf_fleet_booking ongoing_booking ON latest_log.id_booking = ongoing_booking.id_booking
                                    LEFT JOIN user user_request ON ( upcoming_booking.id_user = user_request.idnik OR ongoing_booking.id_user = user_request.idnik ) 
                            ORDER BY
                            u.idnik;;";
                                            $result = mysqli_query($koneksi, $sql);
                                            while ($row_driver = mysqli_fetch_assoc($result)) { ?>
                                                <tr>
                                                    <td>
                                                        <div class="form-check">
                                                            <a href="javascript: void(0);" class="d-inline-block" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Jansh William">
                                                                <img src="../file/profile/laki-laki.jpg" alt="" class="rounded-circle avatar-xxs">
                                                            </a>
                                                            <?= $row_driver['nama_driver']; ?>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge badge-soft-danger"><?= $row_driver['status']; ?></span></td>
                                                    <td class="text-muted"><?= $row_driver['idbooking']; ?></td>
                                                    <td class="text-muted"><?= $row_driver['nama_user']; ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } else { ?>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-header align-items-center d-flex">
                                <h4 class="card-title mb-0 flex-grow-1 py-1">Status Driver Approve</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive table-card">
                                    <table class="table table-borderless table-nowrap table-centered align-middle mb-0">
                                        <thead class="table-light text-muted">
                                            <tr>
                                                <th scope="col">Name Driver</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">ID Booking</th>
                                                <th scope="col">By Request </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $sql = "SELECT
                            u.idnik AS id_driver,
                            u.nama AS nama_driver,
                        CASE
                                
                                WHEN upcoming_booking.id_booking IS NOT NULL 
                                AND latest_log.id_log IS NULL THEN
                                    'Scheduled' 
                                    WHEN latest_log.status_driver_activity IS NOT NULL 
                                    AND latest_log.status_driver_activity != 'Finished' THEN
                                        latest_log.status_driver_activity ELSE 'Available' 
                                        END AS status,
                                    COALESCE ( upcoming_booking.id_booking, latest_log.id_booking ) AS idbooking,
                                    COALESCE ( upcoming_booking.id_user, ongoing_booking.id_user ) AS id_user,
                                    user_request.nama AS nama_user 
                                FROM
                                    user u
                                    INNER JOIN access_level al ON u.idnik = al.idnik 
                                    AND al.driver = 1
                                    LEFT JOIN ( SELECT id_driver, MIN( datetime_booking_start ) AS earliest_booking_start, id_booking AS upcoming_id_booking FROM rf_fleet_booking WHERE status_booking = 'Approved' GROUP BY id_driver ) AS earliest_booking ON u.idnik = earliest_booking.id_driver
                                    LEFT JOIN rf_fleet_booking upcoming_booking ON u.idnik = upcoming_booking.id_driver 
                                    AND upcoming_booking.id_booking = earliest_booking.upcoming_id_booking
                                    LEFT JOIN ( SELECT id_booking, MAX( id_log ) AS last_log FROM rf_driver_log GROUP BY id_booking ) AS last_log ON upcoming_booking.id_booking = last_log.id_booking
                                    LEFT JOIN rf_driver_log latest_log ON latest_log.id_log = last_log.last_log
                                    LEFT JOIN rf_fleet_booking ongoing_booking ON latest_log.id_booking = ongoing_booking.id_booking
                                    LEFT JOIN user user_request ON ( upcoming_booking.id_user = user_request.idnik OR ongoing_booking.id_user = user_request.idnik ) 
                            ORDER BY
                            u.idnik;;";
                                            $result = mysqli_query($koneksi, $sql);
                                            while ($row_driver = mysqli_fetch_assoc($result)) { ?>
                                                <tr>
                                                    <td>
                                                        <div class="form-check">
                                                            <a href="javascript: void(0);" class="d-inline-block" data-bs-toggle="tooltip" data-bs-placement="top" title="" data-bs-original-title="Jansh William">
                                                                <img src="../file/profile/laki-laki.jpg" alt="" class="rounded-circle avatar-xxs">
                                                            </a>
                                                            <?= $row_driver['nama_driver']; ?>
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td><span class="badge badge-soft-danger"><?= $row_driver['status']; ?></span></td>
                                                    <td class="text-muted"><?= $row_driver['idbooking']; ?></td>
                                                    <td class="text-muted"><?= $row_driver['nama_user']; ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>



            <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


            <!--datatable js-->
            <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
            <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
            <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

            <script src="../assets/js/pages/datatables.init.js"></script>