<?php
require_once("koneksi.php");
$sql = "SELECT user.idnik AS id_driver, user.nama 
        FROM user
        INNER JOIN access_level ON user.idnik = access_level.idnik
        WHERE access_level.driver = 1";

$result = mysqli_query($koneksi, $sql);

if ($result) {
    while ($row = mysqli_fetch_assoc($result)) {
        $driverID = $row['id_driver'];
        $driverName = $row['nama'];
        
        echo "<div class='row-xl-4'>
                <div class='card'>
                    <div class='card-header'>
                        <h6 class='card-title mb-0 text-center'>Driver ID : $driverID</h6>
                    </div>
                    <div class='card-body p-4 text-center'>
                        <div class='mx-auto avatar-md mb-3'>
                            <img src='assets/images/users/avatar-8.jpg' alt='' class='img-fluid rounded-circle'>
                        </div>
                        <h5 class='card-title mb-1'>$driverName</h5>
                    </div>
                    <div class='card-footer text-center'>
                    <h6 class='card-title mb-1' style='font-size: 14px;'>Driver Contact (if needed) :</h6>
                        <ul class='list-inline mb-0'>
                            <li class='list-inline-item'>
                                <a href='javascript:void(0);' class='lh-1 align-middle link-success'><i class='ri-whatsapp-line'></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>";
    }
    mysqli_free_result($result);
} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($koneksi);
}
mysqli_close($koneksi);
