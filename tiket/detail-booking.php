<?php
$id_booking = isset($_GET['id']) ? mysqli_real_escape_string($koneksi, $_GET['id']) : '';

$sql = mysqli_query($koneksi, "SELECT 
    user_request.nama AS nama_request,
    user_driver.nama AS nama_driver,
    rf_fleet_booking.id_booking,
    rf_fleet_booking.create_booking_time,
    rf_fleet_booking.id_driver,
    rf_fleet_booking.id_user,
    rf_fleet_booking.datetime_booking_start,
    rf_fleet_booking.datetime_booking_end,
    rf_fleet_booking.pick_up_point,
    rf_fleet_booking.drop_off_point,
    rf_fleet_booking.description,
    rf_fleet_booking.whatsapp_number,
    rf_fleet_booking.status_booking 
FROM
    rf_fleet_booking
LEFT JOIN rf_driver_log ON rf_fleet_booking.id_booking = rf_driver_log.id_booking
LEFT JOIN user AS user_request ON rf_fleet_booking.id_user = user_request.idnik 
LEFT JOIN user AS user_driver ON rf_fleet_booking.id_driver = user_driver.idnik 
WHERE rf_fleet_booking.id_booking ='" . $id_booking . "' ");

$row = mysqli_fetch_assoc($sql);

$id_tiket1 = $id_booking;

?>


<div class="container-fluid">
    <div class="card">
        <div class="card-body">
            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">ID Booking</label>
                </div>
                <div class="col-lg-9">
                    <input readonly class="form-control" placeholder="<?= $row['id_booking'] ?>" name="id_booking">
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">Request Name </label>
                </div>
                <div class="col-lg-9">
                    <input type="text" readonly class="form-control" placeholder="<?= $row['nama_request'] ?>">
                </div>
            </div>

            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">Start Date</label>
                </div>
                <div class="col-lg-9">
                    <input type="text" readonly class="form-control" placeholder="<?= $row['datetime_booking_start'] ?>">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">End Date</label>
                </div>
                <div class="col-lg-9">
                    <input type="text" readonly class="form-control" placeholder="<?= $row['datetime_booking_end'] ?>">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">Pick Up Address</label>
                </div>
                <div class="col-lg-9">
                    <input type="text" readonly class="form-control" placeholder="<?= $row['pick_up_point'] ?>">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-3">
                    <label class="form-label">Drop off Adrress</label>
                </div>
                <div class="col-lg-9">
                    <input type="text" readonly class="form-control" placeholder="<?= $row['drop_off_point'] ?>">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-3">
                    <label for="contactNumber" class="form-label">Whatsapp Number</label>
                </div>
                <div class="col-lg-9">
                    <input type="number" readonly class="form-control" placeholder="<?= $row['whatsapp_number'] ?>">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-3">
                    <label for="meassageInput" class="form-label">Message description</label>
                </div>
                <div class="col-lg-9">
                    <input type="text" readonly class="form-control" placeholder="<?= $row['description'] ?>">
                </div>
            </div>

            <form action="function/update_booking_driver.php" method="POST" enctype="multipart/form-data">
                <!-- Status Booking -->
                <div class="row mb-3">
                    <div class="col-lg-3">
                        <label for="statusBooking" class="form-label">Status Booking </label>
                    </div>
                    <div class="col-lg-9">
                        <select class="form-control" data-choices name="statusBooking">
                            <option value="<?= $row['status_booking'] ?>" selected><?= $row['status_booking'] ?></option>
                            <option value="Approved">Approved</option>
                            <option value="Rejected">Rejected</option>
                            <option value="Done">Done</option>
                        </select>
                    </div>
                </div>


                <!-- Select Driver Option -->
                <div class="row mb-3">
                    <div class="col-lg-3">
                        <label for="id_nik_kurir" class="form-label">Select Driver Option </label>
                    </div>
                    <div class="col-lg-9">
                        <select class="form-select" name="id_driver" data-choices id="choices-status-input">
                            <option value="<?= $row['id_driver'] ?>" selected><?= $row['nama_driver'] ?></option>
                            <?php
                            $sql5 = mysqli_query($koneksi, "SELECT user.idnik AS id_driver, user.nama 
                        FROM user
                        INNER JOIN access_level ON user.idnik = access_level.idnik
                        WHERE access_level.driver = 1");
                            while ($row5 = mysqli_fetch_assoc($sql5)) {
                            ?>
                                <option value="<?= $row5['id_driver'] ?>"><?= $row5['nama'] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <input type="hidden" name="id_booking" value="<?= $row['id_booking'] ?>">
                <div class="text-end mb-3">
                    <button type="submit" class="btn btn-primary" name="updateBookingDriver">Update Booking</button>
                </div>
            </form>
        </div>
    </div>
</div>