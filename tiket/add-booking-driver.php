<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
<style>
    #map {
        height: 400px;
        width: 100%;
    }

    .card {
        background-color: #f8f9fa;
    }
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAb1CBsZR16dK-kgFlEqvM61elLrieLhJk&libraries=places"></script>
<?php
$sql7 = mysqli_query($koneksi, "SELECT * FROM access_level WHERE idnik = $niklogin");
$row7 = mysqli_fetch_assoc($sql7);
?>
<div class="container mt-3">
    <div class="row">
        <div class="card">
            <div class="card-body">
                <h2 class="text-center mb-4 text-uppercase">Form Booking</h2>
                <form action="function/insert_booking_driver.php" method="POST" enctype="multipart/form-data" id="routeForm">
                    <?php
                    $create_booking_time = date('Y-m-d H:i:s');
                    ?>
                    <div class="row g-3">
                        <div class="col-md-6">
                            <!-- Kolom Kiri -->
                            <input type="text" class="form-control" value="<?= $create_booking_time ?>" name="create_booking_time" hidden />
                            <div class="mb-3">
                                <label for="alamatAwal">Alamat Penjemputan Awal:</label>
                                <input id="alamatAwal" type="text" class="form-control" name="pick_up_point" required placeholder="Lokasi saat ini akan muncul di sini">
                            </div>
                            <div class="mb-3">
                                <label for="alamatTujuan">Alamat Tujuan:</label>
                                <input id="alamatTujuan" type="text" class="form-control" name="drop_off_point" required placeholder="Masukkan alamat tujuan">
                            </div>
                            <div class="mb-3">
                                <label for="datetime_booking_start" class="form-label">Tanggal Perjalanan</label>
                                <input type="datetime-local" class="form-control" data-date-format="Y-m-d" data-enable-time placeholder="Select Date Time Start" name="datetime_booking_start" required>
                            </div>
                            <div class="mb-3">
                                <label for="datetime_booking_end" class="form-label">Estimasi Tanggal Selesai Perjalanan</label>
                                <input type="datetime-local" class="form-control" data-date-format="Y-m-d" data-enable-time placeholder="Select Estimated Date Time End" name="datetime_booking_end" required>
                            </div>
                            <div class="mb-3">
                                <label for="wa" class="form-label">No.Whatsapp</label>
                                <input type="number" class="form-control" placeholder="Insert your active number 08xxxxx " name="wa" required />
                            </div>
                            <div class="mb-3">
                                <?php if (isset($row7['ga_pic_driver']) && ($row7['ga_pic_driver'] == '1')) { ?>
                                    <div class="mb-3 mt-3">
                                        <label for="requestUserField" class="form-label"><span>Request User</span></label>
                                        <select class="form-control" data-choices name="nik_request">
                                            <option value="">All Users</option>
                                            <?php
                                            $sql5 = mysqli_query($koneksi, 'SELECT idnik, nama, lokasi FROM user');
                                            while ($row5 = mysqli_fetch_assoc($sql5)) {
                                            ?>
                                                <option value="<?= $row5['idnik'] ?>"><?= $row5['nama'] ?> | <?= $row5['lokasi'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <?php } else { ?>
                                    <input type="text" hidden class="form-control" name="iduser" value="<?= $niklogin ?>" />
                                <?php } ?>
                            </div>
                            <div>
                                <label for="description" style="font-weight: bold;">Description</label>
                                <textarea id="description" name="description" style="width: 100%; height: 100px; resize: vertical; border-radius: 8px; padding: 10px;" required></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id="map" class="mb-3 rounded-3" style="width: 100%; height: 250px;"></div>
                            <div class="form-group mt-3" hidden>
                                <label for="linkAlamatAwalInput">Link Alamat Awal:</label>
                                <input id="linkAlamatAwalInput" type="text" class="form-control" name="pick_up_link">
                            </div>
                            <div class="form-group" hidden>
                                <label for="linkAlamatTujuanInput">Link Alamat Tujuan:</label>
                                <input id="linkAlamatTujuanInput" type="text" class="form-control" name="drop_off_link">
                            </div>
                            <div class="form-group">
                                <label for="estimasiWaktuTanpaLaluLintasInput">Estimasi Waktu Tanpa Lalu Lintas:</label>
                                <input id="estimasiWaktuTanpaLaluLintasInput" type="text" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label for="estimasiWaktuDenganLaluLintasInput">Estimasi Waktu Dengan Lalu Lintas:</label>
                                <input id="estimasiWaktuDenganLaluLintasInput" type="text" class="form-control" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer mt-3 mb-3 p-3">
                        <div class="hstack gap-2 justify-content-end">
                            <a href="index.php?page=DriverList" type="button" class="btn btn-light">Close</a>
                            <button type="submit" class="btn btn-primary waves-effect waves-light" name="add-driver2">Create Booking</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!--end row-->
<script src="assets/libs/@ckeditor/ckeditor5-build-classic/build/ckeditor.js"></script>
<script src="assets/js/pages/project-create.init.js"></script>
<script src="assets/js/pages/ticketdetail.init.js"></script>
<script>
    var map, directionsService, directionsRenderer;
    var autocompleteAwal, autocompleteTujuan;

    function initializeMapAndAutocomplete() {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: -6.2088,
                lng: 106.8456
            },
            zoom: 10
        });
        directionsService = new google.maps.DirectionsService();
        directionsRenderer = new google.maps.DirectionsRenderer();
        directionsRenderer.setMap(map);

        var bounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(-7.355090, 105.064690), // Batas Selatan dan Barat Banten
            new google.maps.LatLng(-5.960630, 107.615120)  // Batas Utara dan Timur Jawa Barat
        );

        var options = {
            types: ['geocode'],
            componentRestrictions: {country: 'ID'},
            bounds: bounds,
            strictBounds: true
        };

        autocompleteAwal = new google.maps.places.Autocomplete(document.getElementById('alamatAwal'), options);
        autocompleteTujuan = new google.maps.places.Autocomplete(document.getElementById('alamatTujuan'), options);

        autocompleteAwal.addListener('place_changed', function() {
            if (document.getElementById('alamatTujuan').value) {
                calculateAndDisplayRoute();
            }
        });

        autocompleteTujuan.addListener('place_changed', function() {
            if (document.getElementById('alamatAwal').value) {
                calculateAndDisplayRoute();
            }
        });
    }

    function calculateAndDisplayRoute() {
        var start = document.getElementById('alamatAwal').value;
        var end = document.getElementById('alamatTujuan').value;

        directionsService.route({
            origin: start,
            destination: end,
            travelMode: 'DRIVING',
            drivingOptions: {
                departureTime: new Date(), // current time
                trafficModel: 'pessimistic'
            }
        }, function(response, status) {
            if (status === 'OK') {
                directionsRenderer.setDirections(response);
                var duration = response.routes[0].legs[0].duration.text; // Tanpa lalu lintas
                var durationInTraffic = response.routes[0].legs[0].duration_in_traffic ? response.routes[0].legs[0].duration_in_traffic.text : "Tidak tersedia"; // Dengan lalu lintas
                document.getElementById('estimasiWaktuTanpaLaluLintasInput').value = duration;
                document.getElementById('estimasiWaktuDenganLaluLintasInput').value = durationInTraffic;
                createGoogleMapsLinks(start, end);
            } else {
                window.alert('Permintaan rute gagal karena ' + status);
            }
        });
    }

    function createGoogleMapsLinks(start, end) {
        var linkAwal = `https://www.google.com/maps/search/?api=1&query=${encodeURIComponent(start)}`;
        var linkTujuan = `https://www.google.com/maps/search/?api=1&query=${encodeURIComponent(end)}`;
        var linkRute = `https://www.google.com/maps/dir/?api=1&origin=${encodeURIComponent(start)}&destination=${encodeURIComponent(end)}&travelmode=driving`;

        document.getElementById('linkAlamatAwalInput').value = linkAwal;
        document.getElementById('linkAlamatTujuanInput').value = linkTujuan;
        document.getElementById('linkRuteInput').value = linkRute;
    }

    google.maps.event.addDomListener(window, 'load', initializeMapAndAutocomplete);
</script>
