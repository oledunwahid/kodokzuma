<?php
session_start();
require_once("../koneksi.php");

function redirectToFacilitiesPage($message, $icon)
{
    $_SESSION["Messages"] = $message;
    $_SESSION["Icon"] = $icon;
    header('Location: ../index.php?page=DriverLog');
    exit();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id_booking = $_POST['id_booking'];
    $status = '';

    if (isset($_POST['actionOTW'])) {
        $status = "On The Way";
    } elseif (isset($_POST['actionArrived'])) {
        $status = "Arrived";
    } elseif (isset($_POST['actionWaiting'])) {
        $status = "Waiting";
    } elseif (isset($_POST['actionFinished'])) {
        $status = "Finished";

        // Transaction Start
        $koneksi->begin_transaction();

        // Insert into rf_driver_log
        $sqlInsertLog = "INSERT INTO rf_driver_log (id_booking, status_driver_activity, timestamp_log) VALUES (?, ?, NOW())";
        $stmtInsert = $koneksi->prepare($sqlInsertLog);
        if (!$stmtInsert) {
            // Gagal melakukan prepare statement, cetak error
            echo "Prepare failed for insert log: (" . $koneksi->errno . ") " . $koneksi->error;
            exit;
        }

        $stmtInsert->bind_param("ss", $id_booking, $status);

        try {
            $stmtInsert->execute();

            // Update rf_fleet_booking status
            $sqlUpdateBooking = "UPDATE rf_fleet_booking SET status_booking = 'Done' WHERE id_booking = ?";
            $stmtUpdate = $koneksi->prepare($sqlUpdateBooking);

            if (!$stmtUpdate) {
                // Gagal melakukan prepare statement, cetak error
                echo "Prepare failed for update booking: (" . $koneksi->errno . ") " . $koneksi->error;
                exit;
            }

            $stmtUpdate->bind_param("s", $id_booking);
            $stmtUpdate->execute();

            // Commit Transaction
            $koneksi->commit();

            redirectToFacilitiesPage('Status updated successfully', 'success');
        } catch (Exception $e) {
            // An error occured, rollback transaction
            $koneksi->rollback();
            echo "Error: " . $e->getMessage();
        }
    } else {
        // Handle other actions if needed
    }

    // Insert log for other statuses (OTW, Arrived, Waiting)
    if (!empty($status) && $status !== "Finished") {
        $sqlInsertLog = "INSERT INTO rf_driver_log (id_booking, status_driver_activity, timestamp_log) VALUES (?, ?, NOW())";
        $stmt = $koneksi->prepare($sqlInsertLog);

        if (!$stmt) {
            // Gagal melakukan prepare statement, cetak error
            echo "Prepare failed for insert log: (" . $koneksi->errno . ") " . $koneksi->error;
            exit;
        }

        $stmt->bind_param("ss", $id_booking, $status);

        if ($stmt->execute()) {
            redirectToFacilitiesPage('Status updated successfully', 'success');
        } else {
            echo "Error inserting log: " . $koneksi->error;
        }
    }
}
?>
