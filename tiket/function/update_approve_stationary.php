<?php
require_once("../koneksi.php");

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["add-stationary"])) {
    $id_request_detail = $_POST["id_request_detail"];
    $id_ga_stationary = $_POST["id_ga_stationary"];
    // $wa = $_POST["wa"];
    $totalApprove = $_POST["totalAcc"]; // Default value untuk total approve
    $feedback = $_POST["feedback"];

    // Sekarang Anda dapat menyimpan data ke dalam database
    $sql_update_atk_detail = "UPDATE atk_detail_request 
                              SET total_approve = '$totalApprove', feedback = '$feedback' 
                              WHERE id_request_detail = '$id_request_detail'";

    // Eksekusi query
    if (mysqli_query($koneksi, $sql_update_atk_detail)) {
        $_SESSION["Messages"] = 'Data Berhasil Di Input';
        $_SESSION["Icon"] = 'success';
    } else {
        $_SESSION["Messages"] = 'Gagal Menambahkan Data';
        $_SESSION["Icon"] = 'error';
    }

    header("Location: ../index.php?page=EditATK/Stationary&id=$id_ga_stationary");
    die();
}
