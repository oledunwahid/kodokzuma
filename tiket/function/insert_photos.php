<?php
require_once("../koneksi.php");

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["add-photos"])) {
    // Pastikan data form tersedia
    if (isset($_POST['id_kurir']) && isset($_FILES['buktiFoto'])) {
        $id_kurir = $_POST['id_kurir'];
        $bukti_foto = $_FILES['buktiFoto']['name']; // Ambil nama file

        // Gunakan prepared statement untuk mencegah SQL injection
        $query = "UPDATE kurir SET bukti_foto = ? WHERE id_kurir = ?";
        $stmt = mysqli_prepare($koneksi, $query);

        if ($stmt) {
            mysqli_stmt_bind_param($stmt, "ss", $bukti_foto, $id_kurir);

            // Validasi jenis file
            $allowed_types = array("jpg", "jpeg", "png", "pdf");
            $file_extension = strtolower(pathinfo($_FILES["buktiFoto"]["name"], PATHINFO_EXTENSION));

            if (!in_array($file_extension, $allowed_types)) {
                echo "Error: Jenis file tidak diizinkan.";
                exit;
            }

            // Validasi ukuran file (misalnya maksimum 2MB)
            $max_size = 2 * 1024 * 1024; // 2MB
            if ($_FILES["buktiFoto"]["size"] > $max_size) {
                echo "Error: Ukuran file terlalu besar.";
                exit;
            }

            // Simpan file di server
            $target_dir = "../file/courier/ "; // Lokasi penyimpanan file
            $target_file = $target_dir . basename($_FILES["buktiFoto"]["name"]);

            // Pindahkan file yang di-upload ke direktori tujuan
            if (move_uploaded_file($_FILES["buktiFoto"]["tmp_name"], $target_file)) {
                // File berhasil dipindahkan
            } else {
                // Terjadi kesalahan saat memindahkan file
                echo "Error: Gagal memindahkan file.";
            }

            // Eksekusi statement
            if (mysqli_stmt_execute($stmt)) {
                session_start();
                $_SESSION["Messages"] = 'Data Berhasil Di Update';
                $_SESSION["Icon"] = 'success';
            } else {
                session_start();
                $_SESSION["Messages"] = 'Data Gagal Di Update';
                $_SESSION["Icon"] = 'error';
            }
            mysqli_stmt_close($stmt);
        } else {
            // Pesan kesalahan jika terjadi kesalahan pada prepared statement
            echo "Error: " . mysqli_error($koneksi);
        }
        mysqli_close($koneksi);
    } else {
        echo "Data tidak lengkap.";
    }
    header("Location: ../index.php?page=EditKurir&id=" . $id_kurir);
    exit();
} else {
    echo "Metode tidak diizinkan atau aksi tidak diatur.";
}
