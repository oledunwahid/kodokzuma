<?php
session_start();
require_once("../koneksi.php");

if (isset($_POST["updateBookingDriver"])) {
    // Validasi: Pastikan bahwa $id_booking bukan null atau kosong
    $id_booking = isset($_POST["id_booking"]) ? $_POST["id_booking"] : '';

    if (empty($id_booking)) {
        // Handle kesalahan, mungkin tampilkan pesan kesalahan ke pengguna
        $_SESSION["Messages"] = 'Update Status Failed: ID Booking tidak valid';
        $_SESSION["Icon"] = 'error';
        header("Location: ../index.php?page=404");
        exit();
    }

    $id_driver = $_POST["id_driver"];
    $statusBooking = $_POST["statusBooking"];

    var_dump($id_booking, $id_driver, $statusBooking);

    $stmt = $koneksi->prepare("UPDATE rf_fleet_booking SET id_driver = ?, status_booking = ? WHERE id_booking = ?");
    $stmt->bind_param("sss", $id_driver, $statusBooking, $id_booking);

    if ($stmt->execute()) {
        // Success
        $_SESSION["Messages"] = 'Update Status Successful';
        $_SESSION["Icon"] = 'success';
        header("Location: ../index.php?page=BookingDriverList");
        exit();
    } else {
        // Failure
        $_SESSION["Messages"] = 'Update Status Failed';
        $_SESSION["Icon"] = 'error';
        // Debugging: Tampilkan pesan error MySQL
        echo "Error: " . $stmt->error;
        error_log("Error: " . $stmt->error);
        header("Location: ../index.php?page=404");
        exit();
    }
} else {
    die("Akses dilarang");
}
