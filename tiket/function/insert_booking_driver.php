<?php
session_start();
require_once("../koneksi.php");

function redirectToFacilitiesPage($message, $icon)
{
    $_SESSION["Messages"] = $message;
    $_SESSION["Icon"] = $icon;
    header('Location: ../index.php?page=BookingDriverList');
    exit();
}

function generateTicketNumber()
{
    $currentDateTime = new DateTime('now', new DateTimeZone('Asia/Jakarta'));
    $timestamp = $currentDateTime->format('ymdHis');
    return "DRV" . $timestamp . str_pad(1, '0', STR_PAD_LEFT);
}

if (isset($_POST['add-driver2'])) {
    $id_booking = generateTicketNumber();
    $create_booking_time = $_POST["create_booking_time"];
    $datetime_booking_start = $_POST["datetime_booking_start"];
    $datetime_booking_end = $_POST['datetime_booking_end'];
    $pick_up_point = $_POST['pick_up_point'];
    $drop_off_point = $_POST['drop_off_point'];
    $pick_up_link = $_POST['pick_up_link'];
    $drop_off_link = $_POST['drop_off_link'];
    $iduser = $_POST["iduser"];
    $description = $_POST["description"];
    $whatsapp_number = $_POST["wa"];

    $sqlExistingBooking = "SELECT COUNT(*) as existing_count FROM rf_fleet_booking WHERE 
        id_user = '$iduser' AND
        (
            (datetime_booking_start < '$datetime_booking_start' AND datetime_booking_end > '$datetime_booking_start') OR
            (datetime_booking_start >= '$datetime_booking_start' AND datetime_booking_start < '$datetime_booking_end')
        )";

    $resultExistingBooking = $koneksi->query($sqlExistingBooking);

    if ($resultExistingBooking->num_rows > 0) {
        $rowExisting = $resultExistingBooking->fetch_assoc();
        $existingCount = $rowExisting['existing_count'];

        if ($existingCount > 0) {
            redirectToFacilitiesPage('Someone has already booked for the same date and time', 'error');
        }
    } else {
        echo "Error: " . $sqlExistingBooking . "<br>" . $koneksi->error;
    }

    $sqlCount = "SELECT COUNT(*) as count FROM rf_fleet_booking WHERE 
        datetime_booking_start >= '$datetime_booking_start' AND datetime_booking_end <= '$datetime_booking_end'";

    $resultCount = $koneksi->query($sqlCount);

    if ($resultCount->num_rows > 0) {
        $row = $resultCount->fetch_assoc();
        $countSchedules = $row['count'];

        if ($countSchedules < 3) {
            $sqlDriverCount = "SELECT COUNT(*) as driver_count FROM rf_fleet_booking WHERE
                (datetime_booking_start >= '$datetime_booking_start' AND datetime_booking_start < '$datetime_booking_end') OR
                (datetime_booking_end > '$datetime_booking_start' AND datetime_booking_end <= '$datetime_booking_end') OR
                (datetime_booking_start <= '$datetime_booking_start' AND datetime_booking_end >= '$datetime_booking_end') AND
                status_booking = 'Assigned'";

            $resultDriverCount = $koneksi->query($sqlDriverCount);

            if ($resultDriverCount->num_rows > 0) {
                $driverRow = $resultDriverCount->fetch_assoc();
                $availableDrivers = 3 - $driverRow['driver_count'];

                if ($availableDrivers > 0) {
                    $sqlBook = "INSERT INTO rf_fleet_booking (id_booking, create_booking_time, id_user, id_driver, datetime_booking_start, datetime_booking_end, pick_up_point , drop_off_point, pick_up_link, drop_off_link ,description, whatsapp_number, status_booking)
                    VALUES ('$id_booking', '$create_booking_time', '$iduser', 'Not Assigned Yet', '$datetime_booking_start', '$datetime_booking_end','$pick_up_point','$drop_off_point','$pick_up_link','$drop_off_link','$description', '$whatsapp_number', 'Created')";

                    if ($koneksi->query($sqlBook) !== TRUE) {
                        echo "Error: " . $sqlBook . "<br>" . $koneksi->error;
                    } else {
                        // Redirect setelah berhasil insert
                        redirectToFacilitiesPage('Booking successful', 'success');
                    }
                } else {
                    redirectToFacilitiesPage('No Available Drivers', 'error');
                }
            } else {
                echo "Error: " . $sqlDriverCount . "<br>" . $koneksi->error;
            }
        } else {
            redirectToFacilitiesPage('Full Booking', 'error');
        }
    } else {
        echo "Error: " . $sqlCount . "<br>" . $koneksi->error;
    }
    $koneksi->close();
}
