<?php
$message = "Selamat! Anda telah memenangkan lelang. Status penawaran Anda telah diubah menjadi menunggu konfirmasi. \n\n Mohon segera mengonfirmasi pada link berikut: http://localhost/eip.maagroup.co.id/index.php?page=ListBid\n\n Terima kasih.";

// Mengatur pengiriman pesan melalui API WhatsApp
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://api.fonnte.com/send',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => array(
        'target' => '081574434332',
        'message' => "tes",
        'countryCode' => '62', // Kode negara WhatsApp pengguna
    ),
    CURLOPT_HTTPHEADER => array(
        'Authorization: vXmxpJo3+5kVsDAWt!y+' // Ganti dengan token Anda
    ),
));

// Melakukan request pengiriman pesan WhatsApp
$response = curl_exec($curl);

// Menutup koneksi cURL
curl_close($curl);
