<?php 	$query = mysqli_query($koneksi, "SELECT max(idlearning) as kodeTerbesar FROM learning");
	$data = mysqli_fetch_array($query);
	$kodeid = $data['kodeTerbesar'];
	$urutan = (int) substr($kodeid, 3, 4);
 
	// bilangan yang diambil ini ditambah 1 untuk menentukan nomor urut berikutnya
	$urutan++;
 
	
	$huruf = "LEA";
	$kodeid= $huruf . sprintf("%04s", $urutan);
	?>


<section class="py-5 bg-danger position-relative mb-4">
            <div class="bg-overlay bg-overlay-pattern opacity-50"></div>
            <div class="container">
                <div class="row align-items-center gy-4">
                    <div class="col-sm">
                        <div>
                            <h4 class="text-white mb-0 fw-semibold">E-Learning Program </h4>
                        </div>
                    </div>
					
                    <!-- end col -->
                    
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </section>


                            
                        <div class="d-flex align-items-center mb-4">
							<?php if ($_SESSION['role'] == 'admin' && $menulogin == '4' ||$menulogin == '9') { ?>
                                    <div class="flex-grow-1">
                                         <button class="btn bg-gradient btn-info" data-bs-toggle="modal" data-bs-target="#add-learning">Add Learning</button>
                                    </div>
						<?php }?>
							 <div class="col-sm">
                            <div class="d-flex justify-content-sm-end gap-2">
                                <div class="search-box ms-2">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <i class="ri-search-line search-icon"></i>
                                </div>

                                
                            </div>
                        </div>
                                    
						</div>
											<?php if(!empty($_SESSION["notif"])){
										   echo $_SESSION["notif"];
										   unset($_SESSION["notif"]);
									   		}	?>
 
<div class="row">
<?php 
	$batas = 9;
	$halaman = isset($_GET['halaman'])?(int)$_GET['halaman'] : 1;
	$halaman_awal = ($halaman>1) ? ($halaman * $batas) - $batas : 0;	
	$previous = $halaman - 1;
	$next = $halaman + 1;
	$data = mysqli_query($koneksi,"select * from learning");
	$jumlah_data = mysqli_num_rows($data);
	$total_halaman = ceil($jumlah_data / $batas);
	
	
	
	$sql = mysqli_query($koneksi, "SELECT * FROM learning  ORDER BY idlearning ASC limit $halaman_awal, $batas ");   
	$nomor = $halaman_awal+1;
	while ($row = mysqli_fetch_assoc($sql)) { ?>

<div class="col-lg-3 col-md-12">        
	<div class="card">      
		
		<div class="card-body">  
			
			 <div class="d-flex align-items-center mb-3">
			<div class="flex-shrink-0 me-3">
				<div class="avatar-sm  rounded p-1"><img src="file/refferal/logo-refferal.jpg" alt="" class="img-fluid d-block">
				</div>
				
			</div>
				 
			<div class="flex-grow-1">
				<h5 class="fs-14 mb-1">MAA Group</h5>
				
			</div>
				<?php if ($_SESSION['role'] == 'admin') { ?>
				 <div class="col text-end dropdown">
                                                        <a href="javascript:void(0);" id="dropdownMenuLink14" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <i class="ri-more-fill fs-17"></i>
                                                        </a>
                                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink14">
                                                            <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#update-learning<?=$row['idlearning']?>"><i class="ri-edit-2-line align-bottom me-2 text-muted"></i>Edit</a></li>
                                                            
                                                            <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#delete<?=$row['idlearning']?>"><i class="ri-delete-bin-5-line me-2 align-middle"></i>Delete</a></li>
                                                        </ul>
                                                    </div><?php }?>
		</div>
			
			<a href="#!"><h5><?=$row['judul_learning']?></h5></a>    
			 <div class="dash-collection overflow-hidden rounded-top position-relative">
			<img src="file/learning/<?=$row['image_learning']?>" alt="" height="220" class="object-cover w-100" />
				  <div class="content position-absolute bottom-0 m-2 p-2 start-0 end-0 rounded d-flex align-items-center">
                                                        <div class="flex-grow-1">
                                                            <a href="#!">
                                                                <h5 class="text fw-bold fs-16 mb-1"><?=$row['kategori_learning']?></h5>
                                                            </a>
                                                          
                                                        </div>
                                                        
                                                    </div>
			</div>                
			<div class="d-flex gap-4 mb-3">                    
				<div><i class="ri-map-pin-2-line text-primary me-1 align-bottom"></i> <?=$row['status_learning']?></div>                    
				<div><i class="ri-time-line text-primary me-1 align-bottom"></i> <?=$row['tgl_learning'] ?></div>   
				<div><i class="ri-book-mark-line" ></i><?php $sql1 = mysqli_query($koneksi, "SELECT * FROM materi WHERE idlearning = '".$row['idlearning']."' "); 
										$data1    = mysqli_num_rows($sql1); 
										 echo "$data1" ?></div> 
			</div>                
			                
			     
			<div class="mt-4 hstack gap-2">                    
				
				<?php $sql3 = mysqli_query($koneksi, "SELECT * FROM add_learning where idlearning = '".$row['idlearning']."'  ");
					  $cek = mysqli_num_rows($sql3);	
					if($cek > 0){?>
						<a href="index.php?page=LearningModule&id=<?=$row['idlearning']?>" class="btn btn-soft-success w-100">View</a>
				<?php	}else { ?> <button  class="btn btn-soft-primary w-100" data-bs-toggle="modal" data-bs-target="#add-materi<?=$row['idlearning']?>" >Start</button>
						
					<?php }?> 
				
				
				
				            
			</div>            
		</div>
		
	</div>    
</div>
	 <?php } ?>

  </div>



					<?php
						$jumlah_number =1;
						$start_number = ($halaman > $jumlah_number)? $halaman - $jumlah_number : 1;
      					$end_number = ($halaman < ($total_halaman - $jumlah_number))? $halaman + $jumlah_number : $total_halaman;	?>
				<div class="row g-0 text-center text-sm-start align-items-center mb-3">
					<div class="col-sm-6">
                            <div>
                                <p class="mb-sm-0">Showing</p>
                            </div>
                        </div> <!-- end col -->
                        <div class="col-sm-6">	
			<ul class="pagination pagination-separated justify-content-center justify-content-sm-end mb-sm-0">
				<li class="page-item">
					<a class="page-link" <?php if($halaman > 1){ 
						echo "href='?page=Learning&halaman=$previous'"; } ?>>Previous</a>
				</li>
				<?php 
				for($x = $start_number; $x <= $end_number;$x++){ 
					$link_active = ($halaman == $x)? ' active' : '';
					?> 
					<li class="page-item<?=$link_active?>"><a class="page-link" href=""><?php echo $x; ?></a></li>
					<?php
				}
				?>				
				<li class="page-item">
					<a  class="page-link" <?php if($halaman < $total_halaman) { echo "href='?page=Learning&halaman=$next'"; } ?>>Next</a>
				</li>
			</ul>
		</div>
</div>



<div class="modal fade zoomIn" id="add-learning" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-l">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Add E-Learning</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/insert_learning.php" method="POST" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-6">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">No. E-Learning</label>
                                                    <input type="text"  name="idnik" value="<?=$niklogin?>" hidden>
													<input type="text" class="form-control" name="idlearning" value="<?=$kodeid?>" readonly required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Date E-Learning</label>
                                                    <input type="date"  class="form-control" value="<?php echo "$tgl" ?>" name="tgl_learning" readonly required />
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Title E-Learning</label>
                                                    <input type="text" class="form-control" placeholder="Title E-Learning" name="judul_learning" required />
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Category E-Learning</label>
                                                    <select class="form-control" name="kategori_learning" data-choices >
                                                            <option value="">Pilih Kategori</option>
                                                            <option value="Mandatory">Mandatory</option>
                                                            <option value="Soft Skill">Soft Skill</option>
                                                            <option value="Hard Skill">Hard Skill</option>
                                                        </select>
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Status E-Learning</label>
                                                    <select class="form-control" data-plugin="choices" name="status_learning" >
														<option value="Aktif">Aktif</option>
													 <option value="Non Aktif">Non Aktif</option>
													
                                                </select>
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Image Thumbnail</label>
                                                    <input type="file" class="form-control"  name="image_learning"  accept="image/png, image/gif, image/jpeg" required />
                                                </div>
                                            </div>
                                            
                                           
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add Learning</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
 

 

<?php $sql = mysqli_query($koneksi, "SELECT * FROM learning ");

    while ($row = mysqli_fetch_assoc($sql)) { ?>

<div class="modal fade zoomIn" id="delete<?=$row['idlearning']?>" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="btn-close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="mt-2 text-center">
                                        <lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>
                                        <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                                            <h4>Are you Sure ?</h4>
                                            <p class="text-muted mx-4 mb-0">Are you Sure You want to Remove this Record <br> <b><?=$row['judul_learning']; ?></b> ?</p>
                                        </div>
                                    </div>
                                    <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                                        <button type="button" class="btn w-sm btn-light" data-bs-dismiss="modal">Close</button>
										<a style="cursor: pointer;" onclick="location.href='function/delete_learning.php?aksi=delete&id=<?=$row['idlearning']; ?>'" class="btn w-sm btn-danger">Yes, Delete It!</a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<div class="modal fade zoomIn" id="update-learning<?=$row['idlearning']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-l">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Update E-Learning</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/update_learning.php" method="POST" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-6">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">No. E-Learning</label>
                                                    <input type="text"  name="idnik" value="<?=$niklogin?>" hidden>
													<input type="text" class="form-control" name="idlearning" value="<?=$row['idlearning']?>" readonly required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Date E-Learning</label>
                                                    <input type="date"  class="form-control" value="<?=$row['tgl_learning']?>" name="tgl_learning"  required />
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Title E-Learning</label>
                                                    <input type="text" class="form-control"  value="<?=$row['judul_learning']?>" name="judul_learning" required />
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Category E-Learning</label>                                                    
													<select class="form-control" data-choices name="kategori_learning" id="choices-single-default">
                                                            <option value="<?=$row['kategori_learning']?>"><?=$row['kategori_learning']?></option>
                                                            <option value="Mandatory">Mandatory</option>
                                                            <option value="Soft Skill">Soft Skill</option>
                                                            <option value="Hard Skill">Hard Skill</option>
                                                        </select>
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Status E-Learning</label>
                                                    <select class="form-control" data-plugin="choices" name="status_learning" >
														<option value="<?=$row['status_learning']?>"><?=$row['status_learning']?></option>
														<option value="Aktif">Aktif</option>
													 <option value="Non Aktif">Non Aktif</option>
													
                                                </select>
                                                </div>
                                            </div>
											<div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Image Thumbnail</label>
													<img class="img-thumbnail"  width="200" src="file/learning/<?=$row['image_learning'] ?>">
													<div class="form-check form-switch">
													<label class="form-check-label" for="SwitchCheck1">Update Image OR Video </label>
													<input class="form-check-input" type="checkbox" role="switch" name="ubah-thumbnail" >
													</div>
                                                    <input type="file" class="form-control"  name="image_learning"  accept="image/png, image/gif, image/jpeg"  />
                                                </div>
                                            </div>
                                            
                                           
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Update Learning</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
<div class="modal fade zoomIn" id="add-materi<?=$row['idlearning']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-l">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Materi</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/insert_add_materi.php" method="POST">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-6">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">No. Materi </label>
                                                    <input type="text"  name="idnik" value="<?=$niklogin?>" hidden >
													 <input type="text" class="form-control" name="idlearning" value="<?=$row['idlearning']?>" readonly required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Tanggal Add</label>
                                                    <input type="date"  class="form-control" value="<?php echo "$tgl" ?>" name="tgl_add" readonly required />
                                                </div>
                                            </div>
											
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Start</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
 
<?php } ?>