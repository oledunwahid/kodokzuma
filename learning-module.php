<?php
$idlearning  = $_GET['id'];	
$sql = mysqli_query($koneksi, "SELECT * FROM learning
										 WHERE idlearning='" . $_GET['id'] . "' "); 
$row = mysqli_fetch_assoc($sql)

?>
   <div class="d-flex align-items-center mb-4">
							<?php if ($_SESSION['role'] == 'admin' && $menulogin == '4' ||$menulogin == '9') { ?>
                                    <div class="flex-grow-1">
                                         <button class="btn bg-gradient btn-info" data-bs-toggle="modal" data-bs-target="#add-learning">Add Modul</button>
										<button class="btn bg-gradient btn-info" data-bs-toggle="modal" data-bs-target="#add-learning-gdrive">Add Modul GDrive</button>
                                    </div>
						<?php }?>
							 <div class="col-sm">
                            <div class="d-flex justify-content-sm-end gap-2">
                                <div class="search-box ms-2">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <i class="ri-search-line search-icon"></i>
                                </div>

                                
                            </div>
                        </div>
                                    
						</div>

											<div class="row">
												
<?php $sql = mysqli_query($koneksi, "SELECT * FROM materi WHERE idlearning='" . $_GET['id'] . "' ");
					$nomor=1;
						while ($row1 = mysqli_fetch_assoc($sql)){
						 ?>
                                                <div class="col-lg-6">
                                                    <div class="card border">
                                                        <div class="card-body">
															<div class="text-end dropdown">
                                                        <a href="javascript:void(0);" id="dropdownMenuLink14" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <i class="ri-more-fill fs-17"></i>
                                                        </a>
                                                        <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdownMenuLink14">
                                                            <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#update-module<?=$row1['idmateri']?>"><i class="ri-edit-2-line align-bottom me-2 text-muted"></i>Edit</a></li>
                                                            
                                                            <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#delete<?=$row1['idmateri']?>"><i class="ri-delete-bin-5-line me-2 align-middle"></i>Delete</a></li>
                                                        </ul>
                                                    </div>
                                                            <div class="d-sm-flex">
                                                                <div class="flex-shrink-0">
                                                                    <img src="file/learning/<?=$row['image_learning']?>" alt="" width="115" class="rounded-1" />
                                                                </div>
                                                                <div class="flex-grow-1 ms-sm-4 mt-3 mt-sm-0">
                                                                    <ul class="list-inline mb-2">
                                                                        <li class="list-inline-item"><span class="badge badge-soft-success fs-11"><?=$row['judul_learning']?></span></li>
                                                                    </ul>
                                                                    <h5><a href="index.php?page=ViewModule&id=<?=$row1['idmateri'] ?>"><?=$row1['nama_materi']?></a></h5>
                                                                   
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end card-->
                                                </div>
                                                <!--end col-->

                                          <?php }?>
                                                <!--end col-->
                                            </div>
<div class="modal fade zoomIn" id="add-learning" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-l">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Module E-Learning</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/insert_module.php" method="POST" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-6">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">No. E-Learning</label>
                                                    <input type="text"  name="idnik" value="<?=$niklogin?>" hidden>
													<input type="text" class="form-control" name="idlearning" value="<?=$row['idlearning']?>" readonly required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Title Module</label>
                                                    <input type="text"  class="form-control" name="nama_materi"  required />
                                                </div>
                                            </div>
											
											
											<div class="col-lg-12">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Media Materi</label>
                                                    <input type="file" class="form-control"  name="media_materi"  accept="image/png, image/gif, image/jpeg, video/mp4"  />
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div>
                                                     <label class="form-label">News Description</label>
                                        <textarea id="ckeditor-classic" name="isi_materi">
                                            
                                        </textarea>
                                                </div>
                                            </div>
                                           
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add Module</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
<div class="modal fade zoomIn" id="add-learning-gdrive" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-l">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Module E-Learning</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/insert_module.php" method="POST" >
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-6">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">No. E-Learning</label>
                                                    <input type="text"  name="idnik" value="<?=$niklogin?>" hidden>
													<input type="text" class="form-control" name="idlearning" value="<?=$row['idlearning']?>" readonly required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Title Module</label>
                                                    <input type="text"  class="form-control" name="nama_materi"  required />
                                                </div>
                                            </div>
											
											
											<div class="col-lg-12">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Media Materi</label>
                                                    <input type="text" class="form-control"  name="media_materi"  />
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div>
                                                     <label class="form-label">News Description</label>
                                        <textarea id="ckeditor-classic1" name="isi_materi">
                                            
                                        </textarea>
                                                </div>
                                            </div>
                                           
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan-gdrive" id="add-btn">Add Module</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

<?php $sql = mysqli_query($koneksi, "SELECT * FROM materi ");

    while ($row = mysqli_fetch_assoc($sql)) { ?>

<div class="modal fade zoomIn" id="delete<?=$row['idmateri']?>" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="btn-close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="mt-2 text-center">
                                        <lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>
                                        <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                                            <h4>Are you Sure ?</h4>
                                            <p class="text-muted mx-4 mb-0">Are you Sure You want to Remove this Record <br> <b><?=$row['nama_materi']; ?></b> ?</p>
                                        </div>
                                    </div>
                                    <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                                        <button type="button" class="btn w-sm btn-light" data-bs-dismiss="modal">Close</button>
										<a style="cursor: pointer;" onclick="location.href='function/delete_module.php?aksi=delete&id=<?=$row['idmateri']; ?>&uid=<?=$idlearning?>'" class="btn w-sm btn-danger">Yes, Delete It!</a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<div class="modal fade zoomIn" id="update-module<?=$row['idmateri']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-l">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Update Module E-Learning</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/insert_module.php" method="POST" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-6">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">No. E-Learning</label>
                                                    <input type="text"  name="idnik" value="<?=$niklogin?>" hidden>
													<input type="text" class="form-control" name="idlearning" value="<?=$row['idlearning']?>" readonly required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Title Module</label>
                                                    <input type="text"  class="form-control" name="nama_materi"  required />
                                                </div>
                                            </div>
											
											
											<div class="col-lg-12">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Media Materi</label>
                                                    <input type="file" class="form-control"  name="media_materi"  accept="image/png, image/gif, image/jpeg, video/mp4"  />
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div>
                                                     <label class="form-label">News Description</label>
                                        <textarea id="ckeditor-classic2" name="isi_materi">
                                            
                                        </textarea>
                                                </div>
                                            </div>
                                           
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Update Module</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
 


<?php } ?>
<script src="assets/libs/@ckeditor/ckeditor5-build-classic/build/ckeditor.js"></script>
  <script src="assets/js/pages/project-create.init.js"></script>
