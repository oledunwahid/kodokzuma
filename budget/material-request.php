 <!--datatable css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <!--datatable responsive css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
									<div class="d-flex align-items-center">
                                    <h5 class="card-title mb-0 flex-grow-1">Request Form Material </h5>
										
										<div class="flex-shrink-0">
											
											
											<form action="function/insert_mr.php" method="POST">
												<input type="text"  value="<?=$niklogin?>" name="id_request" hidden />
												<button class="btn btn-danger add-btn" name="add-mr" type="submit"><i class="ri-add-line align-bottom me-1"></i> Create MR</button>
											</form>
											</div>
									
                                </div>
								</div>
								
                                <div class="card-body">
                                     <table id="buttons-datatables" class="display table table-bordered dt-responsive" style="width:100%">
    <thead>
        <tr>
            <th>No.</th>
            <th>ID MR</th>
            <th>Tanggal MR</th>
            <th>ID Request</th>
            <th>Status MR</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $sql = mysqli_query($koneksi, "SELECT id_mr, tgl_mr, id_request, status_mr FROM budget_mr");                                  
        $nomor = 1;
        while ($row = mysqli_fetch_assoc($sql)) { 
        ?>
            <tr>
                <td><?= $nomor++ ?></td>
                <td><a href="index.php?page=ViewMR&id=<?= $row['id_mr']; ?>"><?= $row['id_mr'] ?></a></td>
                <td><?= $row['tgl_mr'] ?></td>
                <td><?= $row['id_request'] ?></td>
                <td><?= $row['status_mr'] ?></td>
                <td>
                    <div class="dropdown d-inline-block">
                        <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="ri-more-fill align-middle"></i>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-end">
                            <li><a href="index.php?page=ViewMR&id=<?= $row['id_mr']; ?>" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
                            <?php if ($_SESSION['role'] == 'admin') { ?>
                            <li><a class="dropdown-item" href="index.php?page=DetailMR&id=<?= $row['id_mr']; ?>"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                            <li><a class="dropdown-item" href="index.php?page=DeleteMR&id=<?= $row['id_mr']; ?>" onclick="return confirm('Are you sure you want to delete this item?');"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Delete</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </td>
            </tr>
        <?php }?>
    </tbody>
</table>

									
									
                                </div>
                            </div>
                        </div><!--end col-->
                    </div>





<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


<!--datatable js-->
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="../assets/js/pages/datatables.init.js"></script>


   

