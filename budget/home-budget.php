<?php if (!empty($_SESSION["notif"])) {
    echo $_SESSION["notif"];
    unset($_SESSION["notif"]);
}
?>
<div class="h-100">
    <div class="row mb-3 pb-1">
        <div class="col-12">
            <div class="d-flex align-items-lg-center flex-lg-row flex-column">
                <div class="flex-grow-1">
                    <h4 class="fs-16 mb-1">Welcome, <?= $namalogin ?> </h4>
                    <p class="text-muted mb-0">Silahkan Klik Bagian Menu Kiri, Untuk mempelajari seluruh informasi mengenai Perusahaan. </p>
                </div>

            </div><!-- end card header -->
        </div>
        <!--end col-->
    </div>
</div>
<!--end row-->

<div class="row">
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-animate">
    <div class="card-body">
        <div class="d-flex align-items-center">
            <div class="flex-grow-1 overflow-hidden">
                <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total MR Budget for 2024</p>
            </div>
        </div>
        <div class="d-flex align-items-end justify-content-between mt-4">
            <div>
                <h4 class="fs-22 fw-semibold ff-secondary mb-4"> 
                    <?php
                    $sql = mysqli_query($koneksi, "SELECT SUM((ppn*total_price_mr)/100 + total_price_mr) AS total_budget FROM budget_mr WHERE YEAR(tgl_mr) = 2024");
                    $row = mysqli_fetch_assoc($sql);
                    $total_budget = $row['total_budget'];
                    echo rupiah($total_budget); // Menampilkan total budget dengan format angka
                    ?> 
                </h4>
            </div>
            <div class="avatar-sm flex-shrink-0">
                <span class="avatar-title bg-soft-success rounded fs-3">
                    <i class="bx bx-wallet text-success"></i>
                </span>
            </div>
        </div>
    </div><!-- end card body -->
</div><!-- end card -->

    </div><!-- end col -->
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-animate">
    <div class="card-body">
        <div class="d-flex align-items-center">
            <div class="flex-grow-1 overflow-hidden">
                <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total MR Budget  HO</p>
            </div>
        </div>
        <div class="d-flex align-items-end justify-content-between mt-4">
            <div>
                <h4 class="fs-22 fw-semibold ff-secondary mb-4"> 
                    <?php
                    $sql = mysqli_query($koneksi, "SELECT SUM((ppn*total_price_mr)/100 + total_price_mr) AS total_budget FROM budget_mr WHERE YEAR(tgl_mr) = 2024 AND lokasi_mr ='HO'");
                    $row = mysqli_fetch_assoc($sql);
                    $total_budget = $row['total_budget'];
                    echo rupiah($total_budget); // Menampilkan total budget dengan format angka
                    ?> 
                </h4>
            </div>
            <div class="avatar-sm flex-shrink-0">
                <span class="avatar-title bg-soft-success rounded fs-3">
                    <i class="bx bx-wallet text-success"></i>
                </span>
            </div>
        </div>
    </div><!-- end card body -->
</div><!-- end card -->
<!-- end card -->
    </div><!-- end col -->
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-animate">
    <div class="card-body">
        <div class="d-flex align-items-center">
            <div class="flex-grow-1 overflow-hidden">
                <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total MR Budget OBI</p>
            </div>
        </div>
        <div class="d-flex align-items-end justify-content-between mt-4">
            <div>
                <h4 class="fs-22 fw-semibold ff-secondary mb-4"> 
                    <?php
                    $sql = mysqli_query($koneksi, "SELECT SUM((ppn*total_price_mr)/100 + total_price_mr) AS total_budget FROM budget_mr WHERE YEAR(tgl_mr) = 2024 AND lokasi_mr ='OBI'");
                    $row = mysqli_fetch_assoc($sql);
                    $total_budget = $row['total_budget'];
                    echo rupiah($total_budget); // Menampilkan total budget dengan format angka
                    ?> 
                </h4>
            </div>
            <div class="avatar-sm flex-shrink-0">
                <span class="avatar-title bg-soft-success rounded fs-3">
                    <i class="bx bx-wallet text-success"></i>
                </span>
            </div>
        </div>
    </div><!-- end card body -->
</div><!-- end card -->
    </div><!-- end col -->
    <div class="col-xl-3 col-md-6">
        <!-- card -->
        <div class="card card-animate">
    <div class="card-body">
        <div class="d-flex align-items-center">
            <div class="flex-grow-1 overflow-hidden">
                <p class="text-uppercase fw-medium text-muted text-truncate mb-0">Total MR Budget BCPM</p>
            </div>
        </div>
        <div class="d-flex align-items-end justify-content-between mt-4">
            <div>
                <h4 class="fs-22 fw-semibold ff-secondary mb-4"> 
                    <?php
                    $sql = mysqli_query($koneksi, "SELECT SUM((ppn*total_price_mr)/100 + total_price_mr) AS total_budget FROM budget_mr WHERE YEAR(tgl_mr) = 2024 AND lokasi_mr ='BCPM'");
                    $row = mysqli_fetch_assoc($sql);
                    $total_budget = $row['total_budget'];
                    echo rupiah($total_budget); // Menampilkan total budget dengan format angka
                    ?> 
                </h4>
            </div>
            <div class="avatar-sm flex-shrink-0">
                <span class="avatar-title bg-soft-success rounded fs-3">
                    <i class="bx bx-wallet text-success"></i>
                </span>
            </div>
        </div>
    </div><!-- end card body -->
</div><!-- end card -->
    </div><!-- end col -->

</div>








<script src="assets/libs/swiper/swiper-bundle.min.js"></script>
<script src="assets/js/pages/nft-landing.init.js"></script>