<?php $page =  $_GET['page']; ?>
<div class="app-menu navbar-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box">
        <!-- Dark Logo-->
        <a href="../index.php?page=Dashboard" class="logo logo-dark">
            <span class="logo-sm">
                <img src="../assets/images/logo_MAA.png" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="../assets/images/logo_MAAA.png" alt="" height="35">
            </span>
        </a>
        <!-- Light Logo-->
        <a href="../index.php?page=Dashboard" class="logo logo-light">
            <span class="logo-sm">
                <img src="../assets/images/logo_MAA.png" alt="" height="22">
            </span>
            <span class="logo-lg">
                <img src="../assets/images/logo_MAAA.png" alt="" height="35">
            </span>
        </a>
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">
            <div id="two-column-menu">
            </div>

            <ul class="navbar-nav" id="navbar-nav">
                <li class="menu-title"><span data-key="t-menu">Menu</span></li>
                <li class="nav-item">
                    <a class="nav-link menu-link<?php if ($page == 'Dashboard') echo ' active'; ?>" href="index.php?page=Dashboard" aria-expanded="false">
                        <i class="ri-dashboard-2-line"></i> <span> Dashboard </span>
                    </a>
                </li>
				
                
				<li class="nav-item">
					<?php $pages = ['Material-Request', 'ApprovalMR', 'LinkApprovalMR','ViewMR','DetailMR']; ?>
                         <a class="nav-link menu-link<?php if (in_array($page, $pages)) echo ' active'; ?>" href="#Sop" data-bs-toggle="collapse" role="button" aria-expanded="false" aria-controls="sidebarSop">
                             <i class="ri-check-double-line"></i> <span>Material Request</span>
                         </a>
                         <div class="collapse <?php if (in_array($page, $pages)) echo 'show'; ?> menu-dropdown" id="Sop">
                             <ul class="nav nav-sm flex-column">
                                 <li class="nav-item">
                                     <a href="index.php?page=Material-Request" class="nav-link <?php if ($page == 'Material-Request') echo 'active'; ?>">Input MR</a>
                                 </li>
                                 <li class="nav-item">
                                     <a href="index.php?page=ApprovalMR" class="nav-link <?php if ($page == 'ApprovalMR') echo 'active'; ?>">List Approval</a>
                                 </li>
                                             </ul>
                         </div>
                     </li>



            </ul>


        </div>
        <!-- Sidebar -->
    </div>

    <div class="sidebar-background"></div>
</div>