<!--datatable css-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
<!--datatable responsive css-->
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

<link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<style>
    .card-enhanced {
        transition: transform .3s, box-shadow .3s;
        cursor: pointer;
    }

    .card-enhanced:hover {
        transform: scale(1.03);
        box-shadow: 0 10px 20px rgba(0, 0, 0, .12), 0 4px 8px rgba(0, 0, 0, .06);
    }

    .table-hover tbody tr:hover {
        background-color: #f8f9fa;
    }


    .btn-enhanced {
        transition: background-color .3s, transform .3s;
    }

    .btn-enhanced:hover {
        transform: translateY(-2px);
    }




    /* Menyesuaikan lebar kolom pada tabel */


    /* Custom button styles */
    .btn-icon .ri {
        margin-right: 4px;
    }

    .action-buttons .btn {
        padding: 5px 10px;
        font-size: 14px;
    }

    /* Responsive card body padding */
    .card-body-enhanced {
        padding: 1.5rem;
    }

    /* Customizing the input fields for a better look */
    .form-control {
        border-radius: 0.375rem;
    }

    .form-control:focus {
        border-color: #86b7fe;
        box-shadow: 0 0 0 0.25rem rgba(13, 110, 253, 0.25);
    }
</style>


<?php
$idmr     = $_GET['id'];
$sql = mysqli_query($koneksi, "SELECT * FROM budget_mr WHERE id_mr='" . $_GET['id'] . "' "); // query jika filter dipilih
$row = mysqli_fetch_assoc($sql) // fetch query yang sesuai ke dalam array
?>

<div class="container mt-0">
    <div class="row justify-content-center">
        <div class="col-lg-12"> <!-- Ubah menjadi col-lg-12 -->
            <div class="card card-custom">
                <div class="card-header">
                    <h5 class="card-title mb-0">Input Detail Material Request</h5>
                </div>
                <div class="card-body">
                    <button type="button" class="btn btn-primary btn-enhanced" id="addRow">
                        <i class="ri-add-line icon-btn-space"></i>Add Row
                    </button>
                    <div class="table-responsive mt-3">
                        <table class="table table-hover" id="detail-mr">
                            <thead>
                                <tr>
                                    <th style="display:none;">ID MR</th>
                                    <th>Nama Barang</th>
                                    <th width="10%">Qty</th>
                                    <th width="10%">Uom</th>
                                    <th width="15%">Harga</th>
                                    <th width="20%">Total Harga</th>
                                    <th width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Data akan di-load menggunakan AJAX -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card card-enhanced">
                <div class="card-body card-body-enhanced">
                    <h5 class="card-title">Material Request - <?= $row['status_mr'] ?></h5>
                    <form id="updateMaterialRequestForm">

                        <div class="card-body border-bottom border-bottom-dashed ">

                            <div class="row g-3">
                                <div class="col-lg-3 col-sm-6">
                                    <label for="invoicenoInput">No MR</label>
                                    <input type="text" name="id_mr" class="form-control bg-light border-0" value="<?= $row['id_mr'] ?>" readonly>
                                </div>
                                <div class="col-lg-3 col-sm-6">

                                    <label for="priority">Pilih Type:</label>
                                    <ul>
                                        <li>
                                            <label>
                                                <input type="radio" name="type_mr" value="Non Project" <?php echo ($row['type_mr'] == 'Non Project') ? 'checked' : ''; ?>>Non Project
                                            </label>
                                        </li>
                                        <li>
                                            <label>

                                                <input type="radio" name="type_mr" value="Project" <?php echo ($row['type_mr'] == 'Project') ? 'checked' : ''; ?>>Project
                                            </label>

                                        </li>
                                    </ul>

                                </div>
                                <!--end col-->
                                <div class="col-lg-3 col-sm-6">
                                    <div>
                                        <label for="date-field">Date Request</label>
                                        <input type="date" name="tgl_mr" data-date-format="Y-m-d" class="form-control bg-light border-0" value="<?= $row['tgl_mr'] ?>">
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div>
                                        <label for="date-field">Date Need</label>
                                        <input type="date" name="tgl_need_mr" data-date-format="Y-m-d" class="form-control" value="<?= $row['tgl_need_mr'] ?>">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-3 col-sm-6">
                                    <label for="choices-payment-status">Nama PT</label>
                                    <div class="input-light">
                                        <select class="form-control" name="pt_mr" data-choices data-choices-search-false id="choices-payment-status" required>

                                            <option value="<?= $row['pt_mr'] ?>"><?= $row['pt_mr'] ?></option><br>
                                            <option value="">Select PT</option>
                                            <option value="PT. Mineral Alam Abadi">PT. Mineral Alam Abadi</option>
                                            <option value="PT. Mitra Mineral Perkasa">PT. Mitra Mineral Perkasa</option><br>
                                            <option value="PT. Bangun Teknik Group">PT. Bangun Teknik Group</option>
                                        </select>
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-lg-3 col-sm-6">
                                    <label for="choices-payment-status">Lokasi</label>
                                    <div class="input-light">
                                        <select class="form-control" name="lokasi_mr" data-choices data-choices-search-false id="choices-payment-status" required>
                                            <option value="<?= $row['lokasi_mr'] ?>"><?= $row['lokasi_mr'] ?></option>
                                            <option value="">Select Lokasi</option>
                                            <option value="HO">HO</option>
                                            <option value="BCPM">Obi</option>
                                            <option value="BCPM">BCPM</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <label for="choices-payment-status">PPN %</label>
                                    <div class="input-light">
                                        <select class="form-control" name="ppn" data-choices data-choices-search-false id="choices-payment-status" required>
                                            <option value="<?= $row['ppn'] ?>"><?= $row['ppn'] ?>%</option>
                                            <option value="0">0</option>
                                            <option value="11">11%</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div>
                                        <label for="totalamountInput">Total Amount</label>

                                        <input type="text" name="total_price_mr" id="total_price_mr" class="form-control bg-light border-0" value="<?= rupiah($row['total_price_mr']) ?>" readonly />
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                    <div>
                                        <label for="totalamountInput">Diskripsi</label>
                                        <textarea name="deskripsi" class="form-control"><?= $row['deskripsi'] ?></textarea>

                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6">
                                    <div>

                                        <label for="priority">Pilih Priority:</label>
                                        <ul>

                                            <li><label><input type="radio" name="priority_mr" value="Normal" onclick="toggleTextArea(this.value)" <?php echo ($row['priority_mr'] == 'Normal') ? 'checked' : ''; ?>> Normal</label></li>
                                            <li><label><input type="radio" name="priority_mr" value="Urgent" onclick="toggleTextArea(this.value)" <?php echo ($row['priority_mr'] == 'Urgent') ? 'checked' : ''; ?>> Urgent</label></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6">
                                    <div>
                                        <div id="urgentTextArea" style="display:none;">
                                            <label for="details">Urgency Details:</label>
                                            <textarea id="details" class="form-control" name="dekripsi_priority_mr"><?= $row['dekripsi_priority_mr'] ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--end col-->

                                <!--end row-->
                            </div>
                        </div>



                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>




<script>
    function toggleTextArea(value) {
        const textAreaDiv = document.getElementById('urgentTextArea');
        if (value === 'Urgent') {
            textAreaDiv.style.display = 'block';
        } else {
            textAreaDiv.style.display = 'none';
        }
    }
</script>

<script>
    $(document).ready(function() {
        var idMR = <?= json_encode($_GET['id']); ?>;

        function formatRibuan(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        }



        function loadData(callback) {
            $.ajax({
                url: 'function/fetch_detail_mr.php',
                type: 'GET',
                data: {
                    id_mr: idMR
                },
                success: function(data) {
                    $('#detail-mr tbody').html(data);
                    if (callback) callback();
                }
            });
        }

        loadData();



        // Memanggil updateTotalPriceMR setiap kali terjadi perubahan pada qty_barang[] atau price[]
        $(document).on('input', "input[name='qty_barang[]'], input[name='price[]']", function() {
            var row = $(this).closest('tr');
            var qty = parseInt(row.find("input[name='qty_barang[]']").val()) || 0;
            var price = parseInt(row.find("input[name='price[]']").val().replace(/\./g, '')) || 0;
            var total = qty * price;
            row.find('.total-price').text(formatRibuan(total));
            updateTotalPriceMR(); // Memanggil fungsi untuk memperbarui total price MR
        });

        function addRow() {
            var newRow = `<tr>
    <td style="display:none;"><input type="text" name="id_mr[]" class="form-control" value="${idMR}" readonly /></td>
    <td><input type="text" name="nama_barang[]" class="form-control" style="width: 100%;" /></td>
    <td><input type="number" name="qty_barang[]" class="form-control" maxlength="5" style="width: 80px;" /></td>
	<td><input type="text" name="uom[]" class="form-control" style="width: 80px;" /></td> <!-- Input uom ditambahkan di sini -->	
    <td><input type="text" name="price[]" class="form-control price-input" maxlength="11" style="width: 120px;" /></td>
    <td class="text-right"><span class="total-price">0</span></td>
    <td>
        <button type="button" class="btn btn-success btn-sm saveNewRow">SN</button>
        <button type="button" class="btn btn-success btn-sm saveRow" style="display: none;">S</button>
        <button type="button" class="btn btn-danger remove" style="display: none;" data-id="">R</button>
    </td>
</tr>`;
            $('#detail-mr tbody').append(newRow);
        }

        $('#addRow').click(function() {
            addRow();
        });

        $(document).on('input', '.price-input', function() {
            var value = $(this).val().replace(/\./g, '');
            $(this).val(formatRibuan(value));
        });

        $(document).on('input', "input[name='qty_barang[]'], input[name='price[]']", function() {
            var row = $(this).closest('tr');
            var qty = parseInt(row.find("input[name='qty_barang[]']").val()) || 0;
            var price = parseInt(row.find("input[name='price[]']").val().replace(/\./g, '')) || 0;
            var total = qty * price;
            row.find('.total-price').text(formatRibuan(total));
        });

        $(document).on('click', '.saveNewRow', function() {
            var row = $(this).closest('tr');
            var data = {
                id_mr: row.find("input[name='id_mr[]']").val(),
                nama_barang: row.find("input[name='nama_barang[]']").val(),
                qty_barang: row.find("input[name='qty_barang[]']").val(),
                uom: row.find("input[name='uom[]']").val(), // Menambahkan uom
                price: row.find("input[name='price[]']").val().replace(/\./g, '')
            };

            $.ajax({
                type: "POST",
                url: "function/insert_detail_mr.php",
                data: data,
                success: function(response) {
                    alert("Data berhasil disimpan");
                    loadData();
                },
                error: function() {
                    alert("Terjadi kesalahan saat menyimpan data");
                }
            });
        });

        $(document).on('click', '.edit', function() {
            var $row = $(this).closest('tr');
            $row.find('input').prop('readonly', false);
            $(this).hide();
            $row.find('.saveRow').show();
        });

        $(document).on('click', '.saveRow', function() {
            var row = $(this).closest('tr');
            var data = {
                id_detail_mr: row.find('.remove').data('id'),
                id_mr: row.find("input[name='id_mr[]']").val(),
                nama_barang: row.find("input[name='nama_barang[]']").val(),
                qty_barang: row.find("input[name='qty_barang[]']").val(),
                uom: row.find("input[name='uom[]']").val(), // Menambahkan uom di sini
                price: row.find("input[name='price[]']").val().replace(/\./g, '')
            };

            $.ajax({
                type: "POST",
                url: "function/update_detail_mr.php",
                data: data,
                success: function(response) {
                    alert("Data berhasil diupdate");
                    loadData();
                },
                error: function() {
                    alert("Terjadi kesalahan saat menyimpan data");
                }
            });
        });

        $(document).on('click', '.remove', function() {
            var idDetailMR = $(this).data('id');
            if (confirm('Apakah Anda yakin ingin menghapus data ini?')) {
                $.ajax({
                    type: "POST",
                    url: "function/delete_detail_mr.php",
                    data: {
                        id_detail_mr: idDetailMR
                    },
                    success: function(response) {
                        alert("Data berhasil dihapus");
                        loadData(function() {
                            updateTotalPriceMR(); // Dipanggil setelah loadData selesai
                        });
                    },
                    error: function() {
                        alert("Terjadi kesalahan saat menghapus data");
                    }
                });
            }
        });


        function updateTotalPriceMR() {
            var total = 0;
            $("#detail-mr tbody tr").each(function() {
                var qty = $(this).find("input[name='qty_barang[]']").val();
                var price = $(this).find("input[name='price[]']").val().replace(/\./g, '');
                var subtotal = (qty * price) || 0;
                total += subtotal;
            });
            $("input[name='total_price_mr']").val(formatRibuan(total));
        }

        $('#reloadTotalPrice').click(function() {
            updateTotalPriceMR();
        });



        $('#updateMaterialRequestForm').on('submit', function(e) {
            e.preventDefault();
            var formData = $(this).serialize(); // Mengambil data dari form

            $.ajax({
                type: "POST",
                url: "function/update_mr.php", // Sesuaikan dengan path file PHP yang menangani update
                data: formData,
                success: function(response) {
                    // Redirect setelah berhasil update
                    window.location.href = "http://localhost/eip/budget/index.php?page=Material-Request";
                },
                error: function() {
                    alert("Terjadi kesalahan saat mengupdate data");
                }
            });
        });



    });
</script>