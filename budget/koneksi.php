<?php
$servername = "localhost";
$username = "root"; // ganti dengan username db anda
$password = ""; // ganti dengan password db anda
$dbname = "emp";

// Membuat koneksi
$conn = new mysqli($servername, $username, $password, $dbname);

// Mengecek koneksi
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
?>
