<?php
session_start();
include 'koneksi.php';

// Hancurkan semua session
session_destroy();

// Arahkan kembali ke halaman login atau home
header('Location: login.php');
exit;
?>
