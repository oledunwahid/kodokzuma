<?php 
// mengaktifkan session pada php
session_start();
$url = $_SESSION['yuhu'];
// menghubungkan php dengan koneksi database
include '../koneksi.php';

// menangkap data yang dikirim dari form login
$username = isset($_POST['username']) ? $_POST['username'] : null;
$password = isset($_POST['password']) ? $_POST['password'] : null;
$token = isset($_POST['token']) ? $_POST['token'] : null;

// Inisialisasi variable untuk mengecek login
$login = null;

// Cek mode login, apakah menggunakan token atau username & password
if (!empty($token)) {
    // Proses login dengan token
    $login = mysqli_query($koneksi, "SELECT * FROM login WHERE token='$token' AND status_login='Aktif'");
} else {
    // Proses login dengan username dan password
    $login = mysqli_query($koneksi, "SELECT * FROM login WHERE username='$username' AND password='$password' AND status_login='Aktif'");
}

// menghitung jumlah data yang ditemukan
$cek = mysqli_num_rows($login);

// cek apakah username dan password atau token ditemukan pada database
if ($cek > 0) {

    $data = mysqli_fetch_assoc($login);

    // Set session username dari database jika login menggunakan token
    if (!empty($token)) {
        $_SESSION['username'] = $data['username'];
    } else {
        $_SESSION['username'] = $username;
    }

    // Set session role
    $_SESSION['role'] = $data['role'];

    // Redirect berdasarkan role
    if ($data['role'] == "admin" || $data['role'] == "user") {
        if ($url == false || $url == null) {
            header("location:index.php?page=Dashboard");
        } else {
            header("location:$url");
        }
    }
    
} else {
    header("location:login.php?pesan=gagal");
}

?>
