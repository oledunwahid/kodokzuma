 <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
 <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
 <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

 <div class="row">
     <div class="col-lg-12">
         <div class="card">
             <div class="card-header">
                 <h5 class="card-title mb-0">Approval Form Material</h5>
             </div>
             <div class="card-body">
                 <table id="buttons-datatables" class="display table table-bordered dt-responsive" style="width:100%">
                     <thead>
							<tr>
								<th>ID MR</th>
								<th>Requester</th>
								<th>PT MR</th>
								<th>Lokasi MR</th>
								<th>Total Price MR</th>
								<th>Status</th>
								<th>Copy Link</th>
								<th>Action</th>
							</tr>
						</thead>
                     <tbody>
                         <?php
                            // Ambil semua approvals untuk MR tertentu dan urutkan berdasarkan id_approval
                            $sqlApprovals = "SELECT id_approval, id_mr, status FROM budget_approval_mr WHERE id_mr IN (SELECT id_mr FROM budget_approval_mr WHERE nik_approval ='$niklogin') ORDER BY id_approval ASC";
                            $resultApprovals = mysqli_query($koneksi, $sqlApprovals);
                            $approvals = [];
                            while ($approval = mysqli_fetch_assoc($resultApprovals)) {
                                $approvals[$approval['id_mr']][] = $approval;
                            }

                            $sql = "SELECT bam.id_approval, bam.id_mr, u1.nama AS approver_name, bam.date_approval, bm.tgl_mr, bm.tgl_need_mr, u2.nama AS requester_name, bm.pt_mr, bm.lokasi_mr, bm.ppn, bm.total_price_mr, bam.status FROM budget_approval_mr bam INNER JOIN user u1 ON bam.nik_approval = u1.idnik INNER JOIN budget_mr bm ON bam.id_mr = bm.id_mr INNER JOIN user u2 ON bm.id_request = u2.idnik WHERE status_mr = 'Procesed' AND nik_approval ='$niklogin'";
                            $result = mysqli_query($koneksi, $sql);
                            while ($row = mysqli_fetch_assoc($result)) {
                                // Cek apakah pengguna saat ini adalah approver berikutnya yang harus menyetujui
                                $currentApprovalIndex = array_search($row['id_approval'], array_column($approvals[$row['id_mr']], 'id_approval'));
                                $showButtons = true;
								$totalppn = ($row['ppn'] * $row['total_price_mr'])/100 ;
									 $totalWithPPN = $row['total_price_mr'] + $totalppn;
								
                                if ($currentApprovalIndex !== false && $currentApprovalIndex > 0) {
                                    // Cek status approval sebelumnya
                                    $previousApprovalStatus = $approvals[$row['id_mr']][$currentApprovalIndex - 1]['status'];
                                    $showButtons = $previousApprovalStatus === 'Approved';
									 
                                }
                            ?>
                             <tr>
                                 <td><a href="index.php?page=LinkApprovalMR&id=<?= $row['id_mr']; ?>"><?= $row['id_mr'] ?></a></td>
                                 <td><?= $row['requester_name'] ?></td>
                                 <td><?= $row['pt_mr'] ?></td>
								 <td><?= $row['lokasi_mr'] ?></td>
                                 <td><?= rupiah($totalWithPPN) ?></td>
                                 <td><?= $row['status'] ?></td>
								 <td>
									 <button class="btn btn-primary" onclick="copyToClipboard('http://localhost/eip/budget/index.php?page=LinkApprovalMR&id=<?= $row['id_mr']; ?>')">Copy Link</button>
								 </td>
                                 <td>
                                     <?php if ($row['status'] == 'Pending' && $showButtons) : ?>
										<button onclick="processMR(<?= $row['id_approval']; ?>, 'Approved')" class="btn btn-success">Approve</button>
										<button onclick="processMR(<?= $row['id_approval']; ?>, 'Rejected')" class="btn btn-danger">Reject</button>
									<?php elseif ($row['status'] == 'Approved' || $row['status'] == 'Rejected') : ?>
										<button onclick="processMR(<?= $row['id_approval']; ?>, 'Cancelled')" class="btn btn-warning">Batalkan</button>
									<?php endif; ?>
                                 </td>
                             </tr>
                         <?php } ?>
                     </tbody>
                 </table>
             </div>
         </div>
     </div><!--end col-->
 </div>

 <!-- Include jQuery and DataTables scripts as before -->
 <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


 <!--datatable js-->
 <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
 <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
 <script src="../assets/js/pages/datatables.init.js"></script>

 <script>
	 
	 function copyToClipboard(text) {
    navigator.clipboard.writeText(text).then(function() {
        alert("Link copied to clipboard");
    }).catch(function(error) {
        alert("Failed to copy: ", error);
    });
}
     function processMR(idApproval, action) {
    // Menambahkan dialog konfirmasi
    var confirmAction = confirm("Apakah Anda yakin akan " + action.toLowerCase() + " dokumen ini?");
    if (confirmAction) {
        console.log("Sending AJAX request for ID Approval:", idApproval, "Action:", action); // Debug log
        $.ajax({
            url: 'function/process_mr.php',
            type: 'POST',
            data: {id_approval: idApproval, action: action},
            success: function(response) {
                // Log response dari server untuk debugging
                console.log("Response from server:", response);
                alert('MR ' + action);
                location.reload(); // Reload halaman untuk melihat perubahan
            },
            error: function(xhr, status, error) {
                // Log error jika AJAX request gagal
                console.error("AJAX request failed:", status, error);
                alert("Failed to process MR. Please try again.");
            }
        });
    } else {
        console.log(action + " cancelled by user."); // User membatalkan aksi
    }
}

 </script>