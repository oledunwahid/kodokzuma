<?php
include("../../koneksi.php");

if (isset($_POST["add-mr"])) {
    $id_request = $_POST['id_request'];

    // Cek apakah sudah ada id_request dengan status 'Created'
    $cek_query = "SELECT * FROM budget_mr WHERE id_request='$id_request' AND status_mr='Created'";
    $cek_result = mysqli_query($koneksi, $cek_query);

    if (mysqli_num_rows($cek_result) > 0) {
        // Jika ditemukan, ambil id_mr dari data yang ada
        $data_exist = mysqli_fetch_assoc($cek_result);
        $existing_id_mr = $data_exist['id_mr'];

        // Redirect ke halaman detail MR dengan id_mr yang sudah ada
        header("location:../index.php?page=DetailMR&id=$existing_id_mr");
        exit;
    } else {
        // Jika tidak ditemukan, lanjutkan dengan proses insert
        $tanggal_req = date('Y-m-d H:i:s');
        $currentDateTime = new DateTime('now', new DateTimeZone('Asia/Jakarta'));
        $timestamp = $currentDateTime->format('ymdHis');
        $RequestNumber = "MR" . $timestamp;

        $id_mr = $RequestNumber;
        $tgl_mr = $tanggal_req;
        $status_mr = 'Created';
        $query = "INSERT INTO budget_mr (id_mr, tgl_mr, id_request, status_mr) VALUES ('$id_mr','$tgl_mr','$id_request','$status_mr')";
        $result = mysqli_query($koneksi, $query);

        if ($result) {
            // Cek atasan dari user yang membuat request
            $cek_query_atasan = "WITH RECURSIVE atasan_path AS (
                                    SELECT idnik, nama, atasan
                                    FROM user
                                    WHERE idnik = '$id_request'
                                    UNION ALL
                                    SELECT u.idnik, u.nama, u.atasan
                                    FROM user u
                                    INNER JOIN atasan_path ap ON u.idnik = ap.atasan
                                )
                                SELECT idnik FROM atasan_path WHERE idnik != '$id_request'";
            $cek_result_atasan = mysqli_query($koneksi, $cek_query_atasan);
            
            // Insert approval untuk setiap atasan yang ditemukan
            while ($atasan = mysqli_fetch_assoc($cek_result_atasan)) {
                $nik_approval = $atasan['idnik'];
                $status = 'Pending'; // Misal status awal adalah 'Pending'
                $insert_approval_query = "INSERT INTO budget_approval_mr (id_mr, nik_approval, status) VALUES ('$id_mr', '$nik_approval', '$status')";
                mysqli_query($koneksi, $insert_approval_query);
            }

            header("location:../index.php?page=DetailMR&id=$id_mr");
        } else {
            // Handle jika insert budget_mr gagal
            header("location:../index.php?page=DetailMR&id=$id_mr");
        }
    }
} else {
    die("Akses dilarang...");
}
?>
