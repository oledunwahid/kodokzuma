<?php
include '../../koneksi.php'; // Sesuaikan path ini sesuai dengan lokasi skrip koneksi database Anda

if(isset($_POST['id_detail_mr'])) {
    $id_detail_mr = $_POST['id_detail_mr'];
    $id_mr = $_POST['id_mr'];
    $nama_barang = $_POST['nama_barang'];
    $uom = $_POST['uom']; // Menambahkan variabel untuk uom
    $qty_barang = $_POST['qty_barang'];
    $price = $_POST['price'];
	

    // Menambahkan uom ke query update
    $query = "UPDATE budget_detail_mr SET nama_barang = ?, uom = ?, qty_barang = ?, price = ? WHERE id_detail_mr = ?";

    // Menyiapkan prepared statement untuk meningkatkan keamanan
    $stmt = mysqli_prepare($koneksi, $query);
    
    // Mengikat parameter ke statement yang disiapkan
    mysqli_stmt_bind_param($stmt, "ssidi", $nama_barang, $uom, $qty_barang, $price, $id_detail_mr);

    // Menjalankan prepared statement
    if(mysqli_stmt_execute($stmt)) {
        echo "Data berhasil diupdate";
    } else {
        echo "Error: " . mysqli_error($koneksi);
    }

    // Menutup statement
    mysqli_stmt_close($stmt);
} else {
    echo "ID Detail MR tidak ditemukan";
}
?>
