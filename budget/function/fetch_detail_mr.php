<?php
include '../../koneksi.php'; // Sesuaikan dengan path file koneksi Anda
$id_mr = $_GET['id_mr'];
function nominal($angka)
{
    $hasil_nominal = number_format($angka, 0, ',', '.');
    return $hasil_nominal;
}

$query = "SELECT * FROM budget_detail_mr WHERE id_mr='$id_mr'";
$result = mysqli_query($koneksi, $query);

$output = '';
while ($row = mysqli_fetch_assoc($result)) {
    // Menghitung total harga (harga * jumlah)
    $totalHarga = $row['price'] * $row['qty_barang'];

    $output .= "<tr>
                    <td style='display:none;'><input type='text' name='id_mr[]' class='form-control' value='" . $row['id_mr'] . "' readonly /></td>
                    <td><input type='text' name='nama_barang[]' class='form-control' value='" . $row['nama_barang'] . "' readonly </td>
					<td><input type='number' name='qty_barang[]' class='form-control' value='" . $row['qty_barang'] . "' readonly maxlength='5' /></td>
                    <td><input type='text' name='uom[]' class='form-control' value='" . $row['uom'] . "' readonly maxlength='5' /></td> 
                    <td><input type='text' name='price[]' class='form-control' value='" . nominal($row['price']) . "' readonly maxlength='11' /></td>
                    <td><span class='totalHarga'><b>" . nominal($totalHarga) . "</b></span></td>
                    <td>
                        <button type='button' class='btn btn-info btn-sm edit'>E</button>
                        <button type='button' class='btn btn-success btn-sm saveRow' style='display:none;'>S</button>
                        <button type='button' class='btn btn-danger btn-sm remove' data-id='" . $row['id_detail_mr'] . "'>R</button>
                    </td>
                </tr>";
}
echo $output;
