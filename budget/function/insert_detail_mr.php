<?php
include '../../koneksi.php'; // Sesuaikan dengan path file koneksi Anda

if(isset($_POST['id_mr'])) {
    $id_mr = $_POST['id_mr'];
    $nama_barang = $_POST['nama_barang'];
    $qty_barang = $_POST['qty_barang'];
    $price = $_POST['price'];
    $uom = $_POST['uom']; // Mengambil nilai uom dari POST request

    // Menyiapkan prepared statement dengan parameter tambahan untuk uom
    $stmt = $koneksi->prepare("INSERT INTO budget_detail_mr (id_mr, uom, nama_barang, qty_barang, price) VALUES (?, ?, ?, ?, ?)");

    // Mengikat parameter ke statement, termasuk uom
    $stmt->bind_param("sssid", $id_mr, $uom, $nama_barang, $qty_barang, $price);

    // Menjalankan statement
    if(!$stmt->execute()){
        echo "Error: " . $stmt->error;
        $stmt->close();
        $koneksi->close();
        exit;
    }

    $stmt->close();
    $koneksi->close();
    echo "success";
} else {
    echo "No data received";
}
?>
