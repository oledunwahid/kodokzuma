
<?php
$page = (isset($_GET['page'])) ? $_GET['page'] : '';

if (isset($_SESSION['username'])) {
	if ($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'user' ) {

		switch ($page) {
			
			case 'Dashboard':
				include "home-budget.php";
				break;
			case 'Material-Request':
				include "material-request.php";
				break;
			case 'DetailMR':
				include "detail-material-request.php";
				break;	
			case 'ViewMR':
				include "print-detail-mr.php";
				break;	
			case 'ApprovalMR':
				include "list-approval.php";
				break;	
			case 'LinkApprovalMR':
				include "link-approval.php";
				break;	
			default:
				include "pages-404.php";
				break;
		}
	}
	if ($_SESSION['role'] == 'manage') { // Jika user yang login adalah user
		// Berikut halaman yang bisa di akses :
		switch ($page) {
			case 'Dashboard':
				include "home.php";
				break;
			case 'DetailMR':
				include "detail-material-request.php";
				break;	
			case 'ViewMR':
				include "print-detail-mr.php";
				break;		
			case 'ApprovalMR':
				include "list-approval.php";
				break;	
			case 'LinkApprovalMR':
				include "link-approval.php";
				break;		
				
			default:
				include "pages-404.php";
				break;
		}
	}
} else
	include "login.php";

?>
