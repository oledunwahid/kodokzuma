<?php 	$query = mysqli_query($koneksi, "SELECT max(ididea) as kodeTerbesar FROM idea");
	$data = mysqli_fetch_array($query);
	$kodeid = $data['kodeTerbesar'];
	$urutan = (int) substr($kodeid, 3, 4);
 
	// bilangan yang diambil ini ditambah 1 untuk menentukan nomor urut berikutnya
	$urutan++;
 
	
	$huruf = "SUG";
	$kodeid= $huruf . sprintf("%04s", $urutan);
	?>


<div class="row">
	<form action="function/insert_suges.php" method="POST" >
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-body">
									
                                    <div class="mb-3">
                                        <label class="form-label" >No. Suges</label>
										<input type="text" class="form-control" hidden value="<?=$niklogin ?>" name="idnik">
                                        <input type="text" class="form-control"  value="<?=$kodeid ?>" name="idsuges" readonly >
                                    </div>
									<div class="mb-3">
                                        <label class="form-label" >Input Date</label>
										<input type="date" class="form-control flatpickr-input active" data-provider="flatpickr" value="<?=$tgl?>" name="tgl_suges">
                                    </div>
									<div class="mb-3">
                                        <label class="form-label" >Your Suggestion For ?</label>
                                        <input type="text" class="form-control"  placeholder="Judul IdeMu..." name="judul_suges">
                                    </div>
									 <div class="mb-3">
										
                                         <label class="form-label">Reason for Your Suggestion ?</label>
                                        <textarea id="ckeditor-classic" name="reason_suges">
                                            
                                        </textarea>

                                    </div> 
									 <div class="mb-3">
										
                                         <label class="form-label">Describe Your Suggestion ?</label>
                                        <textarea id="ckeditor-classic1" name="describe_suges">
                                            
                                        </textarea>

                                    </div> 
									

                                    
                                
                                    </div>

                                    
                                </div>
                                <!-- end card body -->
                            
                            <!-- end card -->

                           
                            <!-- end card -->
                            <div class="text-end mb-4">
                                
                                <button type="submit" name="share-suges" class="btn btn-success w-sm">Create</button>
                            </div>
							</form>
		</div>
                        </div>
                        <!-- end col -->
                        
                   

    <script src="assets/libs/@ckeditor/ckeditor5-build-classic/build/ckeditor.js"></script>
  <script src="assets/js/pages/project-create.init.js"></script>