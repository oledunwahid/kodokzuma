<style type="text/css" media="print, handheld">
    * { display: none; }
</style>
<?php
$pp 	  = '';
$idpolicy = '';
$idsop    = $_GET['id'];
$idim	  = '';
$page     = $_GET['page'];
$cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM histori WHERE idnik='$niklogin' AND idsop='$idsop'"));

if ($cek > 0) {
    echo 'SOP Pernah Dilihat';
} else {
    $query = "INSERT INTO histori 
				VALUES 
				('','$niklogin','$pp','$idpolicy','$idsop','$idim','$page')";
    $wali = mysqli_query($koneksi, $query);;
}

$sql = mysqli_query($koneksi, "SELECT * FROM sop
										 WHERE idsop='" . $_GET['id'] . "' "); // query jika filter dipilih

$row = mysqli_fetch_assoc($sql) // fetch query yang sesuai ke dalam array

?>


				<div class="row">
                        <div class="col-lg-12">
                            <div class="card mt-n4 mx-n4 mb-n5">
                                <div class="bg-soft-warning">
                                    <div class="card-body pb-4 mb-5">
                                        <div class="row">
                                            <div class="col-md">
                                                <div class="row align-items-center">
                                                    <div class="col-md-auto">
                                                        <div class="avatar-md mb-md-0 mb-4">
                                                            <div class="avatar-title bg-white rounded-circle">
                                                                <img src="file/refferal/logo-refferal.jpg" alt="" class="avatar-sm" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end col-->
                                                    <div class="col-md">
                                                        <h4 class="fw-semibold" >Sop dan Form </h4>
                                                        
												       </div>
													<?php if(!empty($_SESSION["notif"])){
										   echo $_SESSION["notif"];
										   unset($_SESSION["notif"]);
									   		}	?>
                                                    <!--end col-->
                                                </div>
                                                <!--end row-->
                                            </div>
                                            <!--end col-->
                                            
                                            <!--end col-->
                                        </div>
                                        <!--end row-->
                                    </div><!-- end card body -->
                                </div>
                            </div><!-- end card -->
                        </div><!-- end col -->
                    </div><!-- end row -->

                    <div class="row">
                        <div class="col-xxl-9">
                            <div class="card">
                                
                                <!--end card-body-->
                                <div class="card-body p-4">
                                    <h5 class="card-title mb-4">Kententuan Prosedure <?= $row['nama_sop'] ?></h5>
                                    <iframe src="<?= $row['file_sop'] ?>" width="100%" height="800"> </iframe>
                                </div>
                                <!-- end card body -->
                            </div>
                            <!--end card-->
                        </div>
                        <!--end col-->
						
						
                        <div class="col-xxl-3">
							
                            <div class="card">
								<div class="position top-0 start-0 ">
	<?php if ($_SESSION['role'] == 'admin') { ?>
									<button class="btn btn-danger add-btn" data-bs-toggle="modal" data-bs-target="#addform"><i class="ri-add-line align-bottom me-1"></i> Add Form </button>
										<?php }?></div>
								
                                <div class="card-header">
									
                                    <h6 class="card-title fw-semibold mb-0">Files Attachment</h6>
									
                                </div>
                                <div class="card-body">
									<?php $sql = mysqli_query($koneksi, "SELECT * FROM form_sop
										 WHERE idsop='" . $_GET['id'] . "' "); // query jika filter dipilih

while ($row1 = mysqli_fetch_assoc($sql)){ ?>
                                    <div class="d-flex align-items-center border border-dashed p-2 rounded">
                                        <div class="flex-shrink-0 avatar-sm">
                                            <div class="avatar-title bg-light rounded">
                                                <i class="<?php if(substr($row1['file_form'],-5)=='.xlsx'){
																echo "ri-file-excel-fill";
																   }else{
																	   
																	  echo "ri-file-word-fill";
																   }
														  
														  ?> fs-20 text-success"></i>
                                            </div>
                                        </div>
                                        <div class="flex-grow-1 ms-3">
                                            <h6 class="mb-1"><a href="javascript:void(0);"><?= $row1['nama_form'] ?></a></h6>
                                            <small class="text-muted">1 MB</small>
                                        </div>
                                        <div class="hstack gap-3 fs-16">
                                            <a href="file/dokumen/<?= $row1['file_form'] ?>" class="text-muted"><i class="ri-download-2-line"></i></a>
                                           	<?php if ($_SESSION['role'] == 'admin' && $menulogin == '2' ||$menulogin == '9') { ?>
											<div class="hstack gap-3 fs-16">
                                            <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#delete<?=$row1['idform']?>" class="text-muted"><i class="ri-delete-bin-line"></i></a>
												
											
                                            
                                        </div>
											<?php }?>
                                        </div>
										
                                    </div>
									<?php } ?>
                                   
                                </div>
                            </div>
							<div class="card">
                                <div class="card-header d-flex align-items-center">
                                    <h6 class="card-title mb- flex-grow-1">History View Policy </h6>
                                    <a class="text-muted" href="">
                                        See All <?php $sql2 = mysqli_query($koneksi,"SELECT idhistori FROM histori where idsop= $idsop");
												$row2 = mysqli_num_rows($sql2);
												?><?=$row2;?> User
										<i class="ri-arrow-right-line align-bottom"></i>
                                    </a>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive table-card">
                                        <div data-simplebar style="max-height: 365px;">
                                            <ul class="list-group list-group-flush">
                                                      <?php $sql = mysqli_query($koneksi, "SELECT
												user.nama, 
												user.divisi,
												user.file_foto
											FROM
												histori
												INNER JOIN user ON histori.idnik = user.idnik 
											WHERE
												histori.idsop = $idsop 
											ORDER BY histori.idhistori DESC "); 
														
													while ($row = mysqli_fetch_assoc($sql)) { 

                    									?>
                                                <li class="list-group-item list-group-item-action">
                                                    <div class="d-flex align-items-center">
                                                        <img src="file/profile/<?=$row['file_foto']?>" alt="" class="avatar-xs object-cover rounded-circle">
                                                        <div class="ms-3 flex-grow-1">
                                                            
                                                                <h6 class="fs-14 mb-1"><?=$row['nama']?></h6>
                                                         <p class="mb-0 text-muted"><?=$row['divisi']?></p>
                                                        </div>
                                                    </div>
                                                </li>
											<?php	}?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
							
                        </div>
                        <!--end col-->
                    </div>
                    <!--end row-->
<div class="modal fade zoomIn" id="addform" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered ">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Form SOP</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/insert_form_sop.php" method="POST" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            
                                                    <input hidden type="text" name="idsop" value="<?=$idsop?>"  >
                                               
                                            <div class="col-lg-12">
                                                <label for="date-field" class="form-label">URL File Form SOP</label>
                                                <input type="file" id="basiInput" class="form-control" name="file_form" required />
                                            </div>
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add Form SOP</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

     <?php $sql = mysqli_query($koneksi, "SELECT * FROM form_sop ");

    while ($row1 = mysqli_fetch_assoc($sql)) { ?>
<div class="modal fade zoomIn" id="delete<?=$row1['idform']?>" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="btn-close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="mt-2 text-center">
                                        <lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>
                                        <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                                            <h4>Are you Sure ?</h4>
                                            <p class="text-muted mx-4 mb-0">Are you Sure You want to Remove this Record <br> <b><?=$row1['nama_form']; ?></b> ?</p>
                                        </div>
                                    </div>
                                    <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                                        <button type="button" class="btn w-sm btn-light" data-bs-dismiss="modal">Close</button>
										<a style="cursor: pointer;" onclick="location.href='function/delete_form_sop.php?aksi=delete&id=<?=$row1['idform']?>&uid=<?=$idsop?>'" class="btn w-sm btn-danger">Yes, Delete It!</a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php }?>



    