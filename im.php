 <!--datatable css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <!--datatable responsive css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<?php if(!empty($_SESSION["notif"])){
										   echo $_SESSION["notif"];
										   unset($_SESSION["notif"]);
									   		}	?>
<div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
									<div class="d-flex align-items-center">
                                    <h5 class="card-title mb-0 flex-grow-1">Internal Memo </h5>
										<?php if ($_SESSION['role'] == 'admin' && $menulogin == '2' ||$menulogin == '9') { ?>
										<div class="flex-shrink-0">
									<button class="btn btn-danger add-btn" data-bs-toggle="modal" data-bs-target="#showModal"><i class="ri-add-line align-bottom me-1"></i> Create Internal Memo</button>
											</div>
										<?php }?>
                                </div>
								</div>
								
                                <div class="card-body">
                                     <table id="IM" class="display table table-bordered dt-responsive" style="width:100%">
                                        <thead>
                                            <tr>
                                               
                                                <th>No.</th>
                                                <th>Internal Memo Number</th>
												<th>Internal Memo Description</th>
												<th>Issued Date</th>
										        <th>Status</th>
                                                
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                              <?php $sql = mysqli_query($koneksi, "SELECT * FROM im "); 
											$nomor=1;
                    while ($row = mysqli_fetch_assoc($sql)) { 

                    ?>
                                            <tr>
                                                
                                                <td><?=$nomor++ ?></td>
                                                <td><?= $row['no_im'] ?></td>
                                                <td><a href="index.php?page=ViewInternalMemo&id=<?= $row['idim']; ?>" > <?= $row['nama_im'] ?></a></td>
                                                <td><?= $row['tgl_im'] ?></td>
												 <td><span class="badge <?php if($row['status_im']=='Aktif'){
													echo 'bg-success';}
													else{ echo 'bg-danger';
													} ?>"><?= $row['status_im'] ?></span></td>
                                
                                                <td>
                                                    <div class="dropdown d-inline-block">
                                                        <button class="btn btn-soft-secondary btn-sm dropdown" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                            <i class="ri-more-fill align-middle"></i>
                                                        </button>
                                                        <ul class="dropdown-menu dropdown-menu-end">
                                                            <li><a href="index.php?page=ViewInternalMemo&id=<?= $row['idim']; ?>" class="dropdown-item"><i class="ri-eye-fill align-bottom me-2 text-muted"></i> View</a></li>
															<?php if ($_SESSION['role'] == 'admin' && $menulogin == '2' ||$menulogin == '9') { ?>
                                                            <li>
															<a class="dropdown-item edit-item-btn" data-bs-toggle="modal" data-bs-target="#update<?=$row['idim']?>"><i class="ri-pencil-fill align-bottom me-2 text-muted"></i> Edit</a></li>
                                                            <li>
                                                                							<a class="dropdown-item remove-item-btn" data-bs-toggle="modal" data-bs-target="#delete<?=$row['idim']?>"><i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i> Delete</a>
                                                            </li>
															<?php }?>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--end col-->
                    </div>


<div class="modal fade zoomIn" id="showModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Create Internal Memo</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/insert_im.php" method="POST">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-6">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">No.Internal Memo</label>
                                                    <input type="text" class="form-control" placeholder="Masukan No. Internal Memo" name="no_im" required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Judul Internal Memo</label>
                                                    <input type="text" class="form-control" placeholder="Masukan Judul Internal Memo" name="nama_im" required />
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Tangga Issued</label>
                                                    <input type="date"  class="form-control" value="<?php echo "$tgl" ?>" name="tgl_im" required />
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="assignedtoName-field" class="form-label">Status Internal Memo</label>
                                                    <select class="form-control" data-plugin="choices" name="status_im" >
                                                    <option value="On Process">On Process</option>
														<option value="Aktif">Aktif</option>
													 <option value="Non Aktif">Non Aktif</option>
													
                                                </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <label for="date-field" class="form-label">URL File Internal Memo PDF</label>
                                                <input type="text" id="basiInput" class="form-control"  placeholder="Masukan URL Google Grive" name="file_im" required />
                                            </div>
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Add Internal Memo</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

<?php $sql = mysqli_query($koneksi, "SELECT * FROM im ");

    while ($row = mysqli_fetch_assoc($sql)) { ?>
<div class="modal fade zoomIn" id="delete<?=$row['idim']?>" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="btn-close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="mt-2 text-center">
                                        <lord-icon src="https://cdn.lordicon.com/gsqxdxog.json" trigger="loop" colors="primary:#f7b84b,secondary:#f06548" style="width:100px;height:100px"></lord-icon>
                                        <div class="mt-4 pt-2 fs-15 mx-4 mx-sm-5">
                                            <h4>Are you Sure ?</h4>
                                            <p class="text-muted mx-4 mb-0">Are you Sure You want to Remove this Record <br> <b><?=$row['nama_im']; ?></b> ?</p>
                                        </div>
                                    </div>
                                    <div class="d-flex gap-2 justify-content-center mt-4 mb-2">
                                        <button type="button" class="btn w-sm btn-light" data-bs-dismiss="modal">Close</button>
										<a style="cursor: pointer;" onclick="location.href='function/delete_im.php?aksi=delete&id=<?=$row['idim']; ?>'" class="btn w-sm btn-danger">Yes, Delete It!</a>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<div class="modal fade zoomIn" id="update<?=$row['idim']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Update Internal Memo</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/update_im.php" method="POST">
                                    <div class="modal-body">
                                        <div class="row g-3">
                                            <div class="col-lg-6">
                                                <div id="modal-id">
                                                    <label for="orderId" class="form-label">No.Internal Memo</label>
													<input type="text" class="form-control" placeholder="Masukan No. IM" name="idim" hidden value="<?=$row['idim']?>" required/>
                                                    <input type="text" class="form-control" placeholder="Masukan No. IM" name="no_im" value="<?=$row['no_im']?>" required/>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="tasksTitle-field" class="form-label">Judul Internal Memo</label>
                                                    <input type="text" class="form-control" placeholder="Masukan Judul Internal Memo" name="nama_im" value="<?=$row['nama_im']?>" required />
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="client_nameName-field" class="form-label">Tangga Issued</label>
                                                    <input type="date"  class="form-control" value="<?=$row['tgl_im']?>" name="tgl_im" required />
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <label for="assignedtoName-field" class="form-label">Status Internal Memo</label>
                                                    <select class="form-control" data-plugin="choices" name="status_im" >
														<option value="<?=$row['status_im']?>"><?=$row['status_im']?></option>
                                                    <option value="On Process">On Process</option>
														<option value="Aktif">Aktif</option>
													 <option value="Non Aktif">Non Aktif</option>
													
                                                </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="date-field" class="form-label">URL File Internal Memo PDF</label>
                                                <input type="text" class="form-control" value="<?=$row['file_im']?>" placeholder="Masukan URL Google Grive" name="file_im"  />
                                            </div>
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Update Internal Memo</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
<?php }?>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


<!--datatable js-->
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="assets/js/pages/datatables.init.js"></script>
    
    <script>

document.addEventListener("DOMContentLoaded", function() {
    new DataTable("#IM", {
        dom: "Bfrtip",
        buttons: ["copy", "csv", "excel", "print", "pdf"],
        order: [[3, 'desc']] // Menetapkan urutan descending pada kolom pertama
    });
})

</script>