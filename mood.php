 <!--datatable css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <!--datatable responsive css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<?php if(!empty($_SESSION["notif"])){
										   echo $_SESSION["notif"];
										   unset($_SESSION["notif"]);
									   		}	?>
<div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header">
									<div class="d-flex align-items-center">
                                    <h5 class="card-title mb-0 flex-grow-1">Your Mood</h5>
										<div class="flex-shrink-0">
									<button class="btn btn-danger add-btn" data-bs-toggle="modal" data-bs-target="#showModal"><i class="ri-add-line align-bottom me-1"></i> Create Your Mood</button>
											</div>
                                </div>
								</div>
								
                                <div class="card-body">
                                     <table id="buttons-datatables" class="display table table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                               
                                                <th>No.</th>
                                                <th>Tanggal</th>
												<th>Your Mood</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                              <?php $sql = mysqli_query($koneksi, "SELECT * FROM mood WHERE idnik ='$niklogin' "); 
											$nomor=1;
                    while ($row = mysqli_fetch_assoc($sql)) { 

                    ?>
                                            <tr>
                                                
                                                <td><?=$nomor++ ?></td>
                                                <td><?= $row['tgl_mood'] ?></td>
                                                <td><?= $row['mood'] ?></td>
                                               
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--end col-->
                    </div>


<div class="modal fade zoomIn" id="showModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-sm">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel">Ekspresi Your Mood</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/insert_mood.php" method="POST">
                                    <div class="modal-body">
                                        <div class="row g-6">
                                          
                                               
											
												<!-- Base Radios -->
										<div class="form-check mb-2">
											<input type="text"  name="idnik" value="<?=$rowlogin['idnik']?>" hidden>
										<input  type="date" name="tgl_mood" value="<?= "$tgl" ?>" hidden>
											<input class="form-check-input" type="radio" name="mood" value="&#128512; Happy" >
											<label class="form-check-label">
												<span style='font-size:20px;'>&#128512; Happy</span>
											</label>
										</div>
										<div class="form-check mb-2">
											<input class="form-check-input" type="radio"  name="mood" value="&#128525; Love" >
											<label class="form-check-label" >
												<span style='font-size:20px;'>&#128525; Love</span>
											</label>
										</div>	

										<div class="form-check mb-2">
											<input class="form-check-input" type="radio"  name="mood" value="&#128542; Sad">
											<label class="form-check-label" >
												<span style='font-size:20px;'>&#128542; Sad</span>
											</label>
										</div>
										<div class="form-check mb-2">
											<input class="form-check-input" type="radio"  name="mood" value="&#128545; Angry" >
											<label class="form-check-label" >
												<span style='font-size:20px;'>&#128545; Angry</span>
											</label>
										</div>
											<div class="form-check mb-2">
											<input class="form-check-input" type="radio" name="mood" value="&#9203; Busy">
											<label class="form-check-label">
												<span style='font-size:20px;'>&#9203; Busy</span>
											</label>
										</div>
											
                                                                                      
                                           
                                        
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="share-mood" id="add-btn">Add Your Mood</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


<!--datatable js-->
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="assets/js/pages/datatables.init.js"></script>