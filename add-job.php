<?php 	$query = mysqli_query($koneksi, "SELECT max(idjob) as kodeTerbesar FROM job");
	$data = mysqli_fetch_array($query);
	$kodeid = $data['kodeTerbesar'];
	$urutan = (int) substr($kodeid, 3, 4);
 
	// bilangan yang diambil ini ditambah 1 untuk menentukan nomor urut berikutnya
	$urutan++;
 
	
	$huruf = "JOB";
	$kodeid= $huruf . sprintf("%04s", $urutan);
	?>


<div class="row">
	<form action="function/insert_job.php" method="POST" >
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-body">
									
                                    <div class="mb-3">
                                        <label class="form-label" >No. JOB</label>
										<input type="text" class="form-control" hidden value="<?=$niklogin ?>" name="idnik">
                                        <input type="text" class="form-control"  value="<?=$kodeid ?>" name="idjob" readonly >
                                    </div>
									<div class="mb-3">
                                        <label class="form-label" >Input Date</label>
										<input type="date" class="form-control flatpickr-input active" data-provider="flatpickr" value="<?=$tgl?>" name="tgl_job">
                                    </div>
									 <div class="mb-3">
                                        <label class="form-label" >Posisi Job</label>
                                        <input type="text" class="form-control"   name="posisi_job"  >
                                    </div>
									<div class="mb-3">
                                        <label class="form-label" >Nama Perusahaan PT</label>
                                        <input type="text" class="form-control"   name="namapt"  >
                                    </div>
									<div class="mb-3">
                                        <label class="form-label" >Lokasi Job</label>
                                        <input type="text" class="form-control"  name="lokasi_job">
                                    </div>
									 <div class="mb-3">
										
                                         <label class="form-label">Description Job</label>
                                        <textarea id="ckeditor-classic" name="diskripsi">
                                            
                                        </textarea>

                                    </div> 
									 <div class="mb-3">
										
                                         <label class="form-label">Status Job</label>
                                         <select class="form-control" data-plugin="choices" name="status_job" >
														<option value="Aktif">Aktif</option>
													 <option value="Non Aktif">Non Aktif</option>
													
                                                </select>

                                    </div> 
									

                                    
                                
                                    </div>

                                    
                                </div>
                                <!-- end card body -->
                            
                            <!-- end card -->

                           
                            <!-- end card -->
                            <div class="text-end mb-4">
                                
                                <button type="submit" name="masukan" class="btn btn-success w-sm">Create</button>
                            </div>
							</form>
		</div>
                        </div>
                        <!-- end col -->
                        
                   

    <script src="assets/libs/@ckeditor/ckeditor5-build-classic/build/ckeditor.js"></script>
  <script src="assets/js/pages/project-create.init.js"></script>