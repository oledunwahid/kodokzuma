
<?php
$page = (isset($_GET['page'])) ? $_GET['page'] : '';

if (isset($_SESSION['username'])) {
	if ($_SESSION['role'] == 'admin') {

		switch ($page) {
			
			case 'Dashboard':
				include "home.php";
				break;
			case 'Profile':
				include "profile.php";
				break;
			case 'Company':
				include "company.php";
				break;
			case 'Policy':
				include "policy.php";
				break;
			case 'ViewPolicy':
				include "view-policy.php";
				break;	
			case 'InternalMemo':
				include "im.php";
				break;
			case 'ViewInternalMemo':
				include "view-im.php";
				break;		
			case 'SopForm':
				include "sop.php";
				break;
			case 'ViewSop':
				include "view-sop.php";
				break;
			case 'Employee':
				include "employee.php";
				break;
			case 'UpdateUser':
				include "edit-user.php";
				break;
			case 'Profile':
				include "profile.php";
				break;
			case 'ViewProfile':
				include "view-profile.php";
				break;		
			case 'News':
				include "news.php";
				break;	
			case 'AddNews':
				include "add-news.php";
				break;
			case 'ViewNews':
				include "view-news.php";
				break;
			case 'UpdateNews':
				include "edit-news.php";
				break;
			case 'IdeaBox':
				include "idea-box.php";
				break;
			case 'Mood':
				include "mood.php";
				break;
			case 'Idea':
				include "idea.php";
				break;
			case 'AddIdea':
				include "add-idea.php";
				break;
			case 'ViewIdea':
				include "view-idea.php";
				break;	
			case 'Suges':
				include "suges.php";
				break;	
			case 'AddSuges':
				include "add-suges.php";
				break;	
			case 'ViewSuges':
				include "view-suges.php";
				break;
			case 'Refferal':
				include "refferal.php";
				break;
			case 'AddJob':
				include "add-job.php";
				break;
			case 'ViewJob':
				include "view-job.php";
				break;
			case 'UpdateJob':
				include "edit-job.php";
				break;	
			case 'ListApply':
				include "list-apply.php";
				break;	
			case 'ListAllApply':
				include "list-all-apply.php";
				break;	
			case 'Learning':
				include "learning.php";
				break;	
			case 'LearningModule':
				include "learning-module.php";
				break;
			case 'ViewAuction':
				include "view-lelang.php";
				break;	
			case 'Auction':
				include "lelang.php";
				break;
			case 'ListBid':
				include "list-bid.php";
				break;
			case 'ListAllBid':
				include "list-all-bid.php";
				break;	
			case 'AddAuction':
				include "add-lelang.php";
				break;	
			case 'UpdateAuction':
				include "edit-lelang.php";
				break;		
			case 'ListLearning':
				include "list-learning.php";
				break;		
			case 'ListAllLearning':
				include "list-all-learning.php";
				break;	
			case 'ViewModule':
				include "view-module.php";
				break;		
			default:
				include "pages-404.php";
				break;
		}
	}
	if ($_SESSION['role'] == 'user') { // Jika user yang login adalah user
		// Berikut halaman yang bisa di akses :
		switch ($page) {
			case 'Dashboard':
				include "home.php";
				break;
			case 'Profile':
				include "profile.php";
				break;
			case 'Company':
				include "company.php";
				break;
			case 'Policy':
				include "policy.php";
				break;
			case 'ViewPolicy':
				include "view-policy.php";
				break;		
			case 'SopForm':
				include "sop.php";
				break;
			case 'ViewSop':
				include "view-sop.php";
				break;
			case 'InternalMemo':
				include "im.php";
				break;
			case 'ViewInternalMemo':
				include "view-im.php";
				break;			
			case 'News':
				include "news.php";
				break;	
			case 'ViewNews':
				include "view-news.php";
				break;	
			case 'IdeaBox':
				include "idea-box.php";
				break;	
			case 'Mood':
				include "mood.php";
				break;
			case 'Idea':
				include "idea.php";
				break;
			case 'AddIdea':
				include "add-idea.php";
				break;
			case 'ViewIdea':
				include "view-idea.php";
				break;	
			case 'Suges':
				include "suges.php";
				break;	
			case 'AddSuges':
				include "add-suges.php";
				break;	
			case 'ViewSuges':
				include "view-suges.php";
				break;
			case 'Refferal':
				include "refferal.php";
				break;
			case 'ViewJob':
				include "view-job.php";
				break;
			case 'ListApply':
				include "list-apply.php";
				break;
			case 'Learning':
				include "learning.php";
				break;	
			case 'LearningModule':
				include "learning-module.php";
				break;
			case 'ListLearning':
				include "list-learning.php";
				break;
			case 'ViewModule':
				include "view-module.php";
				break;
			case 'ViewAuction':
				include "view-lelang.php";
				break;	
			case 'Auction':
				include "lelang.php";
				break;
			case 'ListBid':
				include "list-bid.php";
				break;	
		
			
					
			default:
				include "pages-404.php";
				break;
		}
	}
} else
	include "login.php";

?>
