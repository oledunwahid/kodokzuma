 <!--datatable css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <!--datatable responsive css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

<?php if(!empty($_SESSION["notif"])){
										   echo $_SESSION["notif"];
										   unset($_SESSION["notif"]);
									   		}	?>
<div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header">
									<div class="d-flex align-items-center">
                                    <h5 class="card-title mb-0 flex-grow-1">List All Apply Employee</h5>
										
                                </div>
								</div>
								
                                <div class="card-body">
                                     <table id="buttons-datatables" class="display table table-bordered dt-responsive" style="width:100%">
                                        <thead>
                                            <tr>
                                               
                                                <th>No.</th>
												<th>ID JOB</th>
												<th>Nama Apply</th>
                                                <th>Tanggal Apply</th>
												 <th>Posisi Apply</th>
												<th>Nama Referensi</th>
												<th>Hub Referensi</th>
												<th>Kontak Referensi</th>
												<th>File Lamaran</th>
												<th>Status Apply</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                              <?php $sql = mysqli_query($koneksi, "SELECT *, job.posisi_job, user.nama FROM apply_refferal  JOIN job ON apply_refferal.idjob = job.idjob JOIN user ON apply_refferal.idnik = user.idnik"); 
											$nomor=1;
                    while ($row = mysqli_fetch_assoc($sql)) { 

                    ?>
                                            <tr>
                                                
                                                <td><?=$nomor++ ?></td>
												<td><?= $row['idjob'] ?></td>
												<td><?= $row['nama'] ?></td>
                                                <td><?= $row['tgl_apply'] ?></td>
												<td><?= $row['posisi_job'] ?></td>
												<td><?= $row['nama_apply'] ?></td>
                                                <td><?= $row['hub_apply'] ?></td>
												<td><?= $row['kontak_apply'] ?></td>
												<td><a href="file/apply/<?=$row['file_apply'] ?>"><i class="ri-file-download-fill"></i> Download</a?></td>
												<td>	<div class="btn-group btn-group-sm mt-2"  >
   														<button class="btn btn-primary btn-group-sm" data-bs-toggle="modal" data-bs-target="#updatestatus<?=$row['idapply']?>"><?= $row['status_apply'] ?></button>
														</div>
												</td>
                                              
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><!--end col-->
                    </div>

<?php $sql1 = mysqli_query($koneksi, "SELECT * FROM apply_refferal ");

    while ($row1 = mysqli_fetch_assoc($sql1)) { ?>
<div class="modal fade zoomIn" id="updatestatus<?=$row1['idapply']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-sm">
						
                            <div class="modal-content border-0">
                                <div class="modal-header p-3 bg-soft-info">
                                    <h5 class="modal-title" id="exampleModalLabel"> Status Apply</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" id="close-modal"></button>
                                </div>
                                 <form action="function/update_status_apply.php" method="POST">
                                    <div class="modal-body">
										 <div class="row g-3">
                                       
											<div class="col-lg-12">
                                                <div>
                                                    <label for="assignedtoName-field" class="form-label">Status SOP</label>
													<input  type="text" name="idapply" value="<?=$row1['idapply']?>" hidden>
                                                    <select class="form-control" data-plugin="choices" name="status_apply" >
                                                   <option value="Pending">Pending</option>
														<option value="On Process">On Process</option>
														<option value="Interview">Interview</option>
													 <option value="Closed">Closed</option>
														<option value="Diterima">Diterima</option>
														<option value="Gagal">Ditolak</option>
													
                                                </select>
                                                </div>
                                            </div>

												<!-- Base Radios -->
										
										
                                            
                                            
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-success" name="masukan" id="add-btn">Update</button>
                                           
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
<?php }?>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


<!--datatable js-->
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

    <script src="assets/js/pages/datatables.init.js"></script>