<?php 	$query = mysqli_query($koneksi, "SELECT max(idnews) as kodeTerbesar FROM news");
	$data = mysqli_fetch_array($query);
	$kodeid = $data['kodeTerbesar'];
	$urutan = (int) substr($kodeid, 3, 3);
 
	// bilangan yang diambil ini ditambah 1 untuk menentukan nomor urut berikutnya
	$urutan++;
 
	
	$huruf = "NEW";
	$kodeid= $huruf . sprintf("%03s", $urutan);
	?>


<div class="row">
	<form action="function/insert_news.php" method="POST" enctype="multipart/form-data">
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-body">
									
                                    <div class="mb-3">
                                        <label class="form-label" >No. News</label>
										<input type="text" class="form-control" hidden value="<?=$niklogin ?>" name="idnik">
                                        <input type="text" class="form-control"  value="<?=$kodeid ?>" name="idnews">
                                    </div>
									<div class="mb-3">
                                        <label class="form-label" >Input Date</label>
										<input type="date" class="form-control flatpickr-input active" data-provider="flatpickr" value="<?=$tgl?>" name="tgl_news">
                                    </div>
									<div class="mb-3">
                                        <label class="form-label" >News Title</label>
                                        <input type="text" class="form-control"  placeholder="Enter News Title" name="title">
                                    </div>
									<div class="mb-3">
                                        <label class="form-label" >Category</label>
                                        <input type="text" class="form-control" name="category" placeholder="Enter News Category"  name="category">
                                    </div>

                                    <div class="mb-3">
                                        <label class="form-label" > Image OR Video</label>
                                        <input class="form-control"  type="file" name="image_news" accept="image/png, image/gif, image/jpeg, video/mp4,">
                                    </div>

                                    <div class="mb-3">
										
                                         <label class="form-label">News Description</label>
                                        <textarea id="ckeditor-classic" name="content">
                                            
                                        </textarea>

                                    </div> 
                                
                                    </div>

                                    
                                </div>
                                <!-- end card body -->
                            
                            <!-- end card -->

                           
                            <!-- end card -->
                            <div class="text-end mb-4">
                                
                                <button type="submit" name="masukan" class="btn btn-success w-sm">Create</button>
                            </div>
							</form>
		</div>
                        </div>
                        <!-- end col -->
                        
                   

    <script src="assets/libs/@ckeditor/ckeditor5-build-classic/build/ckeditor.js"></script>
  <script src="assets/js/pages/project-create.init.js"></script>