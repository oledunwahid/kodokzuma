<?php 	$query = mysqli_query($koneksi, "SELECT max(ididea) as kodeTerbesar FROM idea");
	$data = mysqli_fetch_array($query);
	$kodeid = $data['kodeTerbesar'];
	$urutan = (int) substr($kodeid, 3, 4);
 
	// bilangan yang diambil ini ditambah 1 untuk menentukan nomor urut berikutnya
	$urutan++;
 
	
	$huruf = "IDE";
	$kodeid= $huruf . sprintf("%04s", $urutan);
	?>


<div class="row">
	<form action="function/insert_idea.php" method="POST" >
                        <div class="col-lg-8">
                            <div class="card">
                                <div class="card-body">
									
                                    <div class="mb-3">
                                        <label class="form-label" >No. Idea</label>
										<input type="text" class="form-control" hidden value="<?=$niklogin ?>" name="idnik">
                                        <input type="text" class="form-control"  value="<?=$kodeid ?>" name="ididea" readonly >
                                    </div>
									<div class="mb-3">
                                        <label class="form-label" >Input Date</label>
										<input type="date" class="form-control flatpickr-input active" data-provider="flatpickr" value="<?=$tgl?>" name="tgl_idea">
                                    </div>
									<div class="mb-3">
                                        <label class="form-label" >Whats Your Idea</label>
                                        <input type="text" class="form-control"  placeholder="Judul IdeMu..." name="judul_idea">
                                    </div>
									 <div class="mb-3">
										
                                         <label class="form-label">Describe About Your Idea ?</label>
                                        <textarea id="ckeditor-classic" name="describe_idea">
                                            
                                        </textarea>

                                    </div> 
									 <div class="mb-3">
										
                                         <label class="form-label">Whats Your Reason for Create This Idea ?</label>
                                        <textarea id="ckeditor-classic1" name="reason_idea">
                                            
                                        </textarea>

                                    </div> 
									<div class="mb-3">
										
                                         <label class="form-label">Whats Your Goals For Your Idea ?</label>
                                        <textarea id="ckeditor-classic2" name="goals_idea">
                                            
                                        </textarea>

                                    </div>   

                                    
                                
                                    </div>

                                    
                                </div>
                                <!-- end card body -->
                            
                            <!-- end card -->

                           
                            <!-- end card -->
                            <div class="text-end mb-4">
                                
                                <button type="submit" name="share-idea" class="btn btn-success w-sm">Create</button>
                            </div>
							</form>
		</div>
                        </div>
                        <!-- end col -->
                        
                   

    <script src="assets/libs/@ckeditor/ckeditor5-build-classic/build/ckeditor.js"></script>
  <script src="assets/js/pages/project-create.init.js"></script>